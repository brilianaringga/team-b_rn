## Basic Instalation Guide

You must install react native CLI in your windows/mac

```
$ git clone https://gitlab.com/binar-telkomsel_use-case-project/team-b_training-app/team-b_rn.git
$ cd team-b-rn
$ git checkout develop
$ yarn install
$ react-native link
```

## Run Project

Start Metro Server in different terminal

```
$ yarn start
```

Android

```
$ yarn android
```

IOS

```
$ react-native unlink react-native-vector-icons`

$ yarn ios
```

## Branch Naming Rules

Branch name format: feature/{nama-feature}

```
$ git checkout -b feature/{nama-feature}
$ git add .
$ git commit -m 'add your own comment'
$ git push origin feature/{nama-feature}
```

## Merge Request Flow

feature/{nama-feature} MR → develop MR → production MR → master
