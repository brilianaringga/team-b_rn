/**
 * @name Colors
 */

export const Color = {
  primaryColor: '#2D8989',
  backgroundColor: '#F5FAFA',
  green: '#4BD964',
  yellowPastel: '#FFFAEC',
  pinkPastel: '#FEF0F0',
  purplePastel: '#F1EFF6',
  orangePastel: '#FFF7F0',
  lightGray: '#F3F4F9',
  gray: '#F3F1F1',
  darkGray: '#A9A9A9',
  dimGray: '#696969',
  primaryColorLighten: '#83d6d6',
  yellowStar: '#F0CE32',
  pink: '#E9B5D6',
  red: '#EF4F4F',
  blueOld: '#0e49b5',
  purpleOld: '#48426d',
};
