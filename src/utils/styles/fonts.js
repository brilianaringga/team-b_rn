/**
 * @name Fonts
 */

export const FONTS = {
  quickSand: "Quicksand-Medium",
  quickSandBold: "Quicksand-Bold",
  brandonGrotesque: "BrandonGrotesque-Regular",
  brandonGrotesqueMedium: "BrandonGrotesque-Medium",
  brandonGrotesqueBold: "BrandonGrotesque-Bold",
};
