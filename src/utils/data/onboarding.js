const { default: IMG } = require('assets/images');

const onboardingData2 = [
  {
    id: 11,
    title: 'Enroll in High Quality Courses',
    description:
      'You can explore any course published in ALITA apps, anytime, anywhere !',
    illustration: IMG.Intro1,
    createdAt: '2021-03-02T17:00:28.585Z',
    updatedAt: '2021-03-02T17:00:28.585Z',
    deletedAt: null,
  },
  {
    id: 9,
    title: 'International coursex',
    description:
      'This course can be taken any time, any where and has certain goalx',
    illustration: IMG.Intro2,
    createdAt: '2021-03-02T14:39:23.776Z',
    updatedAt: '2021-03-02T17:04:59.293Z',
    deletedAt: null,
  },
  {
    id: 10,
    title: 'Ready to find a Course ?',
    description:
      'Join our platform that will help you to increase your knowledge and you can share your knowledge to others',
    illustration: IMG.Intro3,
    createdAt: '2021-03-02T14:39:23.776Z',
    updatedAt: '2021-03-02T17:04:59.293Z',
    deletedAt: null,
  },
];

export default onboardingData2;
