import { StyleSheet } from 'react-native';
import { Color, scale } from 'utils';
const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: scale(15),
    backgroundColor: '#F5FAFA',
    paddingTop: scale(30),
  },
  topbar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: scale(20),
    marginBottom: scale(10),
  },
  notifContainer: {
    backgroundColor: 'white',
    padding: scale(8),
    borderRadius: scale(10),
  },
  notification: { position: 'relative' },
  icon: {
    height: scale(24),
    width: scale(24),
    backgroundColor: '#2D8989',
  },
  notifRed: {
    height: scale(4),
    width: scale(4),
    borderRadius: scale(2),
    backgroundColor: 'red',
    position: 'absolute',
    top: 0,
    right: 0,
  },
  userContainer: { flexDirection: 'row', alignItems: 'center' },
  userWrapper: {
    backgroundColor: '#D9EBEB',
    paddingVertical: scale(6),
    paddingHorizontal: scale(15),
    borderRadius: scale(15),
    marginRight: scale(-18),
  },
  userText: {
    marginRight: scale(10),
    color: '#2D8989',
    fontSize: scale(15),
  },
  avatar: {
    height: scale(45),
    width: scale(45),
    borderRadius: scale(45 / 2),
    borderWidth: scale(2.5),
    borderColor: 'white',
  },
  title: {
    color: Color.primaryColor,
    fontSize: scale(20),
    fontWeight: 'bold',
  },
  categoryWrapper: {
    marginTop: scale(10),
    marginRight: scale(-15),
  },
  greenLine: {
    width: scale(70),
    height: scale(4),
    backgroundColor: '#2D8989',
    borderTopLeftRadius: scale(25),
    borderTopRightRadius: scale(25),
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  seemore: {
    fontSize: scale(14),
    letterSpacing: scale(-0.2),
  },
  titleContainer: {
    paddingTop: 0,
    flex: 1,
    paddingHorizontal: scale(15),
    backgroundColor: '#F5FAFA',
  },
  emptyCourse: {
    textAlign: 'center',
    fontSize: scale(15),
    color: Color.primaryColor,
  },
  emptyCourseWrapper: { flex: 1 },
});

export default styles;
