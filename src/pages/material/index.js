import { Button, AuthorMaterialCard, PageHeader, Text } from 'components';
import { getMaterials } from 'configs/redux/reducers/myCourse/myCourseActions';
import { connect, React, View } from 'libraries';
import { useEffect } from 'react';
import { SafeAreaView, ScrollView } from 'react-native';
import { scale, verticalScale } from 'utils';
import EmptyMaterial from '../../assets/images/emptyMaterial.svg';
import styles from './style';

const MaterialPage = ({ navigation, route, ...props }) => {
  // get data from params
  const { course } = route.params;
  console.log(course);
  // get from redux
  const { courseMaterials, isLoading, errror } = props;
  const { dispatchFetchMaterials } = props;
  // function
  const handlerGoBack = () => navigation.goBack();
  const handlerGoCreate = () =>
    navigation.navigate('MaterialAddPage', { course: course });
  const handlerMaterialPress = (material) => {
    navigation.navigate('MaterialAddPage', {
      course: course,
      material: material,
    });
  };
  // component
  const CurriculumsComp = () => {
    return (
      <View style={{ flex: 1 }}>
        {courseMaterials.map((item, index) => {
          return (
            <View key={item.id} style={styles.curriculumCards}>
              <AuthorMaterialCard
                item={item}
                index={index + 1}
                onPress={() => handlerMaterialPress(item)}
              />
            </View>
          );
        })}
      </View>
    );
  };
  const EmptyCurriculumComp = () => {
    return (
      <View style={styles.emptyMaterials}>
        <EmptyMaterial width={scale(250)} height={scale(250)} />
        <Text style={styles.emptyMaterialsLabel}>Share it to the world</Text>
      </View>
    );
  };
  // useEffect
  useEffect(() => {
    const payload = {
      paramsId: course.id,
    };
    dispatchFetchMaterials(payload);
  }, []);

  // view
  return (
    <>
      <SafeAreaView style={styles.container}>
        <View style={styles.header}>
          <PageHeader title={course.title} backAction={handlerGoBack} />
        </View>
        <View style={styles.title}>
          <Text style={styles.titleText}>Materials</Text>
        </View>
        <ScrollView
          // showsVerticalScrollIndicator={false}
          style={styles.content}
          contentInset={{ bottom: verticalScale(150) }}
          contentContainerStyle={styles.contentContainer}>
          {courseMaterials.length > 0 ? (
            <CurriculumsComp />
          ) : (
            <EmptyCurriculumComp />
          )}
        </ScrollView>
      </SafeAreaView>
      {course.isPublished === false &&
        (course.subscriberCount === undefined ||
          course.subscriberCount === '0') && (
          <View style={styles.bottomActionContainer}>
            <SafeAreaView>
              <View>
                <Button
                  title={'Create Material'}
                  buttonStyle={styles.btnCreate}
                  textStyle={styles.btnCreateLabel}
                  onPress={handlerGoCreate}
                />
              </View>
            </SafeAreaView>
          </View>
        )}
    </>
  );
};

function mapStateToProps(state) {
  return {
    //isi state dari reducer
    courseMaterials: state.myCourseStore.courseMaterials,
    isLoading: state.myCourseStore.isLoading,
    error: state.myCourseStore.error,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    //isi dispatch
    dispatchFetchMaterials: (payload) => dispatch(getMaterials(payload)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(MaterialPage);
