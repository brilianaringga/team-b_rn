import { BackButton, Button, Text } from 'components';
import { base_url } from 'configs/api/url';
import { getCourseCategory } from 'configs/redux/reducers/course/courseActions';
import {
  getMaterials,
  postCourseImage,
  publishCourse,
  unpublishCourse,
  updateCourse,
} from 'configs/redux/reducers/myCourse/myCourseActions';
import {
  connect,
  EvaIcon,
  React,
  TouchableOpacity,
  useEffect,
  useState,
  View,
} from 'libraries';
import {
  ActivityIndicator,
  Image,
  Keyboard,
  KeyboardAvoidingView,
  Platform,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  TextInput,
  TouchableNativeFeedback,
} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { launchImageLibrary } from 'react-native-image-picker';
import { Color } from 'utils';
import RNPickerSelect from 'react-native-picker-select';
import styles from './style';
import { TouchableWithoutFeedback } from 'react-native';

const CourseEdit = (props) => {
  const {
    navigation,
    route,
    isLoading,
    imageLoading,
    courseCategories,
    courseMaterials,
    dispatchGetCourseCatgories,
    dispatchUpdateCourse,
    dispatchFetchMaterials,
    dispatchPublishCourse,
    dispatchUnpublishCourse,
    dispatchUploadImage,
  } = props;
  const { data } = route.params;
  const [courseImage, setCourseImage] = useState(
    'https://akm-img-a-in.tosshub.com/indiatoday/images/story/201811/online-3412473_1920_1.jpeg',
  );
  const [readOnly, setReadOnly] = useState(true);
  const [readOnlyStyle, setReadOnlyStyle] = useState({
    backgroundColor: 'white',
  });
  const [title, setTitle] = useState('');
  const [category, setCategory] = useState('');
  const [desc, setDesc] = useState('');
  const [isPublish, setIsPublish] = useState(false);

  const GetCourseImage = () => {
    const { picture } = data;
    if (picture != null) {
      setCourseImage(`${base_url}/files/course/image/${picture}`);
    } else {
      setCourseImage(
        'https://akm-img-a-in.tosshub.com/indiatoday/images/story/201811/online-3412473_1920_1.jpeg',
      );
    }
  };

  const SetReadOnlyStyle = () => {
    if (readOnly) {
      setReadOnlyStyle({
        backgroundColor: '#ccc',
      });
    } else {
      setReadOnlyStyle({
        backgroundColor: 'white',
      });
    }
  };

  const setDetailBtnLabel = () => {
    return readOnly ? 'Edit' : 'Save';
  };

  const editHandler = async () => {
    if (!readOnly) {
      const payload = {
        body: {
          id: data.id,
          title: title,
          desc: desc,
        },
      };

      const response = await dispatchUpdateCourse(payload);
      // console.log(response);
    }

    setReadOnly(!readOnly);
  };

  const handleSelectImage = () => {
    const options = {
      mediaType: 'photo',
      quality: 1,
    };

    launchImageLibrary(options, (response) => {
      // Same code as in above section!
      const selectedImage = {
        uri: response.uri,
        type: response.type, // mime type
        name: response.fileName,
        size: response.fileSize,
      };
      setCourseImage(selectedImage.uri);
      // upload to server
      const form = new FormData();
      form.append('coursePicture', selectedImage);
      form.append('courseId', data.id);
      const payload = {
        body: form,
        type: 'form-data',
      };
      dispatchUploadImage(payload);
    });
  };

  const addCurriculumHandler = () => {
    navigation.navigate('MaterialPage', {
      course: data,
    });
  };

  const addExamHandler = () => {
    navigation.navigate('ExamPage', {
      course: data,
    });
  };

  const publishHandler = async () => {
    const payload = {
      body: {
        id: data.id,
      },
    };

    const response = await dispatchPublishCourse(payload);
    setIsPublish(response.isPublished);
  };

  const unpublishHandler = async () => {
    const payload = {
      body: {
        id: data.id,
      },
    };

    const response = await dispatchUnpublishCourse(payload);
    setIsPublish(response.isPublished);
  };

  // component
  const DropdownCategories = () => {
    const pickerSelectStyles = StyleSheet.create({
      inputIOS: {
        ...styles.textInput,
        ...readOnlyStyle,
      },
      inputAndroid: {
        ...styles.textInput,
        ...readOnlyStyle,
      },
    });

    return (
      <RNPickerSelect
        disabled={readOnly}
        value={category}
        style={pickerSelectStyles}
        onValueChange={(value) => setCategory(value)}
        items={courseCategories.data.map((item) => {
          return { label: item.name, value: item.id.toString() };
        })}
      />
    );
  };

  useEffect(() => {
    setTitle(data.title);
    setCategory(data.category.id.toString());
    setDesc(data.desc);
    setIsPublish(data.isPublished);
    GetCourseImage();
    dispatchGetCourseCatgories();

    const payload = {
      paramsId: data.id,
    };
    dispatchFetchMaterials(payload);
  }, []);

  useEffect(() => {
    SetReadOnlyStyle();
  }, [readOnly]);

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={styles.container}>
      {/* <TouchableWithoutFeedback onPress={Keyboard.dismiss}> */}
      <SafeAreaView style={styles.container}>
        <StatusBar
          translucent={true}
          backgroundColor={Color.backgroundColor}
          barStyle={'dark-content'}
        />
        <View style={styles.header}>
          <BackButton
            onPress={() => {
              navigation.goBack();
            }}
          />
          <Text bold style={styles.headerLabel}>
            {data.title}
          </Text>
        </View>
        <ScrollView style={styles.body} showsVerticalScrollIndicator={false}>
          <View style={styles.imageContainer}>
            <Image source={{ uri: courseImage }} style={styles.courseImage} />
            <TouchableOpacity
              style={styles.uploadImageBtn}
              opacity={0.3}
              onPress={handleSelectImage}>
              <EvaIcon
                name="upload-outline"
                width={24}
                height={24}
                fill={'black'}
              />
            </TouchableOpacity>
            {imageLoading && (
              <View style={styles.imageLoading}>
                <ActivityIndicator size="large" />
              </View>
            )}
          </View>

          <View style={styles.subTitleContainer}>
            <Text style={styles.subTitle}>Course Detail</Text>
          </View>
          <View style={styles.form}>
            <View style={styles.courseInformation}>
              <View style={styles.courseInformationGroup}>
                <EvaIcon
                  name={
                    isPublish ? 'checkmark-circle-outline' : 'slash-outline'
                  }
                  width={24}
                  height={24}
                  fill={isPublish ? 'green' : 'red'}
                />
                <Text style={styles.courseAttr}>
                  {isPublish ? 'Published' : 'Not Published'}
                </Text>
              </View>
              <View style={styles.courseInformationGroup}>
                <EvaIcon
                  name="people-outline"
                  width={24}
                  height={24}
                  fill={Color.primaryColor}
                />
                <Text style={styles.courseAttr}>
                  Subscriber : {data.subscriberCount}
                </Text>
              </View>
            </View>

            <View style={styles.formGroup}>
              <Text style={styles.courseAttr}>Name</Text>
              <TextInput
                style={[styles.textInput, readOnlyStyle]}
                placeholder={'Course Name'}
                value={title}
                onChangeText={(value) => setTitle(value)}
                editable={!readOnly}
              />
            </View>

            <View style={styles.formGroup}>
              <Text style={styles.courseAttr}>Category</Text>
              {/* <TextInput
                  style={[styles.textInput, readOnlyStyle]}
                  placeholder={'Course Category'}
                  value={category}
                  onChangeText={(value) => setCategory(value)}
                  editable={!readOnly}
                /> */}
              <DropdownCategories />
            </View>

            <View style={styles.formGroup}>
              <Text style={styles.courseAttr}>Description</Text>
              <TextInput
                style={[styles.textInput, styles.textArea, readOnlyStyle]}
                placeholder={'Course Name'}
                value={desc}
                onChangeText={(value) => setDesc(value)}
                multiline={true}
                textAlignVertical="top"
                editable={!readOnly}
                scrollEnabled={true}
                numberOfLines={10}
              />
            </View>

            {(data.subscriberCount === '0' ||
              data.subscriberCount == undefined) && (
              <Button
                title={setDetailBtnLabel()}
                buttonStyle={styles.editBtn}
                textStyle={styles.editBtnLabel}
                onPress={editHandler}
              />
            )}
          </View>

          <View style={styles.subTitleContainer}>
            <Text style={styles.subTitle}>Course Materials</Text>
          </View>
          <View style={styles.curriculumContainer}>
            <Button
              title={'See Materials'}
              buttonStyle={styles.editBtn}
              textStyle={styles.editBtnLabel}
              onPress={() => addCurriculumHandler()}
            />
          </View>

          <View style={styles.subTitleContainer}>
            <Text style={styles.subTitle}>Course Exam</Text>
          </View>
          <View style={styles.curriculumContainer}>
            <Button
              title={'See Exams'}
              buttonStyle={styles.editBtn}
              textStyle={styles.editBtnLabel}
              onPress={() => addExamHandler()}
            />
          </View>
          <View style={[styles.subTitleContainer]}>
            <Text style={styles.subTitle}>Action</Text>
          </View>
          <View style={styles.actionContainer}>
            {courseMaterials.length === 0 ? (
              <View style={styles.emptyMaterialContainer}>
                <Text>Can't publish, you don't have any materials</Text>
              </View>
            ) : !isPublish ? (
              <Button
                title={'Publish Course'}
                buttonStyle={{ ...styles.editBtn, ...styles.yellowBtn }}
                textStyle={styles.editBtnLabel}
                onPress={publishHandler}
                isLoading={isLoading}
              />
            ) : (
              <Button
                title={'Unpublish Course'}
                buttonStyle={{ ...styles.editBtn, ...styles.redBtn }}
                textStyle={styles.editBtnLabel}
                onPress={unpublishHandler}
                isLoading={isLoading}
              />
            )}
          </View>
        </ScrollView>
      </SafeAreaView>
      {/* </TouchableWithoutFeedback> */}
    </KeyboardAvoidingView>
  );
};

function mapStateToProps(state) {
  return {
    //isi state dari reducer
    courseCategories: state.courseStore.course_category,
    courseMaterials: state.myCourseStore.courseMaterials,
    isLoading: state.myCourseStore.isLoading,
    imageLoading: state.myCourseStore.imageLoading,
    error: state.myCourseStore.error,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    //isi dispatch
    dispatchGetCourseCatgories: () => dispatch(getCourseCategory()),
    dispatchFetchMaterials: (payload) => dispatch(getMaterials(payload)),
    dispatchUpdateCourse: (payload) => dispatch(updateCourse(payload)),
    dispatchUploadImage: (payload) => dispatch(postCourseImage(payload)),
    dispatchPublishCourse: (payload) => dispatch(publishCourse(payload)),
    dispatchUnpublishCourse: (payload) => dispatch(unpublishCourse(payload)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CourseEdit);
