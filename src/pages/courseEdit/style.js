import { StyleSheet } from 'libraries';
import { StatusBar } from 'react-native';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import { Color, scale } from 'utils';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Color.backgroundColor,
  },
  header: {
    marginTop: StatusBar.currentHeight,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: scale(10),
    marginBottom: scale(10),
  },
  headerLabel: {
    fontSize: scale(25),
    color: Color.primaryColor,
    marginLeft: scale(10),
    textTransform: 'capitalize',
  },
  body: { flex: 1, width: '100%' },
  courseImage: {
    width: '100%',
    aspectRatio: 16 / 9,
  },
  uploadImageBtn: {
    backgroundColor: 'gray',
    position: 'absolute',
    bottom: scale(5),
    right: scale(5),
    width: scale(30),
    height: scale(30),
    borderRadius: scale(60),
    justifyContent: 'center',
    alignItems: 'center',
  },
  subTitleContainer: {
    backgroundColor: Color.primaryColorLighten,
    justifyContent: 'center',
    alignItems: 'center',
    padding: scale(10),
  },
  subTitle: {
    fontSize: scale(16),
    fontWeight: 'bold',
    marginLeft: scale(10),
  },
  form: {
    padding: scale(10),
    marginBottom: scale(10),
  },
  courseAttr: {
    fontSize: scale(16),
    fontWeight: '400',
  },
  textInput: {
    backgroundColor: 'white',
    borderRadius: scale(5),
    padding: scale(10),
    marginBottom: scale(5),
    // shadowColor: '#000',
    // shadowOffset: {
    //   width: 0,
    //   height: 2,
    // },
    // shadowOpacity: 0.25,
    // shadowRadius: 3.84,
    // elevation: 5,
  },
  textArea: {
    // height: scale(200),
    minHeight: scale(200),
  },

  curriculumTitle: {
    fontSize: scale(16),
    fontWeight: 'bold',
  },
  curriculumContainer: {
    padding: scale(10),
    width: '100%',
  },
  curriculumEmptyContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  addDetailBtn: {
    backgroundColor: Color.primaryColor,
    padding: scale(5),
    paddingHorizontal: scale(10),
    borderRadius: scale(100),
    marginTop: scale(10),
    width: scale(150),
  },
  addDetailBtnLabel: {
    color: 'white',
    textAlign: 'center',
  },
  editBtn: {
    backgroundColor: Color.primaryColor,
    marginTop: scale(10),
  },
  editBtnLabel: {
    color: 'white',
    fontWeight: '400',
    fontSize: scale(14),
  },
  // course image
  imageLoading: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'black',
    opacity: 0.7,
    justifyContent: 'center',
    alignItems: 'center',
  },
  // curriculum
  curriculumCards: {
    flex: 1,
  },
  // publish button
  emptyMaterialContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  emptyText: {
    padding: scale(20),
  },
  actionContainer: {
    padding: scale(10),
    width: '100%',
  },
  yellowBtn: {
    backgroundColor: Color.yellowStar,
  },
  redBtn: {
    backgroundColor: Color.red,
  },
  // course information
  courseInformationGroup: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: scale(10),
  },
});

export default styles;
