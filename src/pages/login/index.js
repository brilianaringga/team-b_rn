import {
  React,
  useEffect,
  useState,
  connect,
  View,
  Image,
  SafeAreaView,
  StatusBar,
} from 'libraries';
import { Text, Button, TextInput } from 'components';
import styles from './style';
// import { scale } from 'utils';
import LottieView from 'lottie-react-native';
import { Form, Input, Item, Label } from 'native-base';
// import { appString, Color, scale } from 'utils';
// import {
//   exampleAction,
//   getDataApi,
// } from 'configs/redux/reducers/example/exampleActions';
// import { Form, Input, Item, Label } from 'native-base';
import { LoginAction } from 'configs/redux/reducers/auth/authActions';
import Toast from 'react-native-toast-message';
import { Color, scale } from 'utils';

const LoginPage = (props) => {
  const { dispatchLogin, navigation, isLoading } = props;

  const [MyUsername, setMyUsername] = useState(null);
  const [MyPassword, setMyPassword] = useState(null);
  const [isValid, setIsValid] = useState({
    username: true,
    password: true,
  });

  useEffect(() => {}, []);

  useEffect(() => {}, []);

  const validate = (item) => {
    if (item === 'username') {
      if (MyUsername == null || MyUsername === '') {
        setIsValid({
          ...isValid,
          username: false,
        });
      } else {
        setIsValid({
          ...isValid,
          username: true,
        });
      }
    }

    if (item === 'password') {
      if (MyPassword == null || MyPassword === '') {
        setIsValid({
          ...isValid,
          password: false,
        });
      } else {
        setIsValid({
          ...isValid,
          password: true,
        });
      }
    }
  };

  const doLogin = () => {
    console.log('ini username', MyUsername);
    let username = true;
    let password = true;
    if (MyUsername == null || MyUsername === '') {
      username = false;
    }
    if (MyPassword == null || MyPassword === '') {
      password = false;
    } else {
      const payload = {
        body: {
          username: MyUsername,
          password: MyPassword,
        },
      };

      dispatchLogin(payload);
    }
    setIsValid({
      ...isValid,
      username,
      password,
    });
  };

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar
        backgroundColor={Color.backgroundColor}
        barStyle={'dark-content'}
      />
      <View style={[styles.style1, { marginTop: StatusBar.currentHeight }]}>
        <Text style={styles.style2}>Welcome Back!</Text>
      </View>

      <View style={styles.style3}>
        <LottieView
          style={styles.style4}
          source={require('../../assets/animasi/training.json')}
          autoPlay
          loop
        />

        <View style={{ marginTop: scale(50) }}>
          <View
            style={[
              styles.style17,
              {
                // borderColor: !isValid.username ? 'red' : Color.primaryColor,
              },
            ]}>
            <Image
              style={styles.style18}
              source={require('../../assets/images/nama.png')}
            />
            <TextInput
              placeholder={'Username'}
              autoCapitalize="none"
              color="#2D8989"
              onChangeText={(username) => {
                setMyUsername(username);
                //reset
                setIsValid({
                  ...isValid,
                  username: true,
                  password: true,
                });
              }}
              value={MyUsername}
              style={{ width: '100%', fontSize: scale(15) }}
            />
          </View>
          {!isValid.username ? (
            <Text style={{ color: 'red', marginLeft: scale(30) }}>
              Username cannot blank !
            </Text>
          ) : (
            <View />
          )}

          <View
            style={[
              styles.style19,
              {
                // borderColor: !isValid.password ? 'red' : Color.primaryColor,
              },
            ]}>
            <Image
              style={styles.style20}
              source={require('../../assets/images/password.png')}
            />
            <TextInput
              placeholder={'Password'}
              autoCapitalize="none"
              secureTextEntry={true}
              color="#2D8989"
              onChangeText={(password) => {
                setMyPassword(password);
                //reset
                setIsValid({
                  ...isValid,
                  username: true,
                  password: true,
                });
              }}
              value={MyPassword}
              style={{ width: '100%', fontSize: scale(15) }}
            />
          </View>
          {!isValid.password ? (
            <Text style={{ color: 'red', marginLeft: scale(30) }}>
              Password cannot blank !
            </Text>
          ) : (
            <View />
          )}
          {/* <Form style={styles.style5}>
            <Item style={styles.style6} floatingLabel>
              <Label style={styles.style7}>Username</Label>
              <Input
                style={styles.style8}
                placeholderTextColor="white"
                autoCapitalize="none"
                onChangeText={(username) => setMyUsername(username)}
                value={MyUsername}
              />
            </Item>
            <Item style={styles.style9} floatingLabel>
              <Label style={styles.style10}>Password</Label>
              <Input
                style={styles.style11}
                placeholderTextColor="white"
                autoCapitalize="none"
                secureTextEntry={true}
                onChangeText={(password) => setMyPassword(password)}
                value={MyPassword}
              />
            </Item>
          </Form> */}
        </View>

        <View style={styles.style12}>
          <Button
            buttonStyle={styles.style15}
            textStyle={styles.style16}
            title={'SIGN IN'}
            onPress={() => doLogin()}
            isLoading={isLoading}
          />

          <Text style={styles.style13}>
            Don't have an account?
            <Text
              onPress={() => {
                setIsValid({
                  ...isValid,
                  username: true,
                  password: true,
                });
                navigation.navigate('RegisterPage');
              }}
              style={styles.style14}>
              {' '}
              Sign UP
            </Text>
          </Text>
        </View>
      </View>

      <Toast ref={(ref) => Toast.setRef(ref)} />
    </SafeAreaView>
  );
};

function mapStateToProps(state) {
  return {
    isLoading: state.authStore.isLoading,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatchLogin: (payload) => dispatch(LoginAction(payload)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
