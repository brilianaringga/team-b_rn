import { StatusBar } from 'react-native';
import { StyleSheet } from 'react-native';
import { Color, scale } from 'utils';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Color.backgroundColor,
  },
  header: {
    marginTop: StatusBar.currentHeight,
    padding: scale(5),
  },
  contentContainer: {
    flexGrow: 1,
    paddingBottom: scale(100),
  },
  content: {
    flex: 1,
  },
  title: {
    backgroundColor: Color.primaryColor,
    padding: scale(10),
  },
  titleText: {
    fontSize: scale(16),
    fontWeight: 'bold',
    color: 'white',
  },
  // exam card
  examContainer: {
    flex: 1,
    // backgroundColor: 'yellow',
  },
  emptyExam: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  emptyExamLabel: {
    fontSize: scale(16),
    fontWeight: '400',
  },
  // bottom action
  bottomActionContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'white',
    padding: scale(10),
    borderTopLeftRadius: scale(5),
    borderTopRightRadius: scale(5),
  },
  btnStyle: {
    backgroundColor: Color.primaryColor,
  },
  btnStyleLabel: {
    fontSize: scale(16),
    fontWeight: '400',
    color: 'white',
  },
});

export default styles;
