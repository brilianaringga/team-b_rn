import { StatusBar } from 'react-native';
import { StyleSheet } from 'react-native';
import { Color, scale, verticalScale } from 'utils';
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  safeArea: {
    flex: 1,
    backgroundColor: Color.backgroundColor,
  },
  contentContainer: {
    marginTop: StatusBar.currentHeight,
    flex: 1,
    paddingHorizontal: scale(14),
  },
  // search
  searchContainer: {
    flex: 1,
    flexDirection: 'row',
    marginBottom: verticalScale(5),
  },
  searchLabel: {
    color: 'gray',
    fontSize: verticalScale(30),
    marginRight: scale(5),
  },
  searchLabel2: {
    color: Color.primaryColor,
    fontWeight: 'bold',
    fontSize: verticalScale(30),
  },
  search: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: verticalScale(50),
    paddingHorizontal: scale(16),
    marginBottom: verticalScale(5),
    backgroundColor: 'white',
    // shadowColor: '#000',
    // shadowOffset: {
    //   width: 0,
    //   height: 2,
    // },
    // shadowOpacity: 0.25,
    // shadowRadius: 3.84,
    // elevation: 5,
  },
  searchIcon: { paddingHorizontal: scale(10), marginRight: scale(5) },
  searchTextInput: {
    flex: 1,
    // height: scale(30),
    borderRadius: scale(25),
    color: Color.primaryColor,
    fontSize: scale(14),
    paddingVertical: scale(10),
  },
  subTitle: {
    fontSize: scale(18),
    fontWeight: '500',
    justifyContent: 'flex-start',
  },
  categoryView: {
    flex: 1,
    justifyContent: 'space-evenly',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  categoryContainer: {
    width: scale(165),
    justifyContent: 'center',
  },
});

export default styles;
