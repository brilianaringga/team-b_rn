/* eslint-disable react-hooks/exhaustive-deps */
import {
  SearchCategoryCard,
  SearchPopularTrainer,
  Text,
  TextInput,
} from 'components';
import {
  fetchCategories,
  fetchPopularSearch,
  fetchPopularTrainer,
} from 'configs/redux/reducers/search/searchActions';
import {
  connect,
  EvaIcon,
  KeyboardAvoidingView,
  Platform,
  React,
  SafeAreaView,
  StatusBar,
  useEffect,
  useState,
  View,
} from 'libraries';
import { ScrollView } from 'react-native';
import { Color } from 'utils';
import styles from './style';

const SearchPage = (props) => {
  const {
    navigation,
    popular_search: popularSearch,
    popular_trainer: popularTrainer,
    categories,
    isLoading,
    dispatchGetPopularSearch,
    dispatchGetPopularTrainer,
    dispatchGetCategories,
  } = props;
  const [search, setSearch] = useState('');

  useEffect(() => {
    dispatchGetPopularSearch();
    dispatchGetPopularTrainer();
    dispatchGetCategories();
  }, []);

  // return view
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={styles.container}>
      <SafeAreaView style={styles.safeArea}>
        <StatusBar
          translucent
          barStyle="dark-content"
          backgroundColor={Color.backgroundColor}
        />

        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.contentContainer}>
            <View style={styles.searchContainer}>
              <Text style={styles.searchLabel}>Search</Text>
              <Text style={styles.searchLabel2}>Courses</Text>
            </View>

            <View style={styles.search}>
              <EvaIcon
                name="search"
                width={24}
                height={24}
                fill={Color.primaryColor}
                style={styles.searchIcon}
              />
              <TextInput
                inlineImageLeft="search_icon"
                placeholder="Search courses or trainer for you ... "
                placeholderTextColor={Color.primaryColorLighten}
                numberOfLines={1}
                returnKeyType="search"
                value={search}
                onChangeText={(value) => setSearch(value)}
                style={styles.searchTextInput}
                onSubmitEditing={() => {
                  navigation.navigate('SearchResultPage', { keyword: search });
                }}
              />
            </View>

            {/* <View>
              <Text style={[styles.subTitle]}>Popular Search</Text>
              <SearchPopularSearch popularSearch={popularSearch} />
            </View> */}

            <View>
              <Text style={[styles.subTitle]}>Popular Trainer</Text>
              <SearchPopularTrainer
                popularTrainer={popularTrainer}
                navigation={navigation}
              />
            </View>

            <View>
              <Text style={[styles.subTitle]}>Browse Categories</Text>
              <View style={styles.categoryView}>
                {categories.map((item, index) => {
                  return (
                    <View style={styles.categoryContainer} key={item.id}>
                      <SearchCategoryCard
                        item={item}
                        onPress={() => {
                          navigation.navigate('AllCoursePage', {
                            title: item.name,
                            type: 'category',
                            id: item.id,
                          });
                        }}
                      />
                    </View>
                  );
                })}
              </View>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
};

function mapStateToProps(state) {
  return {
    //isi state dari reducer
    popular_search: state.searchStore.popular_search_data,
    popular_trainer: state.searchStore.popular_trainer_data,
    categories: state.searchStore.categories_data,
    isLoading: state.searchStore.isLoading,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    //isi dispatch
    dispatchGetPopularSearch: () => dispatch(fetchPopularSearch()),
    dispatchGetPopularTrainer: () => dispatch(fetchPopularTrainer()),
    dispatchGetCategories: () => dispatch(fetchCategories()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchPage);
