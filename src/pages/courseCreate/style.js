import { StyleSheet } from 'libraries';
import { Color, scale, shadow } from 'utils';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Color.backgroundColor,
    // paddingTop: StatusBar.currentHeight,
  },
  contentHeader: {
    padding: scale(10),
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
  },
  headerTitle: {
    fontSize: scale(25),
    color: Color.primaryColor,
    marginLeft: scale(10),
    textTransform: 'capitalize',
  },
  createForm: { flexGrow: 1, width: '100%', padding: scale(15) },
  textCategoryContainer: {},
  textCategory: {
    backgroundColor: 'white',
    textAlignVertical: 'center',
    padding: scale(10),
    borderRadius: scale(5),
    // shadowColor: '#000',
    // shadowOffset: {
    //   width: 0,
    //   height: 2,
    // },
    // shadowOpacity: 0.25,
    // shadowRadius: 3.84,
    // elevation: 5,
  },
  inputCourseName: {
    backgroundColor: 'white',
    textAlignVertical: 'center',
    padding: scale(10),
    borderRadius: scale(5),
    // shadowColor: '#000',
    // shadowOffset: {
    //   width: 0,
    //   height: 2,
    // },
    // shadowOpacity: 0.25,
    // shadowRadius: 3.84,
    // elevation: 5,
  },
  inputDecription: {
    backgroundColor: 'white',
    borderRadius: scale(5),
    minHeight: scale(50),
    textAlignVertical: 'top',
    padding: scale(20),
    // paddingVertical: scale(20),
    // shadowColor: '#000',
    // shadowOffset: {
    //   width: 0,
    //   height: 2,
    // },
    // shadowOpacity: 0.25,
    // shadowRadius: 3.84,
    // elevation: 5,
  },
  inputLabel: {
    fontSize: scale(18),
    fontWeight: 'bold',
    marginBottom: scale(5),
  },
  bottomSheetContent: {
    height: '100%',
    // flex: 1,
    padding: 15,
    backgroundColor: 'white',
    paddingTop: 20,
  },
  shadowContainer: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: '#000',
  },
  header: {
    width: '100%',
    height: 10,
    backgroundColor: 'white',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    padding: scale(10),
  },
  panelHeader: {
    alignItems: 'center',
  },
  panelHandle: {
    width: 40,
    height: 4,
    borderRadius: 4,
    backgroundColor: '#00000040',
    marginBottom: 10,
  },
  nextButton: {
    backgroundColor: Color.primaryColor,
  },
  nextBtnLabel: {
    color: 'white',
    fontSize: scale(16),
    fontWeight: '400',
  },
  scrollViewContainer: {
    flexGrow: 1,
  },
  coureseImageContainer: {
    flex: 1,
    backgroundColor: 'green',
    ...shadow,
  },
  courseImage: { width: '100%', aspectRatio: 16 / 9, borderRadius: scale(5) },
});

export default styles;
