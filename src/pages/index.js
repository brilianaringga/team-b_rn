import CoursePage from './course';
import CourseDetailPage from './courseDetail';
import CourseEditPage from './courseEdit';
import CourseCreatePage from './courseCreate';
import MaterialPage from './material';
import MaterialAddPage from './materialAdd';
import ExamCreatePage from './examCreate';
import ExamPage from './exam';
import ExamplePage from './examplePage';
import HomePage from './home';
import LoginPage from './login';
import ProfilePage from './profile';
import RegisterPage from './register';
import SearchPage from './search';
import SearchResultPage from './searchResult';
import SplashScreen from './splash';
import TrainerProfilePage from './trainerProfile';
import WishlistPage from './wishlist';
import EditPage from './profile/edit';
import AvatarPage from './profile/avatar';
import AllCoursePage from './allCourse';
import PreferredCategoryPage from './preferredCategory';
import NewProfilePage from './profile/newprofile';
import VerificationPage from './profile/otp';
import OnboardingPage from './onboarding';
import CourseScorePage from './courseScore';
import CourseMaterialPage from './courseMaterialPage';
import NotificationPage from './notification';
import MaterialView from './materialView';
import ReviewPage from './reviewPage';
import TakeExamPage from './takeExamPage';

export {
  ExamplePage,
  CoursePage,
  CourseDetailPage,
  CourseEditPage,
  CourseCreatePage,
  MaterialAddPage,
  ExamCreatePage,
  TrainerProfilePage,
  WishlistPage,
  LoginPage,
  SearchPage,
  SearchResultPage,
  ProfilePage,
  RegisterPage,
  SplashScreen,
  HomePage,
  EditPage,
  AvatarPage,
  AllCoursePage,
  PreferredCategoryPage,
  NewProfilePage,
  VerificationPage,
  OnboardingPage,
  CourseScorePage,
  MaterialPage,
  ExamPage,
  CourseMaterialPage,
  NotificationPage,
  MaterialView,
  ReviewPage,
  TakeExamPage,
};
