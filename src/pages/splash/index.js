import {
  React,
  useEffect,
  useState,
  connect,
  View,
  StatusBar,
  Image,
  Platform,
  Modal,
  Linking,
} from 'libraries';
import { Button, Text } from 'components';
import styles from './style';
import { appString, scale } from 'utils';
import {
  exampleAction,
  getDataApi,
} from 'configs/redux/reducers/example/exampleActions';
import LottieView from 'lottie-react-native';
import { Color } from '../../utils/styles/colors';
import AsyncStorage from '@react-native-community/async-storage';
import { checkTokenValidation } from 'configs/redux/reducers/auth/authActions';
import messaging from '@react-native-firebase/messaging';
import API from 'configs/api';
import IMG from 'assets/images';
import VersionInfo from 'react-native-version-info';
import { base_url } from 'configs/api/url';
import { CommonActions } from '@react-navigation/native';

const SplashScreen = (props) => {
  const { navigation, dispatchCheckToken } = props;
  const [loading, setLoading] = useState(true);
  const [initialRoute, setInitialRoute] = useState('NotificationPage');
  const [visibleModal, setVisibleModal] = useState(false);
  const [updateMessage, setUpdateMessage] = useState([]);

  useEffect(() => {
    checkValidation();
  }, []);

  const navigationReset = (page) => {
    navigation.dispatch(
      CommonActions.reset({
        index: 0,
        routes: [{ name: page }],
      }),
    );
  };
  const validationAction = async () => {
    setVisibleModal(false);
    const token = await AsyncStorage.getItem('@token');
    console.log('ini berarer token', token);
    if (token === null) {
      const isOnboard = await AsyncStorage.getItem('@onboarding');
      if (isOnboard === 'done') {
        navigationReset('LoginPage');
      } else {
        navigationReset('OnboardingPage');
      }
    } else {
      try {
        dispatchCheckToken();
      } catch (e) {
        console.log(e);
      }
    }
  };

  const checkValidation = async () => {
    try {
      const curVersion = `${VersionInfo.appVersion}.${VersionInfo.buildVersion}`;

      const payload = {
        package: VersionInfo.bundleIdentifier,
        type: Platform.OS === 'ios' ? 'ios' : 'android',
        version: curVersion.split('.').map(function (item) {
          return parseInt(item);
        }),
      };
      const checkVersion = await API.checkVersion({
        body: payload,
      });
      console.log(checkVersion);
      if (checkVersion) {
        if (checkVersion.data.availableUpdate.length > 0) {
          console.log(checkVersion.data.availableUpdate);
          setUpdateMessage(checkVersion.data.availableUpdate);
          setVisibleModal(true);
        } else {
          validationAction();
        }
      }
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <View style={styles.container}>
      <StatusBar
        backgroundColor={Color.backgroundColor}
        barStyle={'dark-content'}
      />
      {/* <LottieView
        source={require('../../assets/animasi/learning.json')}
        autoPlay
        loop
      /> */}
      <Image
        source={IMG.logo}
        style={{
          height: scale(100),
          aspectRatio: 16 / 9,
          resizeMode: 'contain',
        }}
      />
      <Text bold style={{ fontSize: scale(30), color: Color.primaryColor }}>
        ALITA
      </Text>
      <Text
        style={{ fontSize: scale(20), color: '#0074B5', fontWeight: '500' }}>
        All in Training Application
      </Text>
      <Modal visible={visibleModal} animationType="slide" transparent={true}>
        <View style={styles.modalContainer}>
          <View style={styles.modalContent}>
            <Text bold style={{ fontSize: scale(17) }}>
              NEW UPDATE
            </Text>
            {updateMessage.length > 0 ? (
              <View>
                <Text style={{ padding: scale(15) }}>
                  {updateMessage[0].message}
                </Text>
                {updateMessage[0].isMandatory ? (
                  <Button
                    title={'UPDATE'}
                    buttonStyle={{
                      paddingVertical: scale(5),
                    }}
                    onPress={() => {
                      Linking.openURL(
                        `${base_url}/files/apps/android/${updateMessage[0].urlDownload}`,
                      ).catch((err) => console.error('An error occurred', err));
                    }}
                  />
                ) : (
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      justifyContent: 'space-evenly',
                    }}>
                    <Button
                      title={'CONTINUE'}
                      buttonStyle={{
                        paddingVertical: scale(5),
                        backgroundColor: 'orange',
                      }}
                      onPress={() => {
                        validationAction();
                      }}
                    />
                    <Button
                      title={'UPDATE'}
                      buttonStyle={{ paddingVertical: scale(5) }}
                      onPress={() => {
                        Linking.openURL(
                          `${base_url}/files/apps/android/${updateMessage[0].urlDownload}`,
                        ).catch((err) =>
                          console.error('An error occurred', err),
                        );
                      }}
                    />
                  </View>
                )}
              </View>
            ) : (
              <View />
            )}
          </View>
        </View>
      </Modal>
    </View>
  );
};

function mapDispatchToProps(dispatch) {
  return {
    dispatchCheckToken: () => dispatch(checkTokenValidation()),
  };
}

export default connect(null, mapDispatchToProps)(SplashScreen);
