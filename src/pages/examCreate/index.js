import { Button, PageHeader, Text, TextInput } from 'components';
import {
  fetchMyCourse,
  getExams,
  postExam,
} from 'configs/redux/reducers/myCourse/myCourseActions';
import { connect, EvaIcon, React, View } from 'libraries';
import { useEffect, useState } from 'react';
import { Platform, TouchableOpacity } from 'react-native';
import {
  KeyboardAvoidingView,
  Pressable,
  SafeAreaView,
  ScrollView,
} from 'react-native';
import { Color, scale } from 'utils';
import styles from './style';

const ExamCreatePage = ({ route, navigation, ...props }) => {
  // properties
  const { course } = route.params;
  const { courseExams, isLoading } = props;
  const { dispatchGetMyCourses, dispatchGetExam, dispatchPostExam } = props;
  const [questionType] = useState(['single', 'multiple']);
  const [selectedQuestionType, setSelectedQuestionType] = useState('single');
  const [question, setQuestion] = useState('');
  const [answer, setAnswer] = useState(['a']);
  const [choices, setChoices] = useState([
    { choice: 'Pilihan 1' },
    { choice: 'Pilihan 2' },
    { choice: 'Pilihan 3' },
    { choice: 'Pilihan 4' },
  ]);

  // function
  const handlerBackBtn = () => navigation.goBack();
  const addOption = () => {
    const newOption = { choice: 'Pilihan Baru' };
    setChoices([...choices, newOption]);
  };

  const deleteOption = (index) => {
    let tempOptions = [...choices];
    tempOptions.splice(index, 1);
    setChoices(tempOptions);
  };

  const setQuestionAnswer = (optionId) => {
    if (selectedQuestionType == 'single') {
      setAnswer([optionId]);
    }
    if (selectedQuestionType == 'multiple') {
      if (answer.includes(optionId)) {
        let tempAnswer = [...answer];
        const selected = tempAnswer.findIndex((el) => el == optionId);
        tempAnswer.splice(selected, 1);
        setAnswer(tempAnswer);
      } else {
        setAnswer([...answer, optionId]);
      }
    }
  };

  const handlerChoiceTextChange = (value, index) => {
    const tempChoices = [...choices];
    // let thisElement = tempChoices[index];
    // thisElement = value;
    tempChoices[index] = { choice: value };
    setChoices([...tempChoices]);
  };

  const handlerSubmit = async () => {
    try {
      const prepChoices = {};
      choices.map((item, index) => {
        const optionCode = String.fromCharCode(index + 97);
        prepChoices[optionCode] = item.choice;
      });
      const data = {
        questionId: courseExams.length + 1,
        type: selectedQuestionType,
        question: question,
        choices: prepChoices,
        answer: answer,
      };

      let payload = {
        body: {
          courseId: course.id,
          questions: [...courseExams, data],
        },
      };

      console.log([...courseExams, data]);

      await dispatchPostExam(payload);
      await dispatchGetMyCourses();
      payload = {
        paramsId: course.id,
      };
      await dispatchGetExam(payload);
      navigation.goBack();
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={styles.container}
      enabled>
      <SafeAreaView style={styles.safearea}>
        <View style={styles.header}>
          <PageHeader title={course.title} backAction={handlerBackBtn} />
        </View>
        <View style={styles.title}>
          <Text style={styles.titleText}>Create Exam</Text>
        </View>
        <ScrollView
          contentContainerStyle={{ paddingBottom: scale(100) }}
          style={styles.content}>
          {/* Question Type */}
          <Text style={styles.formLabel}>Question Type</Text>
          <View style={styles.questionTypeContainer}>
            {questionType.map((item, index) => {
              return (
                <Pressable
                  key={item}
                  style={[
                    styles.questionType,
                    selectedQuestionType === item
                      ? styles.questionTypeSelected
                      : {},
                  ]}
                  onPress={() => {
                    setSelectedQuestionType(item);
                  }}>
                  <Text
                    style={[
                      styles.questionTypeLabel,
                      selectedQuestionType === item
                        ? styles.questionTypeLabelSelected
                        : {},
                    ]}>
                    {item}
                  </Text>
                </Pressable>
              );
            })}
          </View>
          {/* Question */}
          <Text style={styles.formLabel}>Question</Text>
          <TextInput
            style={[styles.textInput]}
            placeholder={'Question?'}
            value={question}
            onChangeText={(value) => setQuestion(value)}
          />
          {/* Choices */}
          <Text style={styles.formLabel}>Choices</Text>
          <View style={styles.choicesContainer}>
            {/* <ScrollView> */}
            {choices.map((item, index) => {
              const optionCode = String.fromCharCode(index + 97);
              const isAnswer = answer.includes(optionCode);

              return (
                <View key={index} style={styles.choices}>
                  <Text>
                    {optionCode.toUpperCase()}
                    {'. '}
                  </Text>
                  <TextInput
                    style={[styles.textInput, styles.textInputChoices]}
                    value={item.choice}
                    onChangeText={(value) =>
                      handlerChoiceTextChange(value, index)
                    }
                  />
                  <TouchableOpacity
                    style={[
                      styles.optionAnswer,
                      isAnswer ? { backgroundColor: Color.primaryColor } : {},
                    ]}
                    onPress={() => setQuestionAnswer(optionCode)}>
                    <EvaIcon
                      name={'checkmark-circle-outline'}
                      width={24}
                      height={24}
                      fill={isAnswer ? 'white' : Color.primaryColor}
                    />
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={styles.deleteOptions}
                    onPress={() => deleteOption(index)}>
                    <EvaIcon
                      name={'trash-outline'}
                      width={24}
                      height={24}
                      fill={'white'}
                    />
                  </TouchableOpacity>
                </View>
              );
            })}
            {/* </ScrollView> */}
            {/* answer */}
            <Text>Answers : {answer.sort().join(', ').toUpperCase()}</Text>
            {/* action */}
            <Button
              title={'add option'}
              buttonStyle={{ ...styles.btnStyle, backgroundColor: 'orange' }}
              textStyle={{ ...styles.btnStyleLabel, color: 'black' }}
              onPress={addOption}
            />
            <Button
              title={'save'}
              isLoading={isLoading}
              buttonStyle={styles.btnStyle}
              textStyle={styles.btnStyleLabel}
              onPress={handlerSubmit}
            />
          </View>
        </ScrollView>
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
};

function mapStateToProps(state) {
  return {
    //isi state dari reducer
    courseExams: state.myCourseStore.courseExams,
    isLoading: state.myCourseStore.isLoading,
    error: state.myCourseStore.error,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    //isi dispatch
    dispatchGetMyCourses: () => dispatch(fetchMyCourse()),
    dispatchGetExam: (payload) => dispatch(getExams(payload)),
    dispatchPostExam: (payload) => dispatch(postExam(payload)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ExamCreatePage);
