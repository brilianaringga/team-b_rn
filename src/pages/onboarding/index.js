import {
  React,
  useEffect,
  useState,
  connect,
  View,
  Image,
  EvaIcon,
  StatusBar,
  TouchableOpacity,
} from 'libraries';
import styles from './style';
import { Color, scale } from 'utils';
import Onboarding from 'react-native-onboarding-swiper';
import AsyncStorage from '@react-native-community/async-storage';
import API from 'configs/api';
import { base_url } from 'configs/api/url';
import onboardingData2 from 'utils/data/onboarding';

const OnboardingPage = (props) => {
  const { navigation } = props;
  const [onboardingData, setOnboardingData] = useState([]);
  const [pages, setPages] = useState([]);

  const getOnboardingData = async () => {
    try {
      // const res = await API.getOnboarding();
      // if (res) {
      //   setOnboardingData(res.data.intro);
      // }
      setOnboardingData(onboardingData2);
    } catch (e) {}
  };

  useEffect(() => {
    getOnboardingData();
  }, []);

  useEffect(() => {
    if (onboardingData.length > 0) {
      let currData = [];
      onboardingData.forEach((item, index) => {
        currData.push(renderPage(item));
      });
      setPages(currData);
    }
  }, [onboardingData]);

  const onDone = () => {
    // User finished the introduction. Show real app through
    AsyncStorage.setItem('@onboarding', 'done');
    navigation.navigate('LoginPage');
  };

  const renderPage = (item) => {
    const newItem = {
      backgroundColor: Color.backgroundColor,
      image: <Image source={item.illustration} style={styles.illustration} />,
      title: item.title,
      subtitle: item.description,
    };
    return newItem;
  };

  return (
    <View style={styles.container}>
      <StatusBar
        translucent
        backgroundColor={'white'}
        barStyle={'dark-content'}
      />
      {pages.length > 0 ? (
        <Onboarding
          showSkip={false}
          titleStyles={styles.titleStyles}
          subTitleStyles={styles.subTitleStyles}
          DoneButtonComponent={() => (
            <TouchableOpacity onPress={() => onDone()}>
              <View style={styles.row} />
              <EvaIcon
                name={'checkmark-circle-outline'}
                height={scale(40)}
                width={scale(40)}
                fill={'green'}
                style={{ marginRight: scale(20) }}
              />
            </TouchableOpacity>
          )}
          pages={pages}
          bottomBarColor={'white'}
        />
      ) : (
        <View />
      )}
    </View>
  );
};

function mapStateToProps(state) {
  return {};
}

function mapDispatchToProps(dispatch) {
  return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(OnboardingPage);
