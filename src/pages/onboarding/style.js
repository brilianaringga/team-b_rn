import { StyleSheet } from 'react-native';
import { Color, scale, METRICS, FONTS } from 'utils';
const styles = StyleSheet.create({
  buttonCircle: {
    width: 40,
    height: 40,
    backgroundColor: 'rgba(0, 0, 0, .2)',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  slide: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 96, // Add padding to offset large buttons and pagination in bottom of page
  },
  image: {
    width: 320,
    height: 320,
    marginTop: 32,
  },
  title: {
    fontSize: 22,
    color: 'white',
    textAlign: 'center',
  },
  illustration: {
    aspectRatio: 4 / 3,
    height: scale(250),
    resizeMode: 'contain',
  },
  container: { flex: 1 },
  titleStyles: {
    fontWeight: 'bold',
    fontFamily: FONTS.brandonGrotesque,
    color: Color.primaryColor,
    fontSize: scale(30),
  },
  subTitleStyles: {
    fontFamily: FONTS.brandonGrotesque,
    fontSize: scale(17),
  },
  row: { flexDirection: 'row', alignItems: 'center' },
});

export default styles;
