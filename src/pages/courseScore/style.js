import { StyleSheet } from 'libraries';
import { Color, scale } from 'utils';
import { Dimensions } from 'react-native';

const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: '#2D8989'
    // alignItems: 'center'
  },
  filler: {
    height: windowHeight * 0.5,
    backgroundColor: '#000000',
    position: 'absolute',
    top: 0,
    left: 0
  },
  touchableArea: {
    backgroundColor: 'rgba(255,255,255, 0.9)',
    borderRadius: scale(4),
    padding: scale(5),
    flexDirection: 'row',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    marginHorizontal: scale(20),
    marginTop: scale(5),
    marginBottom: scale(10),
    minHeight: windowHeight * 0.6,
    justifyContent: 'center',
    alignItems: 'center'
  },
  imageContainer: {
    height: scale(100),
  },
  cardImage: {
    width: scale(150),
    height: scale(100),
    borderRadius: scale(10),
  },
  detailContainer: {
    flex: 1,
    padding: scale(20),
    // backgroundColor: 'green',
    justifyContent: 'space-between',
  },
  statusContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  publishLabel: {
    fontSize: scale(10),
    color: 'gray',
    marginLeft: 2
  },
  header: {
    fontSize: scale(30),
    fontWeight: 'bold',
    justifyContent: 'center',
    color: 'white'
  },
  name: {
    fontSize: scale(30),
    fontWeight: 'bold',
    justifyContent: 'center',
    textAlign: 'center'
  },
  title: {
    fontSize: scale(16),
    fontWeight: 'bold',
    justifyContent: 'center',
    textAlign: 'center'
  },
  subheader: {
    fontSize: scale(16),
    fontWeight: 'bold',
    justifyContent: 'center',
    color: 'white'
  },
  subtitle: {
    fontSize: scale(14),
    justifyContent: 'center',
    color: '#4d4d4d',
    textAlign: 'center'
  },
  desc: {
    fontSize: scale(10),
    fontWeight: '400',
    justifyContent: 'center',
    marginTop: 2
  },
  information: {
    alignSelf: 'flex-end',
    fontStyle: 'italic',
  },
});

export default styles;
