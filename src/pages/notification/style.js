import { StatusBar, StyleSheet } from 'react-native';
import { Color, scale, METRICS } from 'utils';
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Color.backgroundColor,
    paddingTop: StatusBar.currentHeight,
    paddingHorizontal: scale(15),
  },
  topBarWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: scale(10),
    marginBottom: scale(20),
  },
  backButton: {
    backgroundColor: Color.primaryColor,
    borderRadius: scale(50),
    padding: scale(4),
  },
  title: {
    fontSize: scale(25),
    color: Color.primaryColor,
    marginLeft: scale(10),
    textTransform: 'capitalize',
  },
  groupTitle: {
    fontSize: scale(20),
    fontWeight: 'bold',
    marginBottom: scale(15),
  },
  emptyNotifTitle: {
    fontSize: scale(20),
    textAlign: 'center',
    color: Color.primaryColor,
  },
  emptyNotifSub: { fontSize: scale(15) },
  emptyNotifWrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default styles;
