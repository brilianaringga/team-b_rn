import {
  React,
  connect,
  View,
  EvaIcon,
  Pressable,
  StatusBar,
  useEffect,
} from 'libraries';
import { NotificationCard, Text } from 'components';
import styles from './style';
import { Color, scale } from 'utils';
import { SafeAreaView, ScrollView } from 'react-native';
import { readNotification } from 'configs/redux/reducers/profile/profileActions';
import moment from 'moment';
import { getNotification } from 'configs/redux/reducers/profile/profileActions';
import EmptyNotification from 'assets/images/notification.svg';

const NotificationPage = (props) => {
  const {
    navigation,
    notification,
    username,
    dispatchReadNotification,
    dispatchGetNotification,
  } = props;

  useEffect(() => {
    dispatchGetNotification();
  }, []);

  const navigateToTrainerProfile = () => {
    navigation.navigate('TrainerProfilePage', {
      authorUsername: username,
    });
  };

  const notificationPressHandler = (item) => {
    dispatchReadNotification({
      body: {
        id: item.id,
      },
    });
    navigateToTrainerProfile();
  };

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar
        translucent
        backgroundColor={Color.backgroundColor}
        barStyle={'dark-content'}
      />
      <View style={styles.topBarWrapper}>
        <Pressable
          onPress={() => {
            navigation.pop();
          }}>
          <View style={styles.backButton}>
            <EvaIcon
              name={'arrow-ios-back-outline'}
              width={scale(20)}
              height={scale(20)}
              fill={'white'}
            />
          </View>
        </Pressable>

        <Text bold style={styles.title}>
          Notifications
        </Text>
      </View>
      {notification.data.length === 0 ? (
        <View style={styles.emptyNotifWrapper}>
          <EmptyNotification width={scale(250)} height={scale(250)} />
          <Text bold style={styles.emptyNotifTitle}>
            It's okay, let's create a course and hopefully you will get new
            subscriber soon
          </Text>
          <Text style={styles.emptyNotifSub}>
            Notification will come, Build your high quality course
          </Text>
        </View>
      ) : (
        <View />
      )}
      <ScrollView showsVerticalScrollIndicator={false}>
        {notification.data.filter((item) => item.isRead === false).length >
        0 ? (
          <View>
            <Text style={styles.groupTitle}>New</Text>
            {notification.data
              .filter((item) => item.isRead === false)
              .map((item, index) => {
                return (
                  <NotificationCard
                    key={index}
                    avatar={item.user_subcribe.profilePicture}
                    learner={item.user_subcribe.fullname}
                    message={item.body}
                    time={item.updatedAt}
                    thumb={item.course_detail.picture}
                    onPress={() => {
                      notificationPressHandler(item);
                    }}
                  />
                );
              })}
          </View>
        ) : (
          <View />
        )}

        {notification.data.filter(
          (item) =>
            moment(new Date(item.updatedAt)).format('MMM Do YY') ===
            moment().format('MMM Do YY'),
        ).length > 0 ? (
          <View>
            <Text style={styles.groupTitle}>Today</Text>
            {notification.data
              .filter(
                (item) =>
                  moment(new Date(item.updatedAt)).format('MMM Do YY') ===
                  moment().format('MMM Do YY'),
              )
              .map((item, index) => {
                return (
                  <NotificationCard
                    key={index}
                    avatar={item.user_subcribe.profilePicture}
                    learner={item.user_subcribe.fullname}
                    message={item.body}
                    time={item.updatedAt}
                    thumb={item.course_detail.picture}
                    onPress={() => {
                      notificationPressHandler(item);
                    }}
                  />
                );
              })}
          </View>
        ) : (
          <View />
        )}
        {notification.data.filter(
          (item) =>
            moment(new Date(item.updatedAt)).format('MMM Do YY') !==
            moment().format('MMM Do YY'),
        ).length > 0 ? (
          <View>
            <Text style={styles.groupTitle}>Old Notification</Text>
            {notification.data
              .filter(
                (item) =>
                  moment(new Date(item.updatedAt)).format('MMM Do YY') !==
                  moment().format('MMM Do YY'),
              )
              .map((item, index) => {
                return (
                  <NotificationCard
                    key={index}
                    avatar={item.user_subcribe.profilePicture}
                    learner={item.user_subcribe.fullname}
                    message={item.body}
                    time={item.updatedAt}
                    thumb={item.course_detail.picture}
                    onPress={() => {
                      notificationPressHandler(item);
                    }}
                  />
                );
              })}
          </View>
        ) : (
          <View />
        )}
      </ScrollView>
    </SafeAreaView>
  );
};

function mapStateToProps(state) {
  return {
    notification: state.profileStore.notification,
    username: state.profileStore.username,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatchReadNotification: (payload) => dispatch(readNotification(payload)),
    dispatchGetNotification: () => dispatch(getNotification()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(NotificationPage);
