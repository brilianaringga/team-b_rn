import { React, useEffect, useState, connect, View } from 'libraries';
import { Button, MaterialCard, ProgressBar, Text } from 'components';
import styles from './style';
import { Color, FONTS, scale } from 'utils';
import {
  Image,
  Modal,
  SafeAreaView,
  ScrollView,
  StatusBar,
  TouchableOpacity,
} from 'react-native';
import { Icon } from 'react-native-eva-icons';
import ReadMore from '@fawazahmed/react-native-read-more';
import IMG from 'assets/images';
import { base_url } from 'configs/api/url';
import {
  getCourseCheck,
  getMaterialCourse,
} from 'configs/redux/reducers/course/courseActions';

const CourseMaterialPage = (props) => {
  const {
    navigation,
    route,
    dispatchGetMaterial,
    material,
    dispatchGetCourseStatus,
  } = props;
  const { data } = route.params;
  const [progress, setProgress] = useState(0);
  const [materialComplete, setMaterialComplete] = useState(false);
  const [visibileReview, setVisibleReview] = useState(false);

  useEffect(() => {
    dispatchGetMaterial({
      paramsId: data.id,
    });
    return () => {
      setProgress(0);
    };
  }, []);

  useEffect(() => {
    if (material.data.materials.length > 0) {
      if (material.data.materials[0].courseId === data.id) {
        let completed = material.data.materials.filter(
          (item) => item.isRead === true,
        );
        if (completed.length === material.data.materials.length) {
          setMaterialComplete(true);
          dispatchGetCourseStatus({
            body: {
              courseId: data.id,
            },
          });
          if (material.data.isExam === false && data.rating == null) {
            setVisibleReview(true);
          }
        }
        let total = material.data.isExam
          ? material.data.materials.length + 1
          : material.data.materials.length;
        setProgress((completed.length / total) * 100);
      }
    }
  }, [material]);

  const navigateToMaterialPage = (item) => {
    navigation.navigate('MaterialView', {
      data: item,
      thumb:
        data.picture !== null
          ? `${base_url}/files/course/image/${data.picture}`
          : 'https://akm-img-a-in.tosshub.com/indiatoday/images/story/201811/online-3412473_1920_1.jpeg',
      courseId: data.id,
    });
  };

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar
        translucent
        backgroundColor={'transparent'}
        barStyle={'light-content'}
      />
      <View style={styles.topWrapper}>
        <Image
          source={{
            uri:
              data.picture !== null
                ? `${base_url}/files/course/image/${data.picture}`
                : 'https://akm-img-a-in.tosshub.com/indiatoday/images/story/201811/online-3412473_1920_1.jpeg',
          }}
          style={styles.image}
        />
        <View style={styles.overlay} />
        <View style={styles.summaryWrapper}>
          <View style={styles.heightSummary}>
            <View style={styles.backgroundSummary}>
              <View style={styles.row}>
                <Icon
                  name={'people'}
                  height={scale(25)}
                  width={scale(25)}
                  fill={'white'}
                />
                <Text style={styles.subsText}>{data.subscriberCount}</Text>
              </View>
            </View>
          </View>
          <View style={styles.rightSumWrapper}>
            <View style={styles.rightSumBackground}>
              <View style={styles.row}>
                <Icon
                  name={'star'}
                  height={scale(25)}
                  width={scale(25)}
                  fill={'#FF9D2A'}
                />
                <Text style={styles.starText}>
                  {data.rating !== null
                    ? Math.round(
                        (parseFloat(data.rating) + Number.EPSILON) * 100,
                      ) / 100
                    : '-'}
                </Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.overlay} />
        <View style={styles.backButton}>
          <TouchableOpacity
            onPress={() => {
              navigation.goBack();
            }}>
            <Icon
              name={'arrow-ios-back-outline'}
              height={scale(25)}
              width={scale(25)}
              fill={'white'}
            />
          </TouchableOpacity>
        </View>
      </View>

      <View style={styles.scrollWrapper}>
        <ScrollView style={styles.courseContainer}>
          <Text bold numberOfLines={1} style={styles.textTitleStyle}>
            {data.title}
          </Text>
          <ReadMore
            numberOfLines={5}
            style={{
              fontSize: scale(17),
              letterSpacing: scale(-0.2),
              fontFamily: FONTS.brandonGrotesque,
            }}
            backgroundColor={'#F4F4F4'}
            seeMoreStyle={styles.seeMoreStyle}>
            {data.desc}
          </ReadMore>
          <ProgressBar
            step={
              Math.round((parseFloat(progress) + Number.EPSILON) * 100) / 100
            }
          />
          <View style={{ marginTop: scale(10), marginBottom: scale(5) }}>
            <View style={styles.rowBetween}>
              <Text
                bold
                style={{ fontSize: scale(20), letterSpacing: scale(-0.2) }}>
                Course Content
              </Text>
            </View>
            <View style={styles.greenLine} />
          </View>
          {material.data.materials.length > 0 && !material.isLoading ? (
            material.data.materials.map((item, index) => {
              return (
                <MaterialCard
                  key={index}
                  title={item.title}
                  desc={item.desc}
                  type={item.type}
                  onPress={() => {
                    navigateToMaterialPage(item);
                  }}
                  isComplete={item.isRead}
                />
              );
            })
          ) : (
            <Text>Loading</Text>
          )}
        </ScrollView>
        {material.data.isExam ? (
          <View style={{ backgroundColor: Color.backgroundColor }}>
            <Button
              title={'Take Exam'}
              buttonStyle={styles.buttonTextExam}
              textStyle={styles.textExam}
              onPress={() => {
                if (materialComplete) {
                  navigation.navigate('TakeExamPage', {
                    data: data,
                  });
                }
              }}
              disabled={!materialComplete}
            />
          </View>
        ) : (
          <View />
        )}
      </View>
      <Modal visible={visibileReview} animationType="slide" transparent={true}>
        <View style={styles.modalContainer}>
          <View style={styles.modalContent}>
            <Image source={IMG.congratulation} style={styles.imageContent} />
            <Text style={{ marginTop: scale(60) }}>CONGRATULATIONS</Text>
            <Text bold style={{ marginTop: scale(10) }}>
              You have successfully complete your course
            </Text>
            <Button
              title="Give Review"
              buttonStyle={styles.buttonGiveReview}
              textStyle={styles.textReviewButton}
              onPress={() => {
                setVisibleReview(false);
                navigation.navigate('ReviewPage', {
                  data,
                });
              }}
            />
          </View>
        </View>
      </Modal>
    </SafeAreaView>
  );
};

function mapStateToProps(state) {
  return {
    material: state.courseStore.material,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatchGetMaterial: (payload) => dispatch(getMaterialCourse(payload)),
    dispatchGetCourseStatus: (payload) => dispatch(getCourseCheck(payload)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CourseMaterialPage);
