import {
  React,
  useEffect,
  useState,
  connect,
  View,
  StatusBar,
  Image,
  TouchableOpacity,
  EvaIcon,
  ScrollView,
} from 'libraries';
import { CardCourseVertical, Text } from 'components';
import styles from './style';
import { appString, Color, METRICS, scale } from 'utils';
import { Icon } from 'react-native-eva-icons';
import { getTrainerProfile } from 'configs/redux/reducers/profile/profileActions';
import { base_url } from 'configs/api/url';

const TrainerProfilePage = (props) => {
  const { navigation, route, dispatchGetTrainer, trainer } = props;
  const data = route.params;
  // const data = { authorUsername: 'andreaginsa' };

  useEffect(() => {
    // isi initial first load
    getTrainer();
  }, []);

  const getTrainer = () => {
    console.log('data param : ', data);
    const payload = {
      paramsId: data.authorUsername,
    };
    dispatchGetTrainer(payload);
  };

  useEffect(() => {
    // isi initial first load
    console.log('ini data trainer', trainer);
  }, [trainer]);

  return (
    <View style={styles.container}>
      <StatusBar
        translucent
        backgroundColor={'transparent'}
        barStyle={'light-content'}
      />
      <View style={{ position: 'relative' }}>
        <Image
          source={{
            uri:
              trainer.profilePicture !== null
                ? `${base_url}/files/user/image/${trainer.profilePicture}`
                : 'https://akm-img-a-in.tosshub.com/indiatoday/images/story/201811/online-3412473_1920_1.jpeg',
          }}
          style={{
            height: METRICS.window.height * 0.55,
            width: METRICS.window.width,
            resizeMode: 'cover',
          }}
        />
        <View style={styles.overlay} />

        <View
          style={{
            position: 'absolute',
            top: scale(50),
            left: scale(15),
            padding: scale(10),
            borderRadius: scale(50),
            backgroundColor: 'rgba(0,0,0,0.3)',
          }}>
          <TouchableOpacity
            onPress={() => {
              navigation.pop();
            }}>
            <Icon
              name={'arrow-ios-back-outline'}
              height={scale(25)}
              width={scale(25)}
              fill={'white'}
            />
          </TouchableOpacity>
        </View>
        <View
          style={{
            position: 'absolute',
            top: scale(50),
            right: scale(15),
            padding: scale(10),
            borderRadius: scale(50),
            backgroundColor: 'rgba(0,0,0,0.3)',
          }}>
          <TouchableOpacity
            onPress={() => {
              navigation.pop();
            }}
          />
        </View>
      </View>
      <View
        style={{
          flex: 1,
          marginTop: scale(-45),
          backgroundColor: '#F5FAFA',
          borderTopLeftRadius: scale(0),
          borderTopRightRadius: scale(0),
          elevation: 5,
          paddingTop: scale(10),
          paddingHorizontal: scale(15),
        }}>
        <View style={{ flex: 1, marginTop: scale(10) }}>
          <View>
            <Text style={{ fontSize: scale(25) }}>{trainer.fullname}</Text>
          </View>
          <View style={{ flex: 1 }}>
            <ScrollView>
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Icon
                  name={'star'}
                  height={scale(15)}
                  width={scale(15)}
                  fill={'#FF9D2A'}
                />
                <Text style={{ marginLeft: scale(5) }}>
                  <Text medium style={{ color: 'black' }}>
                    {Math.round(
                      (parseFloat(trainer.avgRating) + Number.EPSILON) * 100,
                    ) / 100}
                  </Text>
                  <Text medium style={{ color: '#8F9BB3' }}>
                    {`  (${trainer.reviewer} ${
                      trainer.reviewer > 0 ? 'Reviews' : 'Review'
                    })`}
                  </Text>
                </Text>
              </View>
              <View
                style={{
                  width: METRICS.window.width,
                  height: scale(1.5),
                  backgroundColor: '#EDF1F7',
                  marginVertical: scale(5),
                }}
              />
              <View style={{ marginTop: scale(10), marginBottom: scale(10) }}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                  }}>
                  <View>
                    <Text
                      medium
                      style={{ fontSize: scale(15), color: '#8F9BB3' }}>
                      Courses
                    </Text>
                    <Text
                      medium
                      style={{
                        fontSize: scale(12),
                        color: Color.primaryColor,
                      }}>
                      {trainer.courses && trainer.courses.length}
                      {/* 0 */}
                    </Text>
                  </View>

                  <View>
                    <Text
                      medium
                      style={{ fontSize: scale(15), color: '#8F9BB3' }}>
                      Subscribers
                    </Text>
                    <Text
                      medium
                      style={{
                        fontSize: scale(12),
                        color: Color.primaryColor,
                      }}>
                      {trainer.subscriber}
                    </Text>
                  </View>
                  <View>
                    <Text
                      medium
                      style={{ fontSize: scale(15), color: '#8F9BB3' }}>
                      Likes
                    </Text>
                    <Text
                      medium
                      style={{
                        fontSize: scale(12),
                        color: Color.primaryColor,
                      }}>
                      {trainer.loves}
                    </Text>
                  </View>
                </View>
              </View>
              <View style={{ marginTop: scale(5), marginBottom: scale(10) }}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                  }}>
                  <Text
                    bold
                    style={{ fontSize: scale(17), letterSpacing: scale(-0.2) }}>
                    Courses
                  </Text>
                </View>
                <View
                  style={{
                    width: scale(30),
                    height: scale(4),
                    backgroundColor: '#2D8989',
                    // borderRadius: scale(25),
                    borderTopLeftRadius: scale(25),
                    borderTopRightRadius: scale(25),
                  }}
                />
              </View>
              <View>
                <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                  {trainer.courses &&
                    trainer.courses.map((item, index) => {
                      return (
                        <CardCourseVertical
                          key={index}
                          title={item.title}
                          desc={item.desc}
                          lovedCount={item.loveCount.toString()}
                          viewCount={item.viewCount.toString()}
                          imageUri={
                            item.picture !== null
                              ? `${base_url}/files/course/image/${item.picture}`
                              : 'https://akm-img-a-in.tosshub.com/indiatoday/images/story/201811/online-3412473_1920_1.jpeg'
                          }
                          isLoved={item.isLoved}
                          isWishList={item.isWishlisted}
                          onPress={() => {
                            navigation.navigate('CourseDetailPage', {
                              data: item,
                            });
                          }}
                        />
                      );
                    })}
                </ScrollView>
              </View>
            </ScrollView>
          </View>
        </View>
      </View>
    </View>
  );
};

function mapStateToProps(state) {
  return {
    //isi state dari reducer
    trainer: state.profileStore.trainer.data,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    //isi dispatch
    dispatchGetTrainer: (payload) => dispatch(getTrainerProfile(payload)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(TrainerProfilePage);
