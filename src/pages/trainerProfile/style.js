import { StyleSheet } from 'react-native';
import { Color, scale } from 'utils';
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FAFA',
  },
  overlay: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(45, 137, 137, 0.25)',
  },
});

export default styles;
