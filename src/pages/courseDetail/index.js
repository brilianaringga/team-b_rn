import {
  React,
  useEffect,
  useState,
  connect,
  StatusBar,
  Image,
  TouchableOpacity,
  EvaIcon,
  ScrollView,
  useRef,
} from 'libraries';
import ReadMore from '@fawazahmed/react-native-read-more';
import { FlatList, View } from 'react-native';
import { CardCourseVertical, CommentCard, Text } from 'components';
import styles from './style';
import { Color, FONTS, METRICS, scale } from 'utils';
import { Icon } from 'react-native-eva-icons';
import { getTrainerProfile } from 'configs/redux/reducers/profile/profileActions';
import { base_url } from 'configs/api/url';
import { Pressable } from 'libraries';
import RBSheet from 'react-native-raw-bottom-sheet';
import {
  getCommentList,
  getCourseCheck,
  getRecentCourse,
  getSimilarCourse,
} from 'configs/redux/reducers/course/courseActions';
import API from 'configs/api';
import Toast from 'react-native-toast-message';

import ReviewIllustration from 'assets/images/Reviews.svg';
import { ActivityIndicator } from 'react-native';
import { expiredAction } from 'configs/redux/reducers/auth/authActions';

const CourseDetailPage = (props) => {
  const {
    navigation,
    route,
    dispatchGetTrainer,
    trainer,
    commentList,
    dispatchGetComment,
    dispatchGetSimilarCourse,
    similarCourse,
    dispatchGetRecentCourse,
    dispatchExpired,
    dispatchGetCourseStatus,
    courseCheck,
    username,
  } = props;
  const { data } = route.params;

  const [currentLove, setCurrentLove] = useState(data.isLoved);
  const [currentWish, setCurrentWish] = useState(data.isWishlisted);
  const [offset, setOffset] = useState(1);
  const [currentData, setCurrentData] = useState(data);
  const [isSubscribe, setIsSSubscribe] = useState(false);
  const [isComplete, setIsComplete] = useState(false);
  const [isExam, setIsExam] = useState(false);
  const [mark, setMark] = useState(null);
  const [isLoadingButton, setIsLoadingButton] = useState(true);
  const [isReview, setIsReview] = useState(false);
  const [buttonStatus, setButtonStatus] = useState('');

  const refRBSheet = useRef();

  useEffect(() => {
    // isi initial first load
    getFirstData();
    countView(data.id);
    // checkCourse(data.id);
  }, []);

  useEffect(() => {
    if (
      (courseCheck.data.isSubscribed &&
        courseCheck.data.isExam === true &&
        courseCheck.data.mark == null) ||
      (courseCheck.data.isSubscribed &&
        !courseCheck.data.isCompleted &&
        courseCheck.data.isExam === false)
    ) {
      setButtonStatus('continue');
    } else if (
      (courseCheck.data.isCompleted &&
        courseCheck.data.mark != null &&
        courseCheck.data.isExam === true) ||
      (courseCheck.data.isCompleted &&
        courseCheck.data.isExam === false &&
        courseCheck.data.isReview === true)
    ) {
      setButtonStatus('cert');
    } else if (
      (courseCheck.data.isCompleted &&
        courseCheck.data.mark != null &&
        courseCheck.data.isExam === true) ||
      (courseCheck.data.isCompleted &&
        courseCheck.data.isExam === false &&
        courseCheck.data.isReview === false)
    ) {
      setButtonStatus('review');
    } else if (trainer.username === username) {
      setButtonStatus('own');
    } else if (!courseCheck.data.isSubscribed) {
      setButtonStatus('takeCourse');
    }
  }, [courseCheck, trainer]);

  const renderCommentList = ({ item }) => (
    <CommentCard
      key={item.id}
      avatar={item.user.profilePicture}
      comment={item.comment}
      fullname={item.user.fullname}
      rating={item.rating}
      updateTime={item.updatedAt}
    />
  );

  const getFirstData = () => {
    getTrainer(data.authorUsername);
    dispatchGetComment({
      paramsId: data.id,
      params: {
        page: offset,
        pageSize: 10,
      },
    });
    dispatchGetSimilarCourse({
      paramsId: currentData.id,
    });
    dispatchGetCourseStatus({
      body: {
        courseId: currentData.id,
      },
    });
  };

  const loadMore = () => {
    dispatchGetComment({
      paramsId: currentData.id,
      params: {
        page: offset + 1,
        pageSize: 10,
      },
    });
    setOffset(offset + 1);
  };

  const getTrainer = (username) => {
    const payload = {
      paramsId: username,
    };

    dispatchGetTrainer(payload);
  };

  const toggleWishList = async (current, courseId) => {
    try {
      const res = await API.toggleWishlist({
        body: {
          toggle: current,
          courseId,
        },
      });
      if (res) {
        currentData.isWishlisted = current;
        setCurrentWish(current);
      }
    } catch (e) {
      if (e.status === 401) {
        dispatchExpired();
      }
      Toast.show({
        text1: 'Ups, something wrong, please try again',
        position: 'top',
        type: 'error',
      });
      console.log(e);
    }
  };

  const countView = async (courseId) => {
    try {
      await API.countView({
        body: {
          courseId,
        },
      });
    } catch (e) {
      if (e.status === 401) {
        dispatchExpired();
      }
    }
  };

  const checkCourse = async (courseId) => {
    try {
      const res = await API.checkCourseStatus({
        body: {
          courseId,
        },
      });
      if (res) {
        setIsSSubscribe(res.isSubscribed);
        setIsComplete(res.isCompleted);
        setIsExam(res.isExam);
        setMark(res.mark);
        setIsReview(res.isReview);
        setIsLoadingButton(false);
      }
    } catch (e) {
      if (e.status === 401) {
        dispatchExpired();
      }
      if (e.status === 422) {
        setIsSSubscribe(false);
        setIsLoadingButton(false);
      }
    }
  };

  const toggleLove = async (current, courseId) => {
    try {
      const res = await API.toggleLove({
        body: {
          toggle: current,
          courseId,
        },
      });
      if (res) {
        currentData.isLoved = current;
        setCurrentLove(current);
      }
    } catch (e) {
      if (e.status === 401) {
        dispatchExpired();
      }
      Toast.show({
        text1: 'Ups, something wrong, please try again',
        position: 'top',
        type: 'error',
      });
    }
  };

  const goToCertPage = () => {
    navigation.navigate('CourseScorePage', {
      courseId: data.id,
    });
  };

  const goToReviewPage = () => {
    navigation.navigate('ReviewPage', {
      data,
    });
  };

  const goToMyCourse = () => {
    navigation.navigate('CoursePage');
  };

  const takeCourse = async () => {
    try {
      const take = await API.takeCourse({
        body: {
          courseId: currentData.id,
        },
      });
      if (take) {
        dispatchGetRecentCourse({
          params: {
            page: 1,
            pageSize: 5,
          },
        });
        navigation.navigate('CourseMaterialPage', {
          data: currentData,
        });
        checkCourse(currentData.id);
      }
    } catch (e) {
      if (e.status === 401) {
        dispatchExpired();
      }
      if (e.status === 422) {
        navigation.navigate('CourseMaterialPage', {
          data: currentData,
        });
      } else {
        console.log(e);
        Toast.show({
          text1: 'Ups, something wrong, please try again',
          position: 'top',
          type: 'error',
        });
      }
    }
  };
  return (
    <View style={styles.container}>
      <StatusBar
        translucent
        backgroundColor={'transparent'}
        barStyle={'light-content'}
      />
      <View style={styles.wrapper}>
        <Image
          source={{
            uri:
              currentData.picture !== null
                ? `${base_url}/files/course/image/${currentData.picture}`
                : 'https://akm-img-a-in.tosshub.com/indiatoday/images/story/201811/online-3412473_1920_1.jpeg',
          }}
          style={styles.imageCourse}
        />
        <View style={styles.overlay} />

        <View style={styles.backContainer}>
          <TouchableOpacity
            onPress={() => {
              navigation.goBack();
            }}>
            <Icon
              name={'arrow-ios-back-outline'}
              height={scale(25)}
              width={scale(25)}
              fill={'white'}
            />
          </TouchableOpacity>
        </View>
        <View style={styles.starContainer}>
          <TouchableOpacity
            onPress={() => {
              navigation.pop();
            }}>
            <View style={styles.starWrapper}>
              <Icon
                name={'star'}
                height={scale(25)}
                width={scale(25)}
                fill={'#FF9D2A'}
              />
              <Text bold style={styles.textStar}>
                {currentData.rating !== null
                  ? Math.round(
                      (parseFloat(currentData.rating) + Number.EPSILON) * 100,
                    ) / 100
                  : '-'}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.contentWrapper}>
        <View style={styles.scrollWrapper}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <View style={{ paddingHorizontal: scale(15) }}>
              <Text bold style={styles.courseTitle}>
                {currentData.title}
              </Text>
            </View>
            <View style={styles.scrollWrapper}>
              <View style={styles.descWrapper}>
                <View style={styles.rowBetween}>
                  <Text
                    bold
                    style={{ fontSize: scale(17), letterSpacing: scale(-0.2) }}>
                    Description
                  </Text>
                </View>
                <View style={styles.greenLine} />
              </View>
              <View style={{ paddingHorizontal: scale(15) }}>
                <ReadMore
                  numberOfLines={5}
                  style={{
                    fontSize: scale(15),
                    fontFamily: FONTS.brandonGrotesque,
                  }}
                  backgroundColor={'#F4F4F4'}
                  seeMoreStyle={styles.moreless}
                  seeLessStyle={styles.moreless}>
                  {currentData.desc}
                </ReadMore>
              </View>
              <View style={styles.separator} />
              <View style={styles.categoryRow}>
                <Text
                  bold
                  style={{
                    fontSize: scale(15),
                    color: Color.primaryColor,
                  }}>
                  Category
                </Text>
                <Text style={styles.categoryName}>
                  {currentData.categoryName}
                </Text>
              </View>
              <View style={styles.separator} />

              <View
                style={{
                  marginTop: scale(15),
                  marginBottom: scale(10),
                  paddingHorizontal: scale(15),
                }}>
                <View style={styles.rowBetween}>
                  <Text
                    bold
                    style={{ fontSize: scale(17), letterSpacing: scale(-0.2) }}>
                    Author
                  </Text>
                </View>
                <View style={styles.greenLine} />
              </View>
              <Pressable
                style={styles.trainerWrapper}
                onPress={() => {
                  navigation.navigate('TrainerProfilePage', {
                    authorUsername: trainer.authorUsername,
                  });
                }}>
                <View style={styles.rowBetween}>
                  <View style={styles.row}>
                    <Image
                      source={{
                        uri:
                          trainer.profilePicture !== null
                            ? `${base_url}/files/user/thumb/${trainer.profilePicture}`
                            : 'https://ppid.sulselprov.go.id/assets/front/img/kadis/avatar.png',
                      }}
                      style={styles.imageTrainer}
                    />
                    <View style={styles.trainerName}>
                      <Text bold style={styles.fullNameText}>
                        {trainer.fullname}
                      </Text>
                      <View style={styles.row}>
                        <Icon
                          name={'star'}
                          height={scale(15)}
                          width={scale(15)}
                          fill={'#FF9D2A'}
                        />
                        <Text style={{ marginLeft: scale(5) }}>
                          <Text bold style={styles.ratingTrainerText}>
                            {trainer.avgRating !== null
                              ? Math.round(
                                  (parseFloat(trainer.avgRating) +
                                    Number.EPSILON) *
                                    100,
                                ) / 100
                              : '-'}
                          </Text>
                          <Text medium style={styles.textReviewTrainer}>
                            {`  (${trainer.reviewer} ${
                              trainer.reviewer > 0 ? 'Reviews' : 'Review'
                            })`}
                          </Text>
                        </Text>
                      </View>
                    </View>
                    <View style={styles.buttonTrainerDetail}>
                      <Icon
                        name={'chevron-right-outline'}
                        height={scale(30)}
                        width={scale(30)}
                        fill={'white'}
                      />
                    </View>
                  </View>
                </View>
              </Pressable>

              <View style={{ paddingHorizontal: scale(15) }}>
                <View style={{ marginTop: scale(15), marginBottom: scale(10) }}>
                  <View style={styles.rowBetween}>
                    <View>
                      <View style={styles.rowBetween}>
                        <Text bold style={styles.textReviewRatings}>
                          Review & Ratings
                        </Text>
                      </View>
                      <View style={styles.greenLine} />
                    </View>
                    <Pressable
                      onPress={() => {
                        refRBSheet.current.open();
                      }}>
                      <View>
                        <Text>
                          {commentList.data.length > 0 && !commentList.isLoading
                            ? 'View More'
                            : ''}
                        </Text>
                      </View>
                    </Pressable>
                  </View>
                </View>
              </View>
              {commentList.data.length > 0 && !commentList.isLoading ? (
                <ScrollView style={{ paddingHorizontal: scale(15) }}>
                  {commentList.data.slice(0, 3).map((item, index) => {
                    return (
                      <CommentCard
                        key={index}
                        avatar={item.user.profilePicture}
                        comment={item.comment}
                        fullname={item.user.fullname}
                        rating={item.rating}
                        updateTime={item.updatedAt}
                      />
                    );
                  })}
                </ScrollView>
              ) : commentList.isLoading ? (
                <View style={styles.loaderWrapper}>
                  <ActivityIndicator
                    color={Color.primaryColor}
                    size={'small'}
                  />
                </View>
              ) : (
                <View style={styles.textCenter}>
                  <ReviewIllustration height={scale(80)} width={scale(100)} />
                  <Text>No reviews on this course</Text>
                </View>
              )}

              {similarCourse.data.length > 0 && !similarCourse.isLoading ? (
                <View>
                  <View style={styles.similarWrapper}>
                    <View style={styles.rowBetween}>
                      <Text bold style={styles.textSimilar}>
                        Similar Course
                      </Text>
                    </View>
                    <View style={styles.greenLine} />
                  </View>
                  <View>
                    <ScrollView
                      horizontal
                      showsHorizontalScrollIndicator={false}>
                      {similarCourse.data.map((item, index) => {
                        return (
                          <View
                            key={index}
                            style={styles.wrapperCardSimilar(index)}>
                            <CardCourseVertical
                              title={item.title}
                              desc={item.desc}
                              lovedCount={item.loveCount}
                              viewCount={item.viewCount}
                              imageUri={
                                item.picture !== null
                                  ? `${base_url}/files/course/image/${item.picture}`
                                  : 'https://akm-img-a-in.tosshub.com/indiatoday/images/story/201811/online-3412473_1920_1.jpeg'
                              }
                              isLoved={item.isLoved}
                              isWishList={item.isWishlisted}
                              onPress={() => {
                                setOffset(1);
                                setCurrentData(item);
                                setCurrentLove(item.isLoved);
                                setCurrentWish(item.isWishlisted);
                                getTrainer(item.authorUsername);
                                dispatchGetComment({
                                  paramsId: item.id,
                                  params: {
                                    page: 1,
                                    pageSize: 10,
                                  },
                                });
                                dispatchGetSimilarCourse({
                                  paramsId: item.id,
                                });
                                dispatchGetCourseStatus({
                                  body: {
                                    courseId: item.id,
                                  },
                                });
                                // checkCourse(item.id);
                                countView(item.id);
                              }}
                            />
                          </View>
                        );
                      })}
                    </ScrollView>
                  </View>
                </View>
              ) : (
                <View />
              )}
            </View>
          </ScrollView>
        </View>
        <View style={styles.bottomWraapper}>
          <View style={styles.row}>
            <Pressable
              onPress={() => {
                toggleLove(!currentLove, currentData.id);
              }}>
              <View style={styles.wrapperHeart}>
                <Icon
                  name={currentLove ? 'heart' : 'heart-outline'}
                  height={scale(25)}
                  width={scale(25)}
                  fill={'red'}
                />
              </View>
            </Pressable>
            <Pressable
              onPress={() => {
                toggleWishList(!currentWish, currentData.id);
              }}>
              <View style={styles.bookmarkWrapper}>
                <EvaIcon
                  name={currentWish ? 'bookmark' : 'bookmark-outline'}
                  width={scale(20)}
                  height={scale(20)}
                  fill={currentWish ? 'orange' : 'black'}
                />
              </View>
            </Pressable>
            <Pressable
              style={styles.takeCourse}
              onPress={() => {
                buttonStatus === 'continue'
                  ? navigation.navigate('CourseMaterialPage', {
                      data: currentData,
                    })
                  : buttonStatus === 'cert'
                  ? goToCertPage()
                  : buttonStatus === 'review'
                  ? goToReviewPage()
                  : buttonStatus === 'own'
                  ? goToMyCourse()
                  : takeCourse();
              }}>
              <View style={styles.buttonAction}>
                {!courseCheck.isLoading ? (
                  <Text bold style={styles.textButtonAction}>
                    {buttonStatus === 'continue'
                      ? 'CONTINUE COURSE'
                      : buttonStatus === 'cert'
                      ? 'SHOW MY CERTIFICATE'
                      : buttonStatus === 'review'
                      ? 'GIVE REVIEW'
                      : buttonStatus === 'own'
                      ? 'EDIT COURSE'
                      : 'TAKE COURSE'}
                  </Text>
                ) : (
                  <ActivityIndicator size="small" color={'white'} />
                )}
              </View>
            </Pressable>
          </View>
        </View>
      </View>
      <RBSheet
        ref={refRBSheet}
        closeOnDragDown={true}
        closeOnPressMask={true}
        dragFromTopOnly={true}
        height={METRICS.screen.height * 0.55}
        customStyles={{
          container: {
            borderTopLeftRadius: scale(15),
            borderTopRightRadius: scale(15),
          },
        }}>
        <View style={{ marginBottom: scale(15), paddingHorizontal: scale(10) }}>
          <View>
            <View style={styles.rowBetween}>
              <Text
                bold
                style={{
                  fontSize: scale(17),
                  letterSpacing: scale(-0.2),
                }}>
                Review & Ratings
              </Text>
            </View>
            <View style={styles.greenLine} />
          </View>
        </View>
        <View style={styles.wrapperBottomSheet}>
          <FlatList
            data={commentList.data}
            renderItem={renderCommentList}
            keyExtractor={(item, index) => index.toString()}
            onEndReachedThreshold={0.3}
            onEndReached={({ distanceFromEnd }) => {
              !commentList.isLoading && !commentList.isEnd && loadMore();
            }}
          />
        </View>
      </RBSheet>

      <Toast ref={(ref) => Toast.setRef(ref)} />
    </View>
  );
};

function mapStateToProps(state) {
  return {
    trainer: state.profileStore.trainer.data,
    commentList: state.courseStore.commentList,
    similarCourse: state.courseStore.similarCourse,
    courseCheck: state.courseStore.courseCheck,
    username: state.profileStore.username,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatchGetTrainer: (payload) => dispatch(getTrainerProfile(payload)),
    dispatchGetComment: (payload) => dispatch(getCommentList(payload)),
    dispatchGetSimilarCourse: (payload) => dispatch(getSimilarCourse(payload)),
    dispatchGetRecentCourse: (payload) => dispatch(getRecentCourse(payload)),
    dispatchExpired: () => dispatch(expiredAction()),
    dispatchGetCourseStatus: (payload) => dispatch(getCourseCheck(payload)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CourseDetailPage);
