import { StyleSheet } from 'libraries';
import { StatusBar } from 'react-native';
import { Color, scale } from 'utils';
import { shadow } from 'utils/styles/shadow';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Color.backgroundColor,
  },
  body: {
    flex: 1,
    flexGrow: 1,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: scale(10),
    marginTop: StatusBar.currentHeight,
  },
  headerLabel: {
    fontSize: scale(25),
    color: Color.primaryColor,
    marginLeft: scale(10),
    textTransform: 'capitalize',
  },
  form: {
    flex: 1,
    flexGrow: 1,
    paddingHorizontal: scale(10),
    // backgroundColor: 'blue',
  },
  formGroup: { marginBottom: scale(10) },
  inputLabel: {
    fontSize: scale(16),
    fontWeight: 'bold',
  },
  labelMark: {
    color: 'red',
    fontSize: scale(12),
  },
  inputStyle: {
    ...shadow,
    backgroundColor: 'white',
    padding: scale(10),
    borderRadius: scale(5),
  },
  textArea: {
    height: scale(200),
  },
  saveBtn: {
    backgroundColor: Color.primaryColor,
  },
  saveBtnLabel: {
    color: 'white',
    fontSize: scale(14),
  },
  selectedMaterials: {
    flex: 1,
    paddingVertical: scale(5),
    borderWidth: 1,
    borderRadius: scale(5),
    borderStyle: 'dashed',
  },
  emptyMaterial: {
    flex: 1,
    // flexDirection: 'row',
    justifyContent: 'space-evenly',
    // alignItems: 'center',
    padding: scale(10),
    borderRadius: scale(5),
  },
  materialContainer: {
    padding: scale(5),
  },
  materialCard: {
    backgroundColor: 'white',
    flexDirection: 'row',
    borderRadius: scale(5),
    padding: scale(10),
    borderWidth: 1,
    borderColor: Color.primaryColor,
    justifyContent: 'center',
    alignItems: 'center',
  },
  addMaterialBtn: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    padding: scale(15),
  },
  materialName: {
    flex: 1,
  },
  addLabel: {
    // backgroundColor: 'yellow',
  },
  addBtnContainer: {
    justifyContent: 'center',
    // alignItems: 'center',
  },
  videoBtn: {
    color: 'red',
    fontWeight: 'bold',
    // justifyContent: 'center',
    // alignItems: 'center',
  },
  pdfBtn: {
    color: 'red',
    fontWeight: 'bold',
    // justifyContent: 'center',
    // alignItems: 'center',
  },
  pdfCard: {
    flexDirection: 'row',
  },
  pdfLabel: {
    flex: 1,
  },
  videoCard: {
    flex: 1,
    // flexDirection: 'row',
  },
  videoDetail: {
    flex: 1,
    flexDirection: 'row',
  },
  videoLabel: {
    flex: 1,
  },
  videoStyle: {
    width: '100%',
    minHeight: scale(100),
    aspectRatio: 16 / 9,
  },
  videoCloseBtn: {
    alignSelf: 'flex-end',
    marginBottom: scale(5),
  },
  addMaterialVideo: {
    width: '100%',
    flexDirection: 'row',
    padding: scale(10),
    borderRadius: scale(5),
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: scale(10),
    backgroundColor: Color.purpleOld,
  },
  addMaterialAudio: {
    width: '100%',
    flexDirection: 'row',
    padding: scale(10),
    borderRadius: scale(5),
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: scale(10),
    backgroundColor: Color.blueOld,
  },
  addMaterialPDF: {
    width: '100%',
    flexDirection: 'row',
    padding: scale(10),
    borderRadius: scale(5),
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: scale(10),
    backgroundColor: Color.red,
  },
  addMaterialText: {
    color: 'white',
  },
  currentVideo: {
    padding: scale(10),
  },
});

export default styles;
