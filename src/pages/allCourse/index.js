import {
  React,
  useEffect,
  useState,
  connect,
  View,
  SafeAreaView,
  StatusBar,
  EvaIcon,
  Pressable,
  ActivityIndicator,
  FlatGrid,
  RefreshControl,
} from 'libraries';
import { CardCourseVertical, Text } from 'components';
import styles from './style';
import { Color, scale } from 'utils';
import { getCourseListPagination } from 'configs/redux/reducers/course/courseActions';
import { base_url } from 'configs/api/url';
import Nodata from 'assets/images/no_data.svg';

const AllCoursePage = (props) => {
  const {
    navigation,
    route,
    coursePagination,
    dispatchGetCourseByCategory,
  } = props;
  const { title, type, id } = route.params;

  const [offset, setOffset] = useState(1);

  const [refreshing, setRefreshing] = React.useState(false);
  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    dispatchGetCourseByCategory(type, {
      params: {
        id,
        page: 1,
        pageSize: 10,
      },
    });
    setRefreshing(false);
  }, []);

  useEffect(() => {
    dispatchGetCourseByCategory(type, {
      params: {
        id,
        page: 1,
        pageSize: 10,
      },
    });
  }, []);

  const loadMore = () => {
    dispatchGetCourseByCategory(type, {
      params: {
        id,
        page: offset + 1,
        pageSize: 10,
      },
    });
    setOffset(offset + 1);
  };

  const navigateToCourseDetail = (item) => {
    navigation.navigate('CourseDetailPage', {
      data: item,
    });
  };

  const renderCourse = ({ item }) => (
    <CardCourseVertical
      key={item.id}
      desc={item.desc}
      isLoved={item.isLoved}
      isWishList={item.isWishlisted}
      lovedCount={item.loveCount.toString()}
      imageUri={
        item.picture !== null
          ? `${base_url}/files/course/image/${item.picture}`
          : 'https://akm-img-a-in.tosshub.com/indiatoday/images/story/201811/online-3412473_1920_1.jpeg'
      }
      title={item.title}
      viewCount={item.viewCount.toString()}
      onPress={() => {
        navigateToCourseDetail(item);
      }}
    />
  );

  const renderFooter = () => {
    return coursePagination.isLoadingNext ? (
      <ActivityIndicator color={Color.primaryColor} size={'small'} />
    ) : null;
  };

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar
        translucent
        backgroundColor={Color.backgroundColor}
        barStyle={'dark-content'}
      />
      <View style={styles.topBarWrapper}>
        <Pressable
          onPress={() => {
            navigation.pop();
          }}>
          <View style={styles.backButtonStyle}>
            <EvaIcon
              name={'arrow-ios-back-outline'}
              width={scale(20)}
              height={scale(20)}
              fill={'white'}
            />
          </View>
        </Pressable>

        <Text bold style={styles.title}>
          {title}
        </Text>
      </View>
      {coursePagination.data.length > 0 && !coursePagination.isLoading ? (
        <View style={styles.paginationContainer}>
          <FlatGrid
            itemDimension={scale(130)}
            spacing={0}
            showsVerticalScrollIndicator={false}
            data={coursePagination.data}
            renderItem={renderCourse}
            keyExtractor={(item, index) => item.id.toString()}
            onEndReachedThreshold={0.3}
            onEndReached={({ distanceFromEnd }) => {
              !coursePagination.isLoading &&
                !coursePagination.isEnd &&
                loadMore();
            }}
            renderFooter={renderFooter}
            refreshControl={
              <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
            }
          />
        </View>
      ) : coursePagination.data.length === 0 && coursePagination.isEnd ? (
        <View style={styles.noDataContainer}>
          <Nodata height={scale(200)} />

          <Text style={styles.textNoData}>Course is not available</Text>
        </View>
      ) : (
        <View style={styles.loader}>
          <ActivityIndicator color={Color.primaryColor} size={'large'} />
        </View>
      )}
    </SafeAreaView>
  );
};

function mapStateToProps(state) {
  return {
    coursePagination: state.courseStore.coursePagination,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    dispatchGetCourseByCategory: (type, payload) =>
      dispatch(getCourseListPagination(type, payload)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AllCoursePage);
