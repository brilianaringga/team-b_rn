import { StatusBar, StyleSheet } from 'react-native';
import { Color, scale, METRICS } from 'utils';
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Color.backgroundColor,
    paddingTop: StatusBar.currentHeight,
    paddingHorizontal: scale(15),
  },
  topBarWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: scale(10),
    marginBottom: scale(20),
  },
  backButtonStyle: {
    backgroundColor: Color.primaryColor,
    borderRadius: scale(50),
    padding: scale(4),
  },
  title: {
    fontSize: scale(25),
    color: Color.primaryColor,
    marginLeft: scale(10),
  },
  paginationContainer: { flex: 1 },
  noDataContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: scale(-20),
  },
  textNoData: { fontSize: scale(20), marginTop: scale(10) },
  loader: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: scale(-20),
  },
});

export default styles;
