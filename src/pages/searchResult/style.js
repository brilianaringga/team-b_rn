import { StyleSheet } from 'react-native';
import { Color, scale, verticalScale } from 'utils';
const styles = StyleSheet.create({
  // tab bar
  tabBarIndicator: {
    backgroundColor: Color.primaryColor,
    height: 3,
    borderRadius: 10,
  },
  tabBarStyle: { backgroundColor: 'transparent' },
  tabBarText: {
    color: Color.primaryColor,
    fontSize: scale(18),
    fontWeight: 'bold',
  },
  // route one
  containerRouteOne: { paddingTop: scale(5) },
  routeOneFlatList: {
    flex: 1,
    justifyContent: 'space-evenly',
  },
  // route two
  containerRouteTwo: { paddingTop: scale(10) },
  routeTwoFlatList: {
    flex: 1,
    justifyContent: 'space-evenly',
  },
  // author card
  authorContainer: {
    padding: scale(10),
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: scale(10),
    width: scale(160),
    borderRadius: scale(5),
    backgroundColor: 'white',
    // shadowColor: '#000',
    // shadowOffset: {
    //   width: 0,
    //   height: 2,
    // },
    // shadowOpacity: 0.25,
    // shadowRadius: 3.84,
    // elevation: 5,
  },
  authorImage: {
    height: scale(90),
    width: scale(90),
    borderRadius: scale(60),
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
  },
  authorRate: { flexDirection: 'row', justifyContent: 'center' },
  authorFullName: { fontSize: scale(18), fontWeight: 'bold' },
  authorDetailContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  authorSubscriber: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  authorLoves: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  authorTotalLoves: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  authorDetailButton: {
    backgroundColor: Color.primaryColor,
    borderRadius: scale(5),
    height: scale(30),
    justifyContent: 'center',
    width: '100%',
    marginTop: scale(3),
  },
  authorBtnText: { textAlign: 'center', color: 'white' },
  // main body
  keyboardAvoiding: { flex: 1 },
  container: {
    flex: 1,
    backgroundColor: Color.backgroundColor,
    paddingTop: scale(50),
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: scale(100),
    paddingHorizontal: scale(10),
  },
  searchField: {
    flex: 1,
    paddingVertical: scale(8),
    borderRadius: scale(25),
    color: Color.primaryColor,
    fontSize: verticalScale(14),
    backgroundColor: 'white',
    paddingHorizontal: scale(10),
    // shadowColor: '#000',
    // shadowOffset: {
    //   width: 0,
    //   height: 2,
    // },
    // shadowOpacity: 0.25,
    // shadowRadius: 3.84,
    // elevation: 5,
  },
  tabViewContainer: { flex: 1, paddingHorizontal: scale(5) },
  modalContainer: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#00000040',
  },
});

export default styles;
