import { BackButton, CardCourseVertical, Text, TextInput } from 'components';
import { base_url } from 'configs/api/url';
import { getTrainerProfile } from 'configs/redux/reducers/profile/profileActions';
import { fetchSearchResult } from 'configs/redux/reducers/search/searchActions';
import {
  connect,
  Dimensions,
  EvaIcon,
  FlatList,
  Image,
  KeyboardAvoidingView,
  Platform,
  React,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'libraries';
import { useEffect, useState } from 'react';
import { ActivityIndicator } from 'react-native';
import { Modal } from 'react-native';
import { TabBar, TabView } from 'react-native-tab-view';
import { Color, scale } from 'utils';
import styles from './style';

const renderTabBar = (props) => (
  <TabBar
    {...props}
    indicatorStyle={styles.tabBarIndicator}
    style={styles.tabBarStyle}
    renderLabel={({ route }) => (
      <Text style={styles.tabBarText}>{route.title}</Text>
    )}
  />
);

const SearchResultPage = (props) => {
  const {
    route,
    navigation,
    courses,
    profiles,
    isLoading,
    error,
    dispatchGetTrainer,
    dispatchGetSearchResult,
  } = props;
  const { keyword } = route.params;
  const [search, setSearch] = useState(keyword);
  const [tabIndex, setTabIndex] = useState(0);
  const [routes] = useState([
    { key: 'courses', title: 'Course' },
    { key: 'peoples', title: 'People' },
  ]);
  const initialLayout = { width: Dimensions.get('window').width };
  const tabViewStyles = StyleSheet.create({
    scene: {
      flex: 1,
    },
  });

  const FirstRoute = () => (
    <View style={[tabViewStyles.scene, styles.containerRouteOne]}>
      <FlatList
        showsVerticalScrollIndicator={false}
        data={courses}
        numColumns={2}
        columnWrapperStyle={styles.routeOneFlatList}
        keyExtractor={(item) => String(item.id)}
        renderItem={({ item, index }) => {
          return (
            <CardCourseVertical
              key={index}
              title={item.title}
              desc={item.desc}
              lovedCount={item.totalLoves.toString()}
              viewCount={item.totalview.toString()}
              imageUri={
                item.picture !== null
                  ? `${base_url}/files/course/image/${item.picture}`
                  : 'https://akm-img-a-in.tosshub.com/indiatoday/images/story/201811/online-3412473_1920_1.jpeg'
              }
              isLoved={item.lovebutton}
              isWishList={item.wishlistbutton}
              onPress={() => {
                navigation.navigate('CourseDetailPage', {
                  data: item,
                });
              }}
            />
          );
        }}
      />
    </View>
  );

  const SecondRoute = () => (
    <View style={[tabViewStyles.scene, styles.containerRouteTwo]}>
      <FlatList
        showsVerticalScrollIndicator={false}
        numColumns={2}
        columnWrapperStyle={styles.routeTwoFlatList}
        data={profiles}
        keyExtractor={(item) => String(item.username)}
        renderItem={({ item, index }) => {
          return (
            <View style={styles.authorContainer}>
              <Image
                source={{ uri: 'https://picsum.photos/200/300/?blur' }}
                style={{
                  ...StyleSheet.absoluteFillObject,
                  height: scale(60),
                  borderTopLeftRadius: scale(5),
                  borderTopRightRadius: scale(5),
                }}
              />
              <Image
                source={{
                  uri:
                    item.profilePicture != null
                      ? `http://139.59.124.53:3005/files/user/image/${item.profilePicture}`
                      : 'https://via.placeholder.com/150',
                }}
                style={styles.authorImage}
              />

              {item.averageRating != null ? (
                <View style={styles.authorRate}>
                  <EvaIcon
                    name="star"
                    width={24}
                    height={24}
                    fill={Color.yellowStar}
                  />
                  <Text>{item.averageRating}</Text>
                </View>
              ) : (
                <Text>No Rating Yet</Text>
              )}

              <Text numberOfLines={1} style={styles.authorFullName}>
                {item.fullname}
              </Text>
              <Text numberOfLines={1} style={{ fontSize: scale(14) }}>
                {item.email}
              </Text>
              <View style={styles.authorDetailContainer}>
                <View style={styles.authorSubscriber}>
                  <EvaIcon
                    name="people-outline"
                    width={24}
                    height={24}
                    fill={'black'}
                  />
                  <Text>{item.subscriber}</Text>
                </View>

                <View style={styles.authorLoves}>
                  <EvaIcon
                    name="heart-outline"
                    width={24}
                    height={24}
                    fill={'red'}
                  />
                  <Text>{item.totalLoves}</Text>
                </View>

                <View style={styles.authorTotalLoves}>
                  <EvaIcon
                    name="book-outline"
                    width={24}
                    height={24}
                    fill={'black'}
                  />
                  <Text>{item.totalLoves}</Text>
                </View>
              </View>
              <TouchableOpacity
                style={styles.authorDetailButton}
                onPress={() => viewAuthorHandler(item)}>
                <Text style={styles.authorBtnText}>Details</Text>
              </TouchableOpacity>
            </View>
          );
        }}
      />
    </View>
  );

  const renderScene = ({ route: tabRoute }) => {
    switch (tabRoute.key) {
      case 'courses':
        return <FirstRoute />;
      case 'peoples':
        return <SecondRoute />;
      default:
        return null;
    }
  };

  const getSearchResult = async () => {
    try {
      const payload = {
        params: {
          title: keyword,
        },
      };

      await dispatchGetSearchResult(payload);
    } catch (err) {
      console.log(err);
    }
  };

  const viewAuthorHandler = async (item) => {
    // fetch trainer first
    const payload = {
      paramsId: item.username,
    };
    await dispatchGetTrainer(payload);
    // then route
    navigation.navigate('TrainerProfilePage', {
      authorUsername: item.username,
    });
  };

  const searchHandler = async () => {
    try {
      const payload = {
        params: {
          title: search,
        },
      };

      await dispatchGetSearchResult(payload);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    getSearchResult();
  }, []);

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={styles.keyboardAvoiding}>
      {/* <TouchableWithoutFeedback onPress={Keyboard.dismiss}> */}
      <SafeAreaView style={styles.container}>
        <Modal animationType="fade" visible={isLoading} transparent={true}>
          <View style={styles.modalContainer}>
            <ActivityIndicator size="large" color={Color.primaryColor} />
          </View>
        </Modal>
        <StatusBar
          translsucent
          backgroundColor={Color.backgroundColor}
          barStyle={'dark-content'}
        />
        <View style={styles.header}>
          <BackButton
            onPress={() => {
              navigation.goBack();
            }}
          />

          <TextInput
            inlineImageLeft="search_icon"
            placeholder="Search courses or trainer for you ... "
            placeholderTextColor={Color.primaryColorLighten}
            numberOfLines={1}
            returnKeyType="search"
            value={search}
            onChangeText={(value) => setSearch(value)}
            style={styles.searchField}
            onSubmitEditing={searchHandler}
          />
        </View>

        <View style={styles.tabViewContainer}>
          <TabView
            // renderPager={(tabProps) => <ScrollPager {...tabProps} />}
            navigationState={{ index: tabIndex, routes }}
            renderTabBar={renderTabBar}
            renderScene={renderScene}
            lazy={true}
            lazyPreloadDistance={1}
            onIndexChange={setTabIndex}
            initialLayout={initialLayout}
          />
        </View>
      </SafeAreaView>
      {/* </TouchableWithoutFeedback> */}
    </KeyboardAvoidingView>
  );
};

function mapStateToProps(state) {
  return {
    //isi state dari reducer
    courses: state.searchStore.searchresult_course,
    profiles: state.searchStore.searchresult_profile,
    isLoading: state.searchStore.isLoading,
    error: state.searchStore.error,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    //isi dispatch
    dispatchGetTrainer: (payload) => dispatch(getTrainerProfile(payload)),
    dispatchGetSearchResult: (payload) => dispatch(fetchSearchResult(payload)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchResultPage);
