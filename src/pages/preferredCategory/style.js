import { StatusBar, StyleSheet } from 'react-native';
import { Color, scale, METRICS } from 'utils';
const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#F5FAFA' },
  imageContainer: {
    flex: 1,
    marginTop: -StatusBar.currentHeight,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    height: scale(120),
    aspectRatio: 16 / 9,
    borderRadius: scale(15),
  },
  boxWrapper: {
    width: METRICS.screen.width,
    paddingHorizontal: scale(15),
    marginVertical: scale(15),
  },
  buttonText: { color: 'white' },
});

export default styles;
