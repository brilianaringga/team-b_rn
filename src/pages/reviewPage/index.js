import { React, useState, connect, View, CommonActions } from 'libraries';
import { Button, Text } from 'components';
import styles from './style';
import { Color, scale } from 'utils';
import {
  Image,
  SafeAreaView,
  StatusBar,
  Pressable,
  Keyboard,
  Modal,
} from 'react-native';
import { AirbnbRating } from 'react-native-ratings';
import IMG from 'assets/images';
import { TextInput } from 'react-native';
import { ScrollView } from 'react-native';
import API from 'configs/api';
import Toast from 'react-native-toast-message';

const ReviewPage = (props) => {
  const { navigation, route } = props;
  const { data } = route.params;
  const [rating, setRating] = useState(4);
  const [curImage, setCurImage] = useState(IMG.rate4);
  const [messageRating, setMessageRating] = useState('');
  const [visibleSuccess, setVisibleSuccess] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const navigationReset = (page) => {
    navigation.dispatch(
      CommonActions.reset({
        index: 0,
        routes: [{ name: page }],
      }),
    );
  };

  const onSubmit = async () => {
    try {
      await API.submitReview({
        body: {
          courseId: data.id,
          rating,
          comment: messageRating,
        },
      });
      setIsLoading(false);
      setVisibleSuccess(true);
    } catch (e) {
      Toast.show({
        text1: 'Ups, something wrong, please try again',
        position: 'top',
        type: 'error',
      });
    }
  };

  const ratingCompleted = (item) => {
    setRating(item);
    switch (item) {
      case 1:
        setCurImage(IMG.rate1);
        break;
      case 2:
        setCurImage(IMG.rate2);
        break;
      case 3:
        setCurImage(IMG.rate3);
        break;
      case 4:
        setCurImage(IMG.rate4);
        break;
      case 5:
        setCurImage(IMG.rate5);
        break;
      default:
        break;
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <Pressable onPress={() => Keyboard.dismiss()} style={styles.pressStyle}>
        <ScrollView>
          <View style={styles.scrollWrapper}>
            <StatusBar
              translucent
              backgroundColor={Color.backgroundColor}
              barStyle={'dark-content'}
            />

            <Text bold style={styles.title}>
              How was your overall experience for this course ?
            </Text>
            <View style={styles.imageContainer}>
              <Image source={curImage} style={styles.image} />
            </View>

            <AirbnbRating
              count={5}
              showRating={false}
              defaultRating={4}
              size={scale(50)}
              onFinishRating={ratingCompleted}
              selectedColor={Color.primaryColor}
            />
            <View style={{ marginTop: scale(15) }}>
              <TextInput
                multiline
                style={styles.messageRating}
                onChangeText={(text) => setMessageRating(text)}
                value={messageRating}
              />
            </View>
          </View>
        </ScrollView>
      </Pressable>
      <Button
        title="Submit"
        buttonStyle={{
          borderRadius: scale(30),
          backgroundColor: Color.primaryColor,
          marginBottom: scale(15),
          marginHorizontal: scale(20),
        }}
        textStyle={styles.textButton}
        disabled={messageRating === ''}
        onPress={() => {
          setIsLoading(true);
          onSubmit();
        }}
      />
      <Modal visible={visibleSuccess} animationType="slide" transparent={true}>
        <View style={styles.modalContainer}>
          <View style={styles.modalContent}>
            <Image source={IMG.successSubmit} style={styles.successImage} />
            <Text bold style={styles.successTitle}>
              Submit Success
            </Text>
            <Text style={styles.successSubtitle}>
              Your review has been submitted, you can see it on course detail
              page
            </Text>
            <Button
              title="Back to Home"
              buttonStyle={{
                borderRadius: scale(30),
                backgroundColor: Color.primaryColor,
                marginBottom: scale(15),
                marginHorizontal: scale(20),
                paddingVertical: scale(10),
              }}
              textStyle={styles.textButtonHome}
              onPress={() => {
                setVisibleSuccess(false);
                navigationReset('BottomTab');
              }}
              isLoading={isLoading}
            />
          </View>
        </View>
      </Modal>
    </SafeAreaView>
  );
};

function mapStateToProps(state) {
  return {};
}

function mapDispatchToProps(dispatch) {
  return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(ReviewPage);
