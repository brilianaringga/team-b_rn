import {
  React,
  useEffect,
  // useState,
  connect,
  View,
  Image,
  CommonActions,
  // useRef,
  // Button,
} from 'libraries';
import { Text } from 'components';
import styles from './style';
import { scale } from 'utils';
// import {
//   exampleAction,
//   getDataApi,
// } from 'configs/redux/reducers/example/exampleActions';
import ActionButton from 'react-native-action-button';
import { Icon } from 'react-native-eva-icons';
// import RBSheet from 'react-native-raw-bottom-sheet';
// import { StatusBar } from 'react-native';
import { ListItem } from 'react-native-elements';
import { TouchableOpacity } from 'react-native-gesture-handler';
import * as ImagePicker from 'react-native-image-picker';
import {
  GetProfileAction,
  LogoutAction,
  UpdateImageAction,
} from '../../configs/redux/reducers/profile/profileActions';
// import { AsyncStorage, showMessage } from 'libraries';
import baseUrl from '../../configs/api/url';
import { Card, CardItem, Body } from 'native-base';

const ProfilePage = (props) => {
  const {
    navigation,
    // dispatchExample,
    // contoh_email,
    // contoh_data,
    // dispatchGetApi,
    // contoh_data_api,
    dispatchGetProfile,
    dispatchUpdateImage,
    dispatchLogout,
    myCoursesCount,
    myCompletedCourseCount,
    myOwnCoursesCount,
    mySubsCourseCount,
    profilePicture,
    fullname,
    email,
  } = props;

  // const refRBSheet = useRef();

  useEffect(() => {
    dispatchGetProfile();
    // console.log('picture');
    // console.log(baseUrl);

    // isi initial first load
    //contoh update state global via redux
    // dispatchExample();
    // const start = 1;
    // const limit = 2;
    // dispatchGetApi(start, limit);
  }, [dispatchGetProfile]);

  useEffect(() => {
    // ini kepanggil hanya ketika redux di contoh_data_api variable berubah
    // console.log(contoh_data_api);
  }, []);

  const uploadImage = () => {
    ImagePicker.launchImageLibrary(
      {
        mediaType: 'photo',
        includeBase64: false,
        quality: 1,
      },
      (response) => {
        //console.log('test')
        //console.log(response.uri)
        //setResponse(response);
        const form = new FormData();
        form.append('profilePicture', {
          uri: response.uri,
          type: 'image/jpg',
          name: 'avatar.jpg',
        });
        const payload = {
          body: form,
          type: 'form-data',
        };
        //console.log(payload)
        dispatchUpdateImage(payload);
      },
    );
  };

  const navigationReset = (page) => {
    navigation.dispatch(
      CommonActions.reset({
        index: 0,
        routes: [{ name: page }],
      }),
    );
  };

  const doLogout = () => {
    dispatchLogout();
    navigationReset('LoginPage');
  };

  return (
    <View style={styles.container}>
      <View
        style={{
          flex: 2.5,
          width: '100%',
          backgroundColor: 'white',
          shadowColor: '#000000',
          shadowOffset: {
            width: 0,
            height: 10,
          },
          shadowRadius: 11,
          shadowOpacity: 0.4,
          elevation: 50,
        }}>
        <View
          style={{
            flex: 1.5,
            width: '100%',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <View style={{ flexDirection: 'row', flex: 1, width: '100%' }}>
            <View
              style={{
                flex: 2,
                flexDirection: 'column',
                justifyContent: 'flex-end',
                height: '100%',
              }}
            />
            <View
              style={{
                flex: 2,
                flexDirection: 'column',
                justifyContent: 'flex-end',
                alignItems: 'center',
                height: '100%',
              }}>
              <Text
                style={{
                  fontSize: scale(20),
                  fontWeight: 'bold',
                  color: '#2D8989',
                }}>
                Profile
              </Text>
            </View>
            <View
              style={{
                flex: 2,
                flexDirection: 'column',
                justifyContent: 'flex-end',
                alignItems: 'flex-end',
                height: '100%',
              }}>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('EditPage');
                }}>
                <Icon
                  name="edit"
                  width={scale(30)}
                  height={scale(30)}
                  fill={'grey'}
                  style={{ alignSelf: 'flex-start', marginRight: scale(20) }}
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>

        <View
          style={{
            flex: 3.5,
            width: '100%',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Image
            // source={require('../../assets/images/foto.jpg')}
            source={{
              uri:
                profilePicture == null
                  ? 'https://ualr.edu/studentaffairs/files/2020/01/blank-picture-holder.png'
                  : baseUrl.profil.getprofilepicture + profilePicture,
            }}
            style={{
              width: scale(150),
              height: scale(150),
              borderRadius: scale(150),
            }}
          />
          <ActionButton
            buttonColor="#2D8989"
            offsetY={30}
            offsetX={110}
            size={50}
            onPress={() => {
              navigation.navigate('AvatarPage');
            }}
          />
        </View>
        <View style={{ flex: 1, width: '100%', alignItems: 'center' }}>
          <Text style={{ fontSize: scale(20), fontWeight: 'bold' }}>
            {fullname}
          </Text>
          <Text style={{ color: 'grey' }}>{email}</Text>
        </View>
      </View>
      <View style={{ flex: 3.5, width: '100%', backgroundColor: '#F9FAFC' }}>
        <View style={{ flex: 1, margin: scale(10) }}>
          <View>
            <View style={{ flexWrap: 'wrap', flexDirection: 'row' }}>
              <View
                style={{
                  width: '50%',
                  height: scale(150),
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Card
                  style={{
                    width: '70%',
                    borderRadius: scale(10),
                    backgroundColor: 'powderblue',
                  }}>
                  <CardItem
                    style={{
                      borderRadius: scale(10),
                      backgroundColor: 'powderblue',
                    }}
                    button
                    onPress={() => alert('This is Card Body')}>
                    <Body
                      style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <Text
                        style={{
                          fontSize: scale(50),
                          fontWeight: 'bold',
                          color: 'white',
                          marginBottom: scale(15),
                        }}>
                        {myCoursesCount}
                      </Text>
                      <Text style={{ fontSize: scale(20), color: 'white' }}>
                        {' '}
                        My Course
                      </Text>
                    </Body>
                  </CardItem>
                </Card>
              </View>

              <View
                style={{
                  width: '50%',
                  height: scale(150),
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Card
                  style={{
                    width: '70%',
                    borderRadius: scale(10),
                    backgroundColor: 'powderblue',
                  }}>
                  <CardItem
                    style={{
                      borderRadius: scale(10),
                      backgroundColor: 'powderblue',
                    }}
                    button
                    onPress={() => alert('This is Card Body')}>
                    <Body
                      style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <Text
                        style={{
                          fontSize: scale(50),
                          fontWeight: 'bold',
                          color: 'white',
                        }}>
                        {myCompletedCourseCount}
                      </Text>
                      <Text
                        style={{
                          fontSize: scale(15),
                          textAlign: 'center',
                          color: 'white',
                        }}>
                        {' '}
                        My Complete Course
                      </Text>
                    </Body>
                  </CardItem>
                </Card>
              </View>
              <View
                style={{
                  width: '50%',
                  height: scale(150),
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Card
                  style={{
                    width: '70%',
                    borderRadius: scale(10),
                    backgroundColor: 'powderblue',
                  }}>
                  <CardItem
                    style={{
                      borderRadius: scale(10),
                      backgroundColor: 'powderblue',
                    }}
                    button
                    onPress={() => alert('This is Card Body')}>
                    <Body
                      style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <Text
                        style={{
                          fontSize: scale(50),
                          fontWeight: 'bold',
                          color: 'white',
                          marginBottom: scale(5),
                        }}>
                        {myOwnCoursesCount}
                      </Text>
                      <Text
                        style={{
                          fontSize: scale(15),
                          textAlign: 'center',
                          color: 'white',
                        }}>
                        {' '}
                        My Own Courses
                      </Text>
                    </Body>
                  </CardItem>
                </Card>
              </View>
              <View
                style={{
                  width: '50%',
                  height: scale(150),
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Card
                  style={{
                    width: '70%',
                    borderRadius: scale(10),
                    backgroundColor: 'powderblue',
                  }}>
                  <CardItem
                    style={{
                      borderRadius: scale(10),
                      backgroundColor: 'powderblue',
                    }}
                    button
                    onPress={() => alert('This is Card Body')}>
                    <Body
                      style={{
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <Text
                        style={{
                          fontSize: scale(50),
                          fontWeight: 'bold',
                          color: 'white',
                          marginBottom: scale(5),
                        }}>
                        {mySubsCourseCount}
                      </Text>
                      <Text
                        style={{
                          fontSize: scale(15),
                          textAlign: 'center',
                          color: 'white',
                        }}>
                        {' '}
                        My Subs Courses
                      </Text>
                    </Body>
                  </CardItem>
                </Card>
              </View>
            </View>
          </View>
          {/* <View>
            <ListItem
              containerStyle={{
                borderTopLeftRadius: scale(10),
                borderTopRightRadius: scale(10),
                backgroundColor: 'white',
              }}
              bottomDivider>
              <Icon
                name="clipboard-outline"
                width={scale(30)}
                height={scale(30)}
                fill={'grey'}
                style={{ alignSelf: 'flex-start', marginRight: scale(20) }}
              />
              <ListItem.Content>
                <ListItem.Title
                  style={{ color: '#2D8989', fontWeight: 'bold' }}>
                  My Course
                </ListItem.Title> */}
          {/* <ListItem.Subtitle style={{ color: 'red' }}>
                Vice Chairman
             </ListItem.Subtitle> */}
          {/* </ListItem.Content>
              <Badge status="error" value={myCoursesCount}></Badge>
              <Icon
                name="arrow-ios-forward-outline"
                width={scale(30)}
                height={scale(30)}
                fill={'grey'}
              />
            </ListItem>

            <ListItem
              containerStyle={{
                borderBottomRightRadius: scale(10),
                borderBottomLeftRadius: scale(10),
              }}
              bottomDivider>
              <Icon
                name="clipboard-outline"
                width={scale(30)}
                height={scale(30)}
                fill={'grey'}
                style={{ alignSelf: 'flex-start', marginRight: scale(20) }}
              />
              <ListItem.Content>
                <ListItem.Title
                  style={{ color: '#2D8989', fontWeight: 'bold' }}>
                  Complete Course
                </ListItem.Title> */}
          {/* <ListItem.Subtitle style={{ color: 'red' }}>
                Vice Chairman
             </ListItem.Subtitle> */}
          {/* </ListItem.Content>
              <Badge status="error" value={myCompletedCourseCount}></Badge>
              <Icon
                name="arrow-ios-forward-outline"
                width={scale(30)}
                height={scale(30)}
                fill={'grey'}
              />
            </ListItem>

            <ListItem
              containerStyle={{
                borderBottomRightRadius: scale(10),
                borderBottomLeftRadius: scale(10),
              }}
              bottomDivider>
              <Icon
                name="clipboard-outline"
                width={scale(30)}
                height={scale(30)}
                fill={'grey'}
                style={{ alignSelf: 'flex-start', marginRight: scale(20) }}
              />
              <ListItem.Content>
                <ListItem.Title
                  style={{ color: '#2D8989', fontWeight: 'bold' }}>
                  My Own Courses
                </ListItem.Title> */}
          {/* <ListItem.Subtitle style={{ color: 'red' }}>
                Vice Chairman
             </ListItem.Subtitle> */}
          {/* </ListItem.Content>
              <Badge status="error" value={myOwnCoursesCount}></Badge>
              <Icon
                name="arrow-ios-forward-outline"
                width={scale(30)}
                height={scale(30)}
                fill={'grey'}
              />
            </ListItem>

            <ListItem
              containerStyle={{
                borderBottomRightRadius: scale(10),
                borderBottomLeftRadius: scale(10),
              }}
              bottomDivider>
              <Icon
                name="clipboard-outline"
                width={scale(30)}
                height={scale(30)}
                fill={'grey'}
                style={{ alignSelf: 'flex-start', marginRight: scale(20) }}
              />
              <ListItem.Content>
                <ListItem.Title
                  style={{ color: '#2D8989', fontWeight: 'bold' }}>
                  My Subs Courses
                </ListItem.Title> */}
          {/* <ListItem.Subtitle style={{ color: 'red' }}>
                Vice Chairman
             </ListItem.Subtitle> */}
          {/* </ListItem.Content>
              <Badge status="error" value={mySubsCourseCount}></Badge>
              <Icon
                name="arrow-ios-forward-outline"
                width={scale(30)}
                height={scale(30)}
                fill={'grey'}
              />
            </ListItem>
          </View> */}

          <TouchableOpacity
            onPress={() => {
              navigation.navigate('LoginPage');
            }}>
            <View style={{ marginTop: scale(20) }}>
              <ListItem
                containerStyle={{
                  borderRadius: scale(10),
                }}>
                <Icon
                  name="log-out-outline"
                  width={scale(30)}
                  height={scale(30)}
                  fill={'grey'}
                  style={{ alignSelf: 'flex-start', marginRight: scale(20) }}
                />
                <ListItem.Content>
                  <ListItem.Title
                    style={{ color: '#2D8989', fontWeight: 'bold' }}>
                    Logout
                  </ListItem.Title>
                </ListItem.Content>
                <Icon
                  name="arrow-ios-forward-outline"
                  width={scale(30)}
                  height={scale(30)}
                  fill={'grey'}
                />
              </ListItem>
            </View>
          </TouchableOpacity>
        </View>
      </View>

      {/* <View style={{flex:1, justifyContent:'center'}}>
      <StatusBar backgroundColor={'#F9FAFC'} barStyle={'dark-content'} />
        <View style={{flexDirection:'row', justifyContent:'center', marginTop: scale(20), marginBottom: scale(0)}}>
               <Image
                  source={require('../../assets/images/foto.jpg')}
                  //source={{uri: profile_picture == "" ? 'https://ualr.edu/studentaffairs/files/2020/01/blank-picture-holder.png':'http://139.59.124.53:3003'+profile_picture}}
                  style={{width: scale(150),height: scale(150), borderRadius : scale (150)}}></Image>
               <ActionButton
                  buttonColor='#1E59B5'
                  offsetY={10}
                  offsetX={100}
                  size={50}
                  //onPress = {uploadImage}
                  >
              </ActionButton>
         </View> */}

      {/* <View
        style={{
        justifyContent: "center",
        alignItems: "center",
        marginTop: scale(20),
        marginHorizontal:scale(60),
        backgroundColor: "#1E59B5",
        borderRadius: scale(50),
      }}
    >
      <Button title="About Course" onPress={() => refRBSheet.current.open()} />
      <RBSheet
        ref={refRBSheet}
        closeOnDragDown={true}
        closeOnPressMask={false}
        customStyles={{
          wrapper: {
            backgroundColor: "transparent"
          },
          draggableIcon: {
            backgroundColor: "#1E59B5"
          }
        }}
      > */}
      {/* <YourOwnComponent /> */}
      {/* </RBSheet>
    </View> */}

      {/* </View> */}
    </View>
  );
};

function mapStateToProps(state) {
  return {
    //isi state dari reducer
    // contoh_data: state.exampleStore.contoh_data,
    // contoh_email: state.exampleStore.contoh_email,
    // contoh_data_api: state.exampleStore.contoh_data_api,
    myCoursesCount: state.profileStore.myCoursesCount,
    myCompletedCourseCount: state.profileStore.myCompletedCourseCount,
    myOwnCoursesCount: state.profileStore.myOwnCoursesCount,
    mySubsCourseCount: state.profileStore.myCoursesCount,
    fullname: state.profileStore.fullname,
    email: state.profileStore.email,
    profilePicture: state.profileStore.profilePicture,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    //isi dispatch
    // dispatchExample: () => dispatch(exampleAction()),
    // dispatchGetApi: (start, limit) => dispatch(getDataApi(start, limit)),

    dispatchGetProfile: () => dispatch(GetProfileAction()),
    dispatchUpdateImage: (payload) => dispatch(UpdateImageAction(payload)),
    dispatchLogout: () => dispatch(LogoutAction()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfilePage);
