import { StatusBar, StyleSheet } from 'react-native';
import { Color, scale, METRICS } from 'utils';
const styles = StyleSheet.create({
  backgroundVideo: {
    position: 'absolute',
    top: '20%',
    left: 0,
    right: 0,
    width: METRICS.window.width,
    height: '100%',
  },
  pdf: {
    flex: 1,
    width: METRICS.window.width,
    // height: METRICS.window.height,
  },
  mainContainer: {
    flex: 1,
    backgroundColor: Color.backgroundColor,
  },
  imageContainer: {
    flex: 0.5,
    justifyContent: 'center',
  },
  detailsContainer: {
    flex: 0.05,
    justifyContent: 'center',
    alignItems: 'center',
  },
  controlsContainer: {
    flex: 0.45,
    justifyContent: 'flex-start',
  },
  albumImage: {
    width: 250,
    height: 250,
    alignSelf: 'center',
    borderRadius: 40,
  },
  progressBar: {
    height: 20,
    paddingBottom: 90,
  },
  songTitle: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  artist: {
    fontSize: 14,
  },

  music_logo_view: {
    height: '30%',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  image_view: {
    height: '100%',
    width: '50%',
    borderRadius: 10,
  },
  name_of_song_View: {
    height: '15%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  name_of_song_Text1: {
    fontSize: scale(25),
    fontWeight: '500',
  },
  name_of_song_Text2: {
    color: '#808080',
    marginTop: '4%',
    fontSize: scale(15),
    paddingHorizontal: scale(15),
    textAlign: 'center',
  },
  slider_view: {
    height: '10%',
    width: '100%',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  slider_style: {
    height: '70%',
    width: '70%',
  },
  slider_time: {
    fontSize: 15,
    // marginHorizontal: '3%',
    color: '#808080',
    width: scale(50),
    textAlign: 'center',
  },
  functions_view: {
    flexDirection: 'row',
    height: '10%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    flex: 1,
    backgroundColor: Color.backgroundColor,
  },
  navbar: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: StatusBar.currentHeight + scale(10),
    marginBottom: scale(20),
    paddingHorizontal: scale(10),
  },
  backButton: {
    backgroundColor: Color.primaryColor,
    borderRadius: scale(50),
    padding: scale(4),
  },
  titleMaterial: {
    fontSize: scale(25),
    color: Color.primaryColor,
    marginLeft: scale(10),
    textTransform: 'capitalize',
    flex: 1,
  },
  videoWrapper: { flex: 1, position: 'relative' },
  pdfContainer: { flex: 1 },
  musicControl: { marginHorizontal: '5%' },
});

export default styles;
