import { React, useEffect, useState, connect, View } from 'libraries';
import { Text } from 'components';
import styles from './style';
import { appString } from 'utils';
import {
  exampleAction,
  getDataApi,
} from 'configs/redux/reducers/example/exampleActions';

const ExamplePage = (props) => {
  const {
    navigation,
    dispatchExample,
    contoh_email,
    contoh_data,
    dispatchGetApi,
    contoh_data_api,
  } = props;

  useEffect(() => {
    // isi initial first load
    //contoh update state global via redux
    dispatchExample();
    const start = 1;
    const limit = 2;
    dispatchGetApi(start, limit);
  }, []);

  useEffect(() => {
    // ini kepanggil hanya ketika redux di contoh_data_api variable berubah
    console.log(contoh_data_api);
  }, [contoh_data_api]);

  return (
    <View style={styles.container}>
      <Text>Ini Example Page</Text>
      <Text>contoh ambil redux</Text>
      <Text>{contoh_email}</Text>
      <Text>{contoh_data}</Text>

      <Text>
        Contoh ambil data dari appString yang sudah di define di utils/strings
      </Text>
      <Text>{appString.pages.home.title}</Text>
    </View>
  );
};

function mapStateToProps(state) {
  return {
    //isi state dari reducer
    contoh_data: state.exampleStore.contoh_data,
    contoh_email: state.exampleStore.contoh_email,
    contoh_data_api: state.exampleStore.contoh_data_api,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    //isi dispatch
    dispatchExample: () => dispatch(exampleAction()),
    dispatchGetApi: (start, limit) => dispatch(getDataApi(start, limit)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ExamplePage);
