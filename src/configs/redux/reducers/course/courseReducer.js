const defaultState = {
  course_category: {
    data: [],
    isLoading: false,
    error: {},
  },
  recent_course: {
    data: [],
    isLoading: false,
    error: {},
  },
  new_course: {
    data: [],
    isLoading: false,
    error: {},
  },
  popular_course: {
    data: [],
    isLoading: false,
    error: {},
  },
  most_like_course: {
    data: [],
    isLoading: false,
    error: {},
  },
  suitable_course: {
    data: [],
    isLoading: false,
    error: {},
  },
  commentList: {
    data: [],
    isLoading: false,
    error: {},
    isEnd: false,
  },
  similarCourse: {
    data: [],
    isLoading: false,
    error: {},
  },
  coursePagination: {
    data: [],
    isLoading: false,
    error: {},
    isEnd: false,
    isLoadingNext: false,
  },
  material: {
    data: {
      materials: [],
      isExam: false,
    },
    isLoading: false,
    error: {},
  },
  examQuestion: {
    data: [],
    isLoading: false,
    error: {},
  },
  preference: {
    data: [],
    isLoading: false,
    error: {},
  },
  courseCheck: {
    data: {
      isCompleted: false,
      isSubscribed: false,
      isExam: false,
      mark: null,
      isReview: false,
    },
    isLoading: false,
    error: {},
  },
};

export default (state = defaultState, action = {}) => {
  switch (action.type) {
    //Category
    case 'GETTING_CATEGORY':
      return {
        ...state,
        course_category: {
          ...state.course_category,
          isLoading: true,
          data: [],
          error: {},
        },
      };
    case 'UPDATE_COURSE_CATEGORY':
      return {
        ...state,
        course_category: {
          ...state.course_category,
          data: action.payload,
          isLoading: false,
        },
      };
    case 'UPDATE_COURSE_CATEGORY_FAILED':
      return {
        ...state,
        course_category: {
          ...state.course_category,
          error: action.payload,
          isLoading: false,
        },
      };

    //Popular Course
    case 'GETTING_POPULAR_COURSE':
      return {
        ...state,
        popular_course: {
          ...state.popular_course,
          isLoading: true,
          data: [],
          error: {},
        },
      };
    case 'UPDATE_POPULAR_COURSE':
      return {
        ...state,
        popular_course: {
          ...state.popular_course,
          data: action.payload,
          isLoading: false,
        },
      };
    case 'UPDATE_POPULAR_COURSE_FAILED':
      return {
        ...state,
        popular_course: {
          ...state.popular_course,
          error: action.payload,
          isLoading: false,
        },
      };
    //New Course
    case 'GETTING_NEW_COURSE':
      return {
        ...state,
        new_course: {
          ...state.new_course,
          isLoading: true,
          data: [],
          error: {},
        },
      };
    case 'UPDATE_NEW_COURSE':
      return {
        ...state,
        new_course: {
          ...state.new_course,
          data: action.payload,
          isLoading: false,
        },
      };
    case 'UPDATE_NEW_COURSE_FAILED':
      return {
        ...state,
        new_course: {
          ...state.new_course,
          error: action.payload,
          isLoading: false,
        },
      };
    //Recent Course
    case 'GETTING_RECENT_COURSE':
      return {
        ...state,
        recent_course: {
          ...state.recent_course,
          isLoading: true,
          data: [],
          error: {},
        },
      };
    case 'UPDATE_RECENT_COURSE':
      return {
        ...state,
        recent_course: {
          ...state.recent_course,
          data: action.payload,
          isLoading: false,
        },
      };
    case 'UPDATE_RECENT_COURSE_FAILED':
      return {
        ...state,
        recent_course: {
          ...state.recent_course,
          error: action.payload,
          isLoading: false,
        },
      };
    //Suitable Course
    case 'GETTING_SUITABLE_COURSE':
      return {
        ...state,
        suitable_course: {
          ...state.suitable_course,
          isLoading: true,
          data: [],
          error: {},
        },
      };
    case 'UPDATE_SUITABLE_COURSE':
      return {
        ...state,
        suitable_course: {
          ...state.suitable_course,
          data: action.payload,
          isLoading: false,
        },
      };
    case 'UPDATE_SUITABLE_COURSE_FAILED':
      return {
        ...state,
        suitable_course: {
          ...state.suitable_course,
          error: action.payload,
          isLoading: false,
        },
      };
    //Most Like Course
    case 'GETTING_MOST_LIKE_COURSE':
      return {
        ...state,
        most_like_course: {
          ...state.most_like_course,
          isLoading: true,
          data: [],
          error: {},
        },
      };
    case 'UPDATE_MOST_LIKE_COURSE':
      return {
        ...state,
        most_like_course: {
          ...state.most_like_course,
          data: action.payload,
          isLoading: false,
        },
      };
    case 'UPDATE_MOST_LIKE_COURSE_FAILED':
      return {
        ...state,
        most_like_course: {
          ...state.most_like_course,
          error: action.payload,
          isLoading: false,
        },
      };

    //Get Paging Comment List
    case 'GETTING_COMMENT':
      return {
        ...state,
        commentList: {
          ...state.commentList,
          isLoading: true,
          error: {},
        },
      };
    case 'GETTING_COMMENT_RESET':
      return {
        ...state,
        commentList: {
          ...state.commentList,
          isLoading: true,
          isEnd: false,
          data: [],
          error: {},
        },
      };
    case 'UPDATE_COMMENT':
      return {
        ...state,
        commentList: {
          ...state.commentList,
          data: [...state.commentList.data, ...action.payload],
          isLoading: false,
          isEnd: action.isEnd,
        },
      };
    case 'UPDATE_COMMENT_FAILED':
      return {
        ...state,
        commentList: {
          ...state.commentList,
          error: action.payload,
          isLoading: false,
        },
      };

    //Get Similar Course
    case 'GETTING_SIMILAR':
      return {
        ...state,
        similarCourse: {
          ...state.similarCourse,
          isLoading: true,
          data: [],
          error: {},
        },
      };
    case 'UPDATE_SIMILAR':
      return {
        ...state,
        similarCourse: {
          ...state.similarCourse,
          data: action.payload,
          isLoading: false,
        },
      };
    case 'UPDATE_SIMILAR_FAILED':
      return {
        ...state,
        similarCourse: {
          ...state.similarCourse,
          error: action.payload,
          isLoading: false,
        },
      };

    //Get Paging Course List
    case 'GETTING_COURSE_PAGINATION':
      return {
        ...state,
        coursePagination: {
          ...state.coursePagination,
          isLoadingNext: true,
          error: {},
        },
      };
    case 'GETTING_COURSE_PAGINATION_RESET':
      return {
        ...state,
        coursePagination: {
          ...state.coursePagination,
          isLoading: true,
          isEnd: false,
          data: [],
          error: {},
        },
      };
    case 'UPDATE_COURSE_PAGINATION':
      return {
        ...state,
        coursePagination: {
          ...state.coursePagination,
          data: [...state.coursePagination.data, ...action.payload],
          isLoading: false,
          isEnd: action.isEnd,
        },
      };
    case 'UPDATE_COURSE_PAGINATION_FAILED':
      return {
        ...state,
        coursePagination: {
          ...state.coursePagination,
          error: action.payload,
          isLoading: false,
        },
      };

    //Get Read Material
    case 'GETTING_MATERIAL':
      return {
        ...state,
        material: {
          ...state.material,
          isLoading: true,
          data: {
            materials: [],
            isExam: false,
          },
          error: {},
        },
      };
    case 'UPDATE_MATERIAL':
      return {
        ...state,
        material: {
          ...state.material,
          data: action.payload,
          isLoading: false,
        },
      };
    case 'UPDATE_MATERIAL_FAILED':
      return {
        ...state,
        material: {
          ...state.material,
          error: action.payload,
          isLoading: false,
        },
      };

    //Get Exam
    case 'GETTING_EXAM':
      return {
        ...state,
        examQuestion: {
          ...state.examQuestion,
          isLoading: true,
          data: [],
          error: {},
        },
      };
    case 'UPDATE_EXAM':
      return {
        ...state,
        examQuestion: {
          ...state.examQuestion,
          data: action.payload,
          isLoading: false,
        },
      };
    case 'UPDATE_EXAM_FAILED':
      return {
        ...state,
        examQuestion: {
          ...state.examQuestion,
          error: action.payload,
          isLoading: false,
        },
      };

    //Preference
    case 'GETTING_PREFERENCE':
      return {
        ...state,
        preference: {
          ...state.preference,
          isLoading: true,
          data: [],
          error: {},
        },
      };
    case 'UPDATE_COURSE_PREFERENCE':
      return {
        ...state,
        preference: {
          ...state.preference,
          data: action.payload,
          isLoading: false,
        },
      };
    case 'UPDATE_COURSE_PREFERENCE_FAILED':
      return {
        ...state,
        preference: {
          ...state.preference,
          error: action.payload,
          isLoading: false,
        },
      };

    case 'GETTING_COURSE_CHECK':
      return {
        ...state,
        courseCheck: {
          ...state.courseCheck,
          data: {
            isCompleted: false,
            isSubscribed: false,
            isExam: false,
            mark: null,
            isReview: false,
          },
          isLoading: true,
          error: {},
        },
      };
    case 'UPDATE_COURSE_CHECK':
      return {
        ...state,
        courseCheck: {
          ...state.courseCheck,
          data: action.payload,
          isLoading: false,
        },
      };
    case 'UPDATE_COURSE_CHECK_FAILED':
      return {
        ...state,
        courseCheck: {
          ...state.courseCheck,
          error: action.payload,
          isLoading: false,
        },
      };

    default:
      return state;
  }
};
