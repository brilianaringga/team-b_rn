const defaultState = {
  fullname: '',
  username: '',
  email: '',
  password: '',
  profilePicture: null,
  myCoursesCount: 0,
  isLoading: false,
  myCompletedCourseCount: 0,
  mySubsCourseCount: 0,
  trainer: {
    data: {
      fullname: '',
      avgRating: '',
      reviewer: 0,
      profilePicture: null,
      username: '',
    },
    isLoading: false,
    error: {},
  },
  notification: {
    data: [],
    isLoading: false,
    error: {},
    isNew: false,
  },
};

export default (state = defaultState, action = {}) => {
  switch (action.type) {
    case 'GET_PROFILE':
      return {
        ...state,
        fullname: action.payload.fullname,
        username: action.payload.username,
        email: action.payload.email,
        password: action.payload.password,
        profilePicture: action.payload.profilePicture,
        myCoursesCount: action.payload.myCoursesCount,
        myCompletedCourseCount: action.payload.myCompletedCourseCount,
        myOwnCoursesCount: action.payload.myOwnCoursesCount,
        mySubsCourseCount: action.payload.mySubsCourseCount,
      };

    case 'UPDATE_IMAGE_PROFILE':
      return {
        ...state,
        profilePicture: action.payload.profPict,
      };

    case 'REQUEST_API':
      return {
        ...state,
        isLoading: action.payload,
      };

    case 'UPDATE_PROFILE':
      return {
        ...state,
        fullname: action.payload.fullname,
        username: action.payload.username,
        email: action.payload.email,
        // password: action.payload.password,
      };

    case 'ONCHANGE_FULLNAME':
      return {
        ...state,
        fullname: action.payload.fullname,
      };

    case 'ONCHANGE_USERNAME':
      return {
        ...state,
        username: action.payload.username,
      };

    case 'ONCHANGE_EMAIL':
      return {
        ...state,
        email: action.payload.email,
      };

    case 'ONCHANGE_PASSWORD':
      return {
        ...state,
        password: action.payload.password,
      };

    case 'GET_TRAINER_PROFILE': {
      return {
        ...state,
        trainer: {
          ...state.trainer,
          isLoading: true,
        },
      };
    }
    case 'TRAINER_DATA': {
      return {
        ...state,
        trainer: {
          ...state.trainer,
          isLoading: false,
          data: action.payload,
        },
      };
    }
    case 'TRAINER_DATA_FAILED': {
      return {
        ...state,
        trainer: {
          ...state.trainer,
          isLoading: false,
          error: action.payload,
        },
      };
    }

    //Notification
    case 'GET_NOTIFICATION': {
      return {
        ...state,
        notification: {
          ...state.notification,
          isLoading: true,
        },
      };
    }
    case 'NOTIFICATION_DATA': {
      let filterdData = action.payload.filter((item) => item.isRead === false);
      console.log('jumlah', filterdData.length);
      return {
        ...state,
        notification: {
          ...state.notification,
          isLoading: false,
          data: action.payload,
          isNew: filterdData.length > 0 ? true : false,
        },
      };
    }
    case 'NOTIFICATION_DATA_FAILED': {
      return {
        ...state,
        notification: {
          ...state.notification,
          isNew: false,
          isLoading: false,
          error: action.payload,
        },
      };
    }

    default:
      return state;
  }
};
