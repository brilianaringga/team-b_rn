import API from 'configs/api';
import { AsyncStorage } from 'libraries';
import Toast from 'react-native-toast-message';
import * as RootNavigation from 'configs/routes/RootNavigation';
import { expiredAction, setEmailVerified } from '../auth/authActions';

/*
Example Action
*/

export function getTrainerProfile(payload) {
  return async (dispatch) => {
    try {
      dispatch({
        type: 'GET_TRAINER_PROFILE',
      });

      const res = await API.getTrainerProfile(payload);
      if (res) {
        dispatch({
          type: 'TRAINER_DATA',
          payload: res.data,
        });
      } else {
        dispatch({
          type: 'TRAINER_DATA',
          payload: {},
        });
      }
    } catch (e) {
      if (e.status == 401) {
        dispatch(expiredAction());
      }
      dispatch({
        type: 'TRAINER_DATA_FAILED',
        payload: e,
      });
    }
  };
}

export const GetProfileAction = (payload) => {
  return async (dispatch) => {
    try {
      const res = await API.getProfile(payload);
      console.log(res.data);
      // CATCH POST LOGIN SUCCESS
      if (res) {
        dispatch({
          type: 'GET_PROFILE',
          payload: {
            fullname: res.data.fullname,
            username: res.data.username,
            email: res.data.email,
            password: res.data.password,
            profilePicture: res.data.profilePicture,
            myCoursesCount: res.data.asTrainee.myCoursesCount,
            myCompletedCourseCount: res.data.asTrainee.myCompletedCourseCount,
            myOwnCoursesCount: res.data.asTrainer.myOwnCoursesCount,
            mySubsCourseCount: res.data.asTrainer.mySubsCourseCount,
          },
        });
        dispatch(setEmailVerified(res.data.isEmailVerified));
      }
    } catch (err) {
      if (err.status == 401) {
        dispatch(expiredAction());
      }
      Toast.show({
        text1: 'Something Wrong',
        position: 'bottom',
        type: 'error',
      });
      console.log(err);
    }
  };
};

export const UpdateImageAction = (payload) => {
  return async (dispatch) => {
    dispatch({
      type: 'REQUEST_API',
      payload: true,
    });
    //console.log('dispatch')
    //console.log(payload)
    try {
      const res = await API.uploadProfilePicture(payload);
      console.log(res);
      // CATCH POST UPDATE IMAGE SUCCESS
      if (res) {
        dispatch({
          type: 'UPDATE_IMAGE_PROFILE',
          payload: {
            profPict: res.data.profPict,
          },
        });
        dispatch({
          type: 'REQUEST_API',
          payload: false,
        });
        RootNavigation.navigate('BottomTab', { screen: 'ProfilePage' });
      }
    } catch (err) {
      if (err.status == 401) {
        dispatch(expiredAction());
      }
      Toast.show({
        text1: 'Something Wrong',
        position: 'bottom',
        type: 'error',
      });
    }
  };
};

export const EditProfileAction = (payload) => {
  return async (dispatch) => {
    try {
      const res = await API.updateProfile(payload);
      console.log(res);
      // CATCH POST UPDATE PROFIL SUCCESS
      if (res) {
        dispatch({
          type: 'UPDATE_PROFILE',
          payload: {
            fullname: res.data.fullname,
            username: res.data.username,
            email: res.data.email,
            //   password: res.data.password,
          },
        });
      }
    } catch (err) {
      if (err.status == 401) {
        dispatch(expiredAction());
      }
      alert('Error');
    }
  };
};

export const OnChangeFullname = (payload) => {
  return async (dispatch) => {
    dispatch({
      type: 'ONCHANGE_FULLNAME',
      payload: {
        fullname: payload.fullname,
      },
    });
  };
};

export const OnChangeUsername = (payload) => {
  return async (dispatch) => {
    dispatch({
      type: 'ONCHANGE_USERNAME',
      payload: {
        username: payload.username,
      },
    });
  };
};

export const OnChangeEmail = (payload) => {
  return async (dispatch) => {
    dispatch({
      type: 'ONCHANGE_EMAIL',
      payload: {
        email: payload.email,
      },
    });
  };
};

export const OnChangePassword = (payload) => {
  return async (dispatch) => {
    dispatch({
      type: 'ONCHANGE_PASSWORD',
      payload: {
        password: payload.password,
      },
    });
  };
};

export const LogoutAction = (payload) => {
  return async (dispatch) => {
    await AsyncStorage.removeItem('@token');
  };
};

export const resyncProfile = (res) => {
  return async (dispatch) => {
    await dispatch({
      type: 'GET_PROFILE',
      payload: {
        fullname: res.data.fullname,
        username: res.data.username,
        email: res.data.email,
        password: res.data.password,
        profilePicture: res.data.profilePicture,
        myCoursesCount: res.data.asTrainee.myCoursesCount,
        myCompletedCourseCount: res.data.asTrainee.myCompletedCourseCount,
        myOwnCoursesCount: res.data.asTrainer.myOwnCoursesCount,
        mySubsCourseCount: res.data.asTrainer.mySubsCourseCount,
      },
    });
    RootNavigation.reset('BottomTab');
  };
};

export const getNotification = () => {
  return async (dispatch) => {
    try {
      dispatch({
        type: 'GET_NOTIFICATION',
      });

      const res = await API.getNotification();
      if (res) {
        dispatch({
          type: 'NOTIFICATION_DATA',
          payload: res.data,
        });
      } else {
        dispatch({
          type: 'NOTIFICATION_DATA',
          payload: {},
        });
      }
    } catch (e) {
      if (e.status == 401) {
        dispatch(expiredAction());
      }
      dispatch({
        type: 'NOTIFICATION_DATA_FAILED',
        payload: e,
      });
    }
  };
};

export const readNotification = (payload) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: 'GET_READ_NOTIFICATION',
      });

      const res = await API.readNotification(payload);
      if (res) {
        dispatch(getNotification());
      }
    } catch (e) {
      if (e.status == 401) {
        dispatch(expiredAction());
      }
      dispatch({
        type: 'READ_NOTIFICATION_DATA_FAILED',
        payload: e,
      });
    }
  };
};
