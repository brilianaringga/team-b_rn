const defaultState = {
  contoh_data: '',
  contoh_email: '',
  contoh_data_api: [],
};

export default (state = defaultState, action = {}) => {
  switch (action.type) {
    case 'UPDATE_DATA': {
      return {
        ...state,
        contoh_data: action.payload,
      };
    }
    case 'UPDATE_EMAIL': {
      return {
        ...state,
        contoh_email: action.payload,
      };
    }
    case 'FETCH_DATA': {
      return {
        ...state,
        contoh_data_api: action.payload,
      };
    }
    default:
      return state;
  }
};
