import API from 'configs/api';

/*
Example Action
*/
export function exampleAction() {
  return async (dispatch) => {
    //contoh dispaatch update data
    dispatch({
      type: 'UPDATE_DATA',
      payload: 'ini data baru',
    });

    //contoh dispaatch update email
    dispatch({
      type: 'UPDATE_EMAIL',
      payload: 'indra.cip@gmail.com',
    });
  };
}

export function getDataApi(start, limit) {
  return async (dispatch) => {
    try {
      //---- untuk case api dengan method Get, yang ad parameter dan headers, bisa di taro  kyk gini ----
      // const payload = {
      //   params: {
      //     _start: start,
      //     _limit: limit
      //   },
      //   headers: {
      //     Authorization: "Bearer ejydcsdhcvdscsbdcvvscvdsvcvdsgcvsdghcv"
      //   }
      // }

      //---- untuk case api dengan method Post, Put, atau Delete yang butuh kirim body dan header,bisa di taro di dalam body and headers kyk gini ----
      // const payload = {
      //   body: {
      //     username: "username",
      //     password: "password"
      //   },
      //   headers: {
      //     Authorization: "Bearer ejydcsdhcvdscsbdcvvscvdsvcvdsgcvsdghcv"
      //   }
      // }

      const payload = {
        params: {
          _start: start,
          _limit: limit,
        },
      };
      const data = await API.getPhotos(payload);
      if (data) {
        dispatch({
          type: 'FETCH_DATA',
          payload: data,
        });
        return Promise.resolve(data);
      }
      return Promise.reject();
    } catch (e) {
      console.log('getDataApi Err: \n', e);
      return Promise.reject(e);
    }
  };
}
