import API from 'configs/api';
import * as RootNavigation from 'configs/routes/RootNavigation';
import { StatusBar } from 'react-native';
import { showMessage } from 'react-native-flash-message';
import { MyCourseActionType } from 'utils';
import { expiredAction } from '../auth/authActions';

// course list
export const fetchMyCourse = () => {
  return async (dispatch) => {
    try {
      dispatch({
        type: MyCourseActionType.FETCH_MYCOURSE_REQUEST,
      });

      const response = await API.getMyCourse();

      dispatch({
        type: MyCourseActionType.FETCH_MYCOURSE_SUCCESS,
        payload: response.data.course,
      });
    } catch (error) {
      const errResp = error.data.error;
      if (error.status === 401) {
        dispatch(expiredAction());
      }
      if (errResp) {
        switch (errResp.name) {
          case 'empty-wishlist':
            dispatch({
              type: MyCourseActionType.FETCH_MYCOURSE_FAILED,
              payload: [],
              error: error,
            });
            break;
          default:
            dispatch({
              type: MyCourseActionType.FETCH_MYCOURSE_FAILED,
              error: error,
            });
            break;
        }
      } else {
        dispatch({
          type: MyCourseActionType.FETCH_MYCOURSE_FAILED,
          error: error,
        });
      }
    }
  };
};

// course create
export const setSelectedCategory = (payload) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: MyCourseActionType.SET_SELECTED_COURSE,
        payload: payload,
      });
    } catch (err) {
      console.log(err);
    }
  };
};

export const postCourse = (payload) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: MyCourseActionType.POST_COURSE_REQUEST,
      });

      const response = await API.createCourse(payload);

      dispatch({
        type: MyCourseActionType.POST_COURSE_SUCCESS,
        payload: response.name,
      });

      showMessage({
        icon: 'checkmark-circle-outline',
        message: 'Success',
        description: 'Your course has been created',
        type: 'success',
        autoHide: true,
        statusBarHeight: StatusBar.currentHeight,
      });

      // RootNavigation.replaceScreen('CourseEditPage', {
      //   data: { ...response.data },
      // });
      return response.data;
    } catch (err) {
      showMessage({
        icon: 'auto',
        message: 'Error',
        description: "Oops something wen't wrong",
        type: 'danger',
        autoHide: true,
        statusBarHeight: StatusBar.currentHeight,
      });
      const errResp = err.data.error;
      if (err.status === 401) {
        dispatch(expiredAction());
      }
      if (errResp) {
        switch (errResp.name) {
          case 'empty-wishlist':
            dispatch({
              type: MyCourseActionType.POST_COURSE_FAILED,
              payload: [],
              error: err,
            });
            break;

          default:
            dispatch({
              type: MyCourseActionType.POST_COURSE_FAILED,
              error: err,
            });
            break;
        }
      } else {
        dispatch({
          type: MyCourseActionType.POST_COURSE_FAILED,
          error: err,
        });
      }
    }
  };
};

export const updateCourse = (payload) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: MyCourseActionType.UPDATE_COURSE_REQUEST,
      });

      const response = await API.updateCourse(payload);

      dispatch({
        type: MyCourseActionType.UPDATE_COURSE_SUCCESS,
        payload: response.name,
      });

      showMessage({
        icon: 'checkmark-circle-outline',
        message: 'Success',
        description: response.message,
        type: 'success',
        autoHide: true,
        statusBarHeight: StatusBar.currentHeight,
      });

      return response.data;
    } catch (err) {
      showMessage({
        icon: 'auto',
        message: 'Error',
        description: err.data.error.message,
        type: 'danger',
        autoHide: true,
        statusBarHeight: StatusBar.currentHeight,
      });
      const errResp = err.data.error;
      if (err.status === 401) {
        dispatch(expiredAction());
      }
      if (errResp) {
        switch (errResp.name) {
          default:
            dispatch({
              type: MyCourseActionType.UPDATE_COURSE_FAILED,
              error: err,
            });
            break;
        }
      } else {
        dispatch({
          type: MyCourseActionType.UPDATE_COURSE_FAILED,
          error: err,
        });
      }
    }
  };
};

// course edit
export const postCourseImage = (payload) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: MyCourseActionType.POST_UPLOADIMAGE_REQUEST,
      });

      console.log('masuk redux');

      const response = await API.postUpdatePhoto(payload);

      dispatch({
        type: MyCourseActionType.POST_UPLOADIMAGE_SUCCESS,
        payload: response.data,
      });

      showMessage({
        icon: 'checkmark-circle-outline',
        message: 'Success',
        description: 'Course image updated',
        type: 'success',
        autoHide: true,
        statusBarHeight: StatusBar.currentHeight,
      });
    } catch (error) {
      showMessage({
        icon: 'auto',
        message: 'Error',
        description: error.data.error.message,
        type: 'danger',
        autoHide: true,
        statusBarHeight: StatusBar.currentHeight,
      });
      const errResp = error.data.error;
      if (error.status === 401) {
        dispatch(expiredAction());
      }
      if (errResp) {
        switch (errResp.name) {
          case 'empty-wishlist':
            dispatch({
              type: MyCourseActionType.POST_UPLOADIMAGE_FAILED,
              payload: [],
              error: error,
            });
            break;

          default:
            dispatch({
              type: MyCourseActionType.POST_UPLOADIMAGE_FAILED,
              error: error,
            });
            break;
        }
      } else {
        dispatch({
          type: MyCourseActionType.POST_UPLOADIMAGE_FAILED,
          error: error,
        });
      }
    }
  };
};

// course get materials
export const getMaterials = (payload) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: MyCourseActionType.FETCH_GETMATERIS_REQUEST,
      });

      const response = await API.getMateri(payload);

      dispatch({
        type: MyCourseActionType.FETCH_GETMATERIS_SUCCESS,
        payload: response.data.materials,
      });
    } catch (error) {
      const errResp = error.data.error;
      if (error.status === 401) {
        dispatch(expiredAction());
      }
      if (errResp) {
        switch (errResp.name) {
          case 'empty-wishlist':
            dispatch({
              type: MyCourseActionType.FETCH_GETMATERIS_FAILED,
              payload: [],
              error: error,
            });
            break;

          default:
            dispatch({
              type: MyCourseActionType.FETCH_GETMATERIS_FAILED,
              error: error,
            });
            break;
        }
      } else {
        dispatch({
          type: MyCourseActionType.FETCH_GETMATERIS_FAILED,
          error: error,
        });
      }
    }
  };
};

// materi create
export const postMateri = (payload) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: MyCourseActionType.POST_POSTMATERI_REQUEST,
      });

      const response = await API.postMateri(payload);

      dispatch({
        type: MyCourseActionType.POST_POSTMATERI_SUCCESS,
      });

      showMessage({
        icon: 'checkmark-circle-outline',
        message: 'Success',
        description: response.message,
        type: 'success',
        autoHide: true,
        statusBarHeight: StatusBar.currentHeight,
      });
      // RootNavigation.navigate('MaterialAddPage', {
      //   course: { ...response.data, isNewCreate: true },
      // });
      // RootNavigation.pop(1);
    } catch (error) {
      showMessage({
        icon: 'auto',
        message: 'Error',
        description: error.data.error.message,
        type: 'danger',
        autoHide: true,
        statusBarHeight: StatusBar.currentHeight,
      });
      console.log(error);
      const errResp = error.data.error;
      if (error.status === 401) {
        dispatch(expiredAction());
      }
      if (errResp) {
        switch (errResp.name) {
          case 'empty-wishlist':
            dispatch({
              type: MyCourseActionType.POST_POSTMATERI_FAILED,
              payload: [],
              error: error,
            });
            break;

          default:
            dispatch({
              type: MyCourseActionType.POST_POSTMATERI_FAILED,
              error: error,
            });
            break;
        }
      } else {
        dispatch({
          type: MyCourseActionType.POST_POSTMATERI_FAILED,
          error: error,
        });
      }
    }
  };
};

// materi update
export const updateMateri = (payload) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: MyCourseActionType.UPDATE_MATERI_REQUEST,
      });

      const response = await API.updateMateri(payload);

      dispatch({
        type: MyCourseActionType.UPDATE_MATERI_SUCCESS,
      });

      showMessage({
        icon: 'checkmark-circle-outline',
        message: 'Success',
        description: response.message,
        type: 'success',
        autoHide: true,
        statusBarHeight: StatusBar.currentHeight,
      });
      // RootNavigation.pop(1);
    } catch (error) {
      showMessage({
        icon: 'auto',
        message: 'Error',
        description: error.data.error.message,
        type: 'danger',
        autoHide: true,
        statusBarHeight: StatusBar.currentHeight,
      });
      const errResp = error.data.error;
      if (error.status === 401) {
        dispatch(expiredAction());
      }
      if (errResp) {
        switch (errResp.name) {
          default:
            dispatch({
              type: MyCourseActionType.UPDATE_MATERI_FAILED,
              error: error,
            });
            break;
        }
      } else {
        dispatch({
          type: MyCourseActionType.UPDATE_MATERI_FAILED,
          error: error,
        });
      }
    }
  };
};

// materi file update
export const updateFileMateri = (payload) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: MyCourseActionType.UPDATE_FILEMATERI_REQUEST,
      });

      const response = await API.updateFileMateri(payload);

      dispatch({
        type: MyCourseActionType.UPDATE_FILEMATERI_SUCCESS,
      });

      showMessage({
        icon: 'checkmark-circle-outline',
        message: 'Success',
        description: response.message,
        type: 'success',
        autoHide: true,
        statusBarHeight: StatusBar.currentHeight,
      });
      // RootNavigation.pop(1);
    } catch (error) {
      showMessage({
        icon: 'auto',
        message: 'Error',
        description: error.data.error.message,
        type: 'danger',
        autoHide: true,
        statusBarHeight: StatusBar.currentHeight,
      });
      const errResp = error.data.error;
      if (error.status === 401) {
        dispatch(expiredAction());
      }
      if (errResp) {
        switch (errResp.name) {
          default:
            dispatch({
              type: MyCourseActionType.UPDATE_FILEMATERI_FAILED,
              error: error,
            });
            break;
        }
      } else {
        dispatch({
          type: MyCourseActionType.UPDATE_FILEMATERI_FAILED,
          error: error,
        });
      }
    }
  };
};

// exam get list
export const getExams = (payload) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: MyCourseActionType.FETCH_GETEXAM_REQUEST,
      });

      const response = await API.getAuthorExam(payload);

      console.log(response.data);
      dispatch({
        type: MyCourseActionType.FETCH_GETEXAM_SUCCESS,
        payload: response.data.exams,
      });
    } catch (error) {
      const errResp = error.data.error;
      if (error.status === 401) {
        dispatch(expiredAction());
      }
      if (errResp) {
        switch (errResp.name) {
          default:
            dispatch({
              type: MyCourseActionType.FETCH_GETEXAM_FAILED,
              error: error,
            });
            break;
        }
      } else {
        dispatch({
          type: MyCourseActionType.FETCH_GETMATERIS_FAILED,
          error: error,
        });
      }
    }
  };
};

// exam create
export const postExam = (payload) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: MyCourseActionType.POST_EXAM_REQUEST,
      });

      const response = await API.postExam(payload);

      dispatch({
        type: MyCourseActionType.POST_EXAM_SUCCESS,
      });

      showMessage({
        icon: 'checkmark-circle-outline',
        message: 'Success',
        description: response.message,
        type: 'success',
        autoHide: true,
        statusBarHeight: StatusBar.currentHeight,
      });

      // RootNavigation.pop(1);
    } catch (error) {
      showMessage({
        icon: 'auto',
        message: 'Error',
        description: error.data.error.message,
        type: 'danger',
        autoHide: true,
        statusBarHeight: StatusBar.currentHeight,
      });
      const errResp = error.data.error;
      if (error.status === 401) {
        dispatch(expiredAction());
      }
      if (errResp) {
        switch (errResp.name) {
          case 'empty-wishlist':
            dispatch({
              type: MyCourseActionType.POST_EXAM_FAILED,
              payload: [],
              error: error,
            });
            break;

          default:
            dispatch({
              type: MyCourseActionType.POST_EXAM_FAILED,
              error: error,
            });
            break;
        }
      } else {
        dispatch({
          type: MyCourseActionType.POST_EXAM_FAILED,
          error: error,
        });
      }
    }
  };
};

// exam update
export const updateExam = (payload) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: MyCourseActionType.UPDATE_EXAM_REQUEST,
      });

      const response = await API.updateExam(payload);

      dispatch({
        type: MyCourseActionType.UPDATE_EXAM_SUCCESS,
      });

      showMessage({
        icon: 'checkmark-circle-outline',
        message: 'Success',
        description: response.message,
        type: 'success',
        autoHide: true,
        statusBarHeight: StatusBar.currentHeight,
      });
      // RootNavigation.pop(1);
    } catch (error) {
      showMessage({
        icon: 'auto',
        message: 'Error',
        description: error.data.error.message,
        type: 'danger',
        autoHide: true,
        statusBarHeight: StatusBar.currentHeight,
      });
      const errResp = error.data.error;
      if (error.status === 401) {
        dispatch(expiredAction());
      }
      if (errResp) {
        switch (errResp.name) {
          default:
            dispatch({
              type: MyCourseActionType.UPDATE_EXAM_FAILED,
              error: error,
            });
            break;
        }
      } else {
        dispatch({
          type: MyCourseActionType.UPDATE_EXAM_FAILED,
          error: error,
        });
      }
    }
  };
};

// publish course
export const publishCourse = (payload) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: MyCourseActionType.POST_PUBLISH_REQUEST,
      });

      const response = await API.publishCourse(payload);

      dispatch({
        type: MyCourseActionType.POST_PUBLISH_SUCCESS,
      });

      showMessage({
        icon: 'checkmark-circle-outline',
        message: 'Success',
        description: response.message,
        type: 'success',
        autoHide: true,
        statusBarHeight: StatusBar.currentHeight,
      });

      return response.data;
    } catch (error) {
      showMessage({
        icon: 'auto',
        message: 'Error',
        description: "Oops something wen't wrong",
        type: 'danger',
        autoHide: true,
      });
      const errResp = error.data.error;
      if (error.status === 401) {
        dispatch(expiredAction());
      }
      if (errResp) {
        switch (errResp.name) {
          default:
            dispatch({
              type: MyCourseActionType.POST_PUBLISH_FAILED,
              error: error,
            });
            break;
        }
      } else {
        dispatch({
          type: MyCourseActionType.POST_PUBLISH_FAILED,
          error: error,
        });
      }
    }
  };
};

// unpublish course
export const unpublishCourse = (payload) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: MyCourseActionType.POST_UNPUBLISH_REQUEST,
      });

      const response = await API.unpublishCourse(payload);

      dispatch({
        type: MyCourseActionType.POST_UNPUBLISH_SUCCESS,
      });

      showMessage({
        icon: 'checkmark-circle-outline',
        message: 'Success',
        description: response.message,
        type: 'success',
        autoHide: true,
        statusBarHeight: StatusBar.currentHeight,
      });

      return response.data;
    } catch (error) {
      showMessage({
        icon: 'auto',
        message: 'Error',
        description: "Oops something wen't wrong",
        type: 'danger',
        autoHide: true,
        statusBarHeight: StatusBar.currentHeight,
      });
      const errResp = error.data.error;
      if (error.status === 401) {
        dispatch(expiredAction());
      }
      if (errResp) {
        switch (errResp.name) {
          default:
            dispatch({
              type: MyCourseActionType.POST_UNPUBLISH_FAILED,
              error: error,
            });
            break;
        }
      } else {
        dispatch({
          type: MyCourseActionType.POST_UNPUBLISH_FAILED,
          error: error,
        });
      }
    }
  };
};

export const fetchMySubscribedCourseList = () => {
  return async (dispatch) => {
    try {
      dispatch({
        type: MyCourseActionType.FETCH_MYCOURSE_SUBSCRIBED_LIST_REQUEST,
      });

      const response = await API.getSubscribedCourseList();

      dispatch({
        type: MyCourseActionType.FETCH_MYCOURSE_SUBSCRIBED_LIST_SUCCESS,
        payload: response.data.course ? response.data.course : [],
      });
    } catch (error) {
      const errResp = error.data.error;
      if (error.status === 401) {
        dispatch(expiredAction());
      }
      if (errResp) {
        switch (errResp.name) {
          case 'empty-wishlist':
            dispatch({
              type: MyCourseActionType.FETCH_MYCOURSE_SUBSCRIBED_LIST_FAILED,
              payload: [],
              error: error,
            });
            break;
          default:
            dispatch({
              type: MyCourseActionType.FETCH_MYCOURSE_SUBSCRIBED_LIST_FAILED,
              error: error,
            });
            break;
        }
      } else {
        dispatch({
          type: MyCourseActionType.FETCH_MYCOURSE_SUBSCRIBED_LIST_FAILED,
          error: error,
        });
      }
    }
  };
};

export const fetchMyScore = (payload) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: MyCourseActionType.FETCH_MYCOURSE_SCORE_REQUEST,
      });

      const response = await API.getCourseScore(payload);

      dispatch({
        type: MyCourseActionType.FETCH_MYCOURSE_SCORE_SUCCESS,
        payload: response.data ? response.data : {},
      });
    } catch (error) {
      const errResp = error.data.error;
      if (error.status === 401) {
        dispatch(expiredAction());
      }
      if (errResp) {
        switch (errResp.name) {
          case 'empty-wishlist':
            dispatch({
              type: MyCourseActionType.FETCH_MYCOURSE_SCORE_FAILED,
              payload: [],
              error: error,
            });
            break;
          default:
            dispatch({
              type: MyCourseActionType.FETCH_MYCOURSE_SCORE_FAILED,
              error: error,
            });
            break;
        }
      } else {
        dispatch({
          type: MyCourseActionType.FETCH_MYCOURSE_SCORE_FAILED,
          error: error,
        });
      }
    }
  };
};
