import { MyCourseActionType } from 'utils';

const defaultState = {
  selectedCatgory: {},
  myCourses: [],
  myCoursesList: [],
  scoreData: {},
  courseMaterials: [],
  courseExams: [],
  createResponse: '',
  isLoading: false,
  imageLoading: false,
  errors: [],
};

export default (state = defaultState, action = {}) => {
  switch (action.type) {
    // COURSE CREATE - SELECT CATEGORY
    case MyCourseActionType.SET_SELECTED_COURSE: {
      return {
        ...state,
        selectedCatgory: action.payload,
      };
    }
    // COURSE CREATE
    case MyCourseActionType.POST_COURSE_REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case MyCourseActionType.POST_COURSE_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        createResponse: action.payload,
      };
    }
    case MyCourseActionType.POST_COURSE_FAILED: {
      return {
        ...state,
        isLoading: false,
        errors: action.error,
      };
    }
    // COURSE UPDATE
    case MyCourseActionType.UPDATE_COURSE_REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case MyCourseActionType.UPDATE_COURSE_SUCCESS: {
      return {
        ...state,
        isLoading: false,
      };
    }
    case MyCourseActionType.UPDATE_COURSE_FAILED: {
      return {
        ...state,
        isLoading: false,
        errors: action.error,
      };
    }
    // COURSE LIST FETCH
    case MyCourseActionType.FETCH_MYCOURSE_REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case MyCourseActionType.FETCH_MYCOURSE_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        myCourses: action.payload,
      };
    }
    case MyCourseActionType.FETCH_MYCOURSE_FAILED: {
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };
    }

    // Subscribed List
    case MyCourseActionType.FETCH_MYCOURSE_SUBSCRIBED_LIST_REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }
    // COURSE UPDATE IMAGE
    case MyCourseActionType.POST_UPLOADIMAGE_REQUEST: {
      return {
        ...state,
        imageLoading: true,
      };
    }
    case MyCourseActionType.POST_UPLOADIMAGE_SUCCESS: {
      return {
        ...state,
        imageLoading: false,
      };
    }
    case MyCourseActionType.POST_UPLOADIMAGE_FAILED: {
      return {
        ...state,
        imageLoading: false,
        error: action.error,
      };
    }
    // COURSE GET MATERIALS
    case MyCourseActionType.FETCH_GETMATERIS_REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case MyCourseActionType.FETCH_GETMATERIS_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        courseMaterials: action.payload,
      };
    }
    case MyCourseActionType.FETCH_GETMATERIS_FAILED: {
      return {
        ...state,
        isLoading: false,
        courseMaterials: [],
        error: action.error,
      };
    }
    // MATERIAL CREATE
    case MyCourseActionType.POST_POSTMATERI_REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case MyCourseActionType.POST_POSTMATERI_SUCCESS: {
      return {
        ...state,
        isLoading: false,
      };
    }
    case MyCourseActionType.POST_POSTMATERI_FAILED: {
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };
    }
    // MATERIAL UPDATE
    case MyCourseActionType.UPDATE_MATERI_REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case MyCourseActionType.UPDATE_MATERI_SUCCESS: {
      return {
        ...state,
        isLoading: false,
      };
    }
    case MyCourseActionType.UPDATE_MATERI_FAILED: {
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };
    }
    // MATERIAL UPDATE FILE
    case MyCourseActionType.UPDATE_FILEMATERI_REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case MyCourseActionType.UPDATE_FILEMATERI_SUCCESS: {
      return {
        ...state,
        isLoading: false,
      };
    }
    case MyCourseActionType.UPDATE_FILEMATERI_FAILED: {
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };
    }
    // EXAM LIST FETCH
    case MyCourseActionType.FETCH_GETEXAM_REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case MyCourseActionType.FETCH_GETEXAM_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        courseExams: action.payload,
      };
    }
    case MyCourseActionType.FETCH_GETEXAM_FAILED: {
      return {
        ...state,
        isLoading: false,
        courseExams: [],
        error: action.error,
      };
    }
    // EXAMP CREATE
    case MyCourseActionType.POST_EXAM_REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case MyCourseActionType.POST_EXAM_SUCCESS: {
      return {
        ...state,
        isLoading: false,
      };
    }
    case MyCourseActionType.POST_EXAM_FAILED: {
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };
    }
    // EXAMP UPDATE
    case MyCourseActionType.UPDATE_EXAM_REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case MyCourseActionType.UPDATE_EXAM_SUCCESS: {
      return {
        ...state,
        isLoading: false,
      };
    }
    case MyCourseActionType.UPDATE_EXAM_FAILED: {
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };
    }
    // PUBLISH COURSE
    case MyCourseActionType.POST_PUBLISH_REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case MyCourseActionType.FETCH_MYCOURSE_SUBSCRIBED_LIST_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        myCoursesList: action.payload,
      };
    }
    case MyCourseActionType.FETCH_MYCOURSE_SUBSCRIBED_LIST_FAILED: {
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };
    }
    case MyCourseActionType.POST_PUBLISH_SUCCESS: {
      return {
        ...state,
        isLoading: false,
      };
    }
    case MyCourseActionType.POST_COURSE_FAILED: {
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };
    }

    // Score for Exam
    case MyCourseActionType.FETCH_MYCOURSE_SCORE_REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }
    // UNPUBLISH COURSE
    case MyCourseActionType.POST_UNPUBLISH_REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case MyCourseActionType.FETCH_MYCOURSE_SUBSCRIBED_LIST_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        myCoursesList: action.payload,
      };
    }
    case MyCourseActionType.FETCH_MYCOURSE_SCORE_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        scoreData: action.payload,
      };
    }
    case MyCourseActionType.FETCH_MYCOURSE_SUBSCRIBED_LIST_FAILED: {
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };
    }
    case MyCourseActionType.FETCH_MYCOURSE_SCORE_FAILED: {
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };
    }
    case MyCourseActionType.POST_UNPUBLISH_SUCCESS: {
      return {
        ...state,
        isLoading: false,
      };
    }
    case MyCourseActionType.POST_UNPUBLISH_FAILED: {
      return {
        ...state,
        isLoading: false,
        error: action.error,
      };
    }
    default:
      return state;
  }
};
