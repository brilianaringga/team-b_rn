import API from 'configs/api';
import { WishlistActionType } from 'utils';

export const fetchMyWishlist = () => {
  return async (dispatch) => {
    try {
      dispatch({
        type: WishlistActionType.FETCH_WISHLIST_REQUEST,
      });

      const response = await API.getMyWishlist();

      dispatch({
        type: WishlistActionType.FETCH_WISHLIST_SUCCESS,
        payload: response.data.wishlist.results,
      });
    } catch (error) {
      const errResp = error.data.error;
      if (errResp) {
        switch (errResp.name) {
          case 'empty-wishlist':
            dispatch({
              type: WishlistActionType.FETCH_WISHLIST_FAILED,
              payload: [],
              error: error,
            });
            break;

          default:
            dispatch({
              type: WishlistActionType.FETCH_WISHLIST_FAILED,
              error: error,
            });
            break;
        }
      } else {
        dispatch({
          type: WishlistActionType.FETCH_WISHLIST_FAILED,
          error: error,
        });
      }
    }
  };
};
