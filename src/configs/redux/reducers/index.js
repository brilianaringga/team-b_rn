import { combineReducers } from 'redux';

import exampleReducer from './example/exampleReducer';
import searchReducer from './search/searchReducer';
import wishlistReducer from './wishlist/wishlistReducer';
import courseReducer from './course/courseReducer';
import myCourseReducer from './myCourse/myCourseReducer';
import authReducer from './auth/authReducer';
import profileReducer from './profile/profileReducer';

const reducers = {
  exampleStore: exampleReducer,
  searchStore: searchReducer,
  wishlistStore: wishlistReducer,
  courseStore: courseReducer,
  myCourseStore: myCourseReducer,
  authStore: authReducer,
  profileStore: profileReducer,
};

const rootReducer = combineReducers(reducers);

export default rootReducer;
