const defaultState = {
  isLoading: false,
  isLoadingRegister: false,
  fullname: '',
  username: '',
  email: '',
  profilePicture: '',
  error: null,
  isEmailVerified: null,
  isAuthor: null,
  preference: null,
  isSuccessVerify: null,
  errorRegister: null,
};

export default (state = defaultState, action = {}) => {
  switch (action.type) {
    case 'SESSION_EXPIRED': {
      return defaultState;
    }

    case 'LOGIN_REQUEST': {
      return {
        ...state,
        isLoading: true,
        error: null,
      };
    }

    case 'REGISTER_REQUEST': {
      return {
        ...state,
        isLoadingRegister: true,
        errorRegister: null,
      };
    }

    case 'LOGIN_SUCCESS': {
      return {
        ...state,
        fullname: action.payload.fullname,
        username: action.payload.username,
        email: action.payload.email,
        profilePicture: action.payload.profilePicture,
        isEmailVerified: action.payload.isEmailVerified,
        isAuthor: action.payload.isAuthor,
        preference: action.payload.preference,
        isLoading: false,
        error: false,
      };
    }

    case 'LOGIN_FAILED': {
      return {
        ...defaultState,
        error: action.payload.error,
        isLoading: false,
      };
    }

    case 'REGISTER_SUCCESS': {
      return {
        ...state,
        isLoadingRegister: false,
        errorRegister: false,
      };
    }

    case 'VERIFY_OTP': {
      return {
        ...state,
        isSuccessVerify: action.payload.isSuccessVerify,
        isEmailVerified: action.payload.isEmailVerified,
        isLoading: false,
      };
    }
    case 'REGISTER_FAILED': {
      return {
        ...state,
        errorRegister: action.payload.error,
      };
    }

    case 'SET_VERIFIED': {
      return {
        ...state,
        isEmailVerified: action.payload,
      };
    }

    default:
      return state;
  }
};
