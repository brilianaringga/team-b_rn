import { SearchActionType } from 'utils';

const defaultState = {
  popular_search_data: [],
  popular_trainer_data: [],
  categories_data: [],
  searchresult_course: [],
  searchresult_profile: [],
  isLoading: false,
  errors: [],
};

export default (state = defaultState, action = {}) => {
  switch (action.type) {
    case SearchActionType.FETCH_POPSEARCH_REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case SearchActionType.FETCH_POPSEARCH_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        popular_search_data: action.payload,
      };
    }
    case SearchActionType.FETCH_POPSEARCH_FAILED: {
      return {
        ...state,
        isLoading: false,
        errors: action.error,
      };
    }
    case SearchActionType.FETCH_POPTRAINER_REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case SearchActionType.FETCH_POPTRAINER_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        popular_trainer_data: action.payload,
      };
    }
    case SearchActionType.FETCH_POPTRAINER_FAILED: {
      return {
        ...state,
        isLoading: false,
        errors: action.error,
      };
    }
    case SearchActionType.FETCH_CATEGORIES_REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case SearchActionType.FETCH_CATEGORIES_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        categories_data: action.payload,
      };
    }
    case SearchActionType.FETCH_CATEGORIES_FAILED: {
      return {
        ...state,
        isLoading: false,
        errors: action.error,
      };
    }
    case SearchActionType.FETCH_SEARCHRESULT_REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case SearchActionType.FETCH_SEARCHRESULT_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        searchresult_course: action.payload.course,
        searchresult_profile: action.payload.profile,
      };
    }
    case SearchActionType.FETCH_SEARCHRESULT_FAILED: {
      return {
        ...state,
        isLoading: false,
        errors: action.error,
      };
    }
    default:
      return state;
  }
};
