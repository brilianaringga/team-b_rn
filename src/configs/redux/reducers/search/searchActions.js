import API from 'configs/api';
import { SearchActionType } from 'utils';
import * as RootNavigation from 'configs/routes/RootNavigation';
import {
  dummyApiPopularSearch,
  dummyApiPopularTrainer,
  dummyApiCategories,
} from './dummy';
import { expiredAction } from '../auth/authActions';

export const fetchPopularSearch = () => {
  return async (dispatch) => {
    try {
      dispatch({
        type: SearchActionType.FETCH_POPSEARCH_REQUEST,
      });

      //   const response = await API.getMovieReviews();
      const response = await dummyApiPopularSearch();

      dispatch({
        type: SearchActionType.FETCH_POPSEARCH_SUCCESS,
        payload: response,
      });
    } catch (error) {
      const errResp = error.data.error;
      if (errResp) {
        switch (errResp.name) {
          case 'empty-review':
            dispatch({
              type: SearchActionType.FETCH_POPSEARCH_FAILED,
              payload: [],
              error: error,
            });
            break;

          default:
            dispatch({
              type: SearchActionType.FETCH_POPSEARCH_FAILED,
              error: error,
            });
            break;
        }
      } else {
        dispatch({
          type: SearchActionType.FETCH_POPSEARCH_FAILED,
          error: error,
        });
      }
    }
  };
};

export const fetchPopularTrainer = () => {
  return async (dispatch) => {
    try {
      dispatch({
        type: SearchActionType.FETCH_POPTRAINER_REQUEST,
      });

      const response = await API.popularTrainer();

      dispatch({
        type: SearchActionType.FETCH_POPTRAINER_SUCCESS,
        payload: response.PopularTrainer,
      });
    } catch (error) {
      const errResp = error.data.error;
      if (errResp) {
        switch (errResp.name) {
          case 'empty-review':
            dispatch({
              type: SearchActionType.FETCH_POPTRAINER_FAILED,
              payload: [],
              error: error,
            });
            break;

          default:
            dispatch({
              type: SearchActionType.FETCH_POPTRAINER_FAILED,
              error: error,
            });
            break;
        }
      } else {
        dispatch({
          type: SearchActionType.FETCH_POPTRAINER_FAILED,
          error: error,
        });
      }
    }
  };
};

export const fetchCategories = () => {
  return async (dispatch) => {
    try {
      dispatch({
        type: SearchActionType.FETCH_CATEGORIES_REQUEST,
      });

      const response = await API.getCategory();

      dispatch({
        type: SearchActionType.FETCH_CATEGORIES_SUCCESS,
        payload: response.data,
      });
    } catch (error) {
      const errResp = error.data.error;
      if (errResp) {
        switch (errResp.name) {
          case 'empty-review':
            dispatch({
              type: SearchActionType.FETCH_CATEGORIES_FAILED,
              payload: [],
              error: error,
            });
            break;

          default:
            dispatch({
              type: SearchActionType.FETCH_CATEGORIES_FAILED,
              error: error,
            });
            break;
        }
      } else {
        dispatch({
          type: SearchActionType.FETCH_CATEGORIES_FAILED,
          error: error,
        });
      }
    }
  };
};

export const fetchSearchResult = (payload) => {
  return async (dispatch) => {
    try {
      dispatch({
        type: SearchActionType.FETCH_SEARCHRESULT_REQUEST,
      });

      const response = await API.searchCourse(payload);
      const response_profile = response.dataUser;
      const response_course = response.dataCourse;

      if (response.message === 'Data has been found') {
        dispatch({
          type: SearchActionType.FETCH_SEARCHRESULT_SUCCESS,
          payload: {
            course: response_course,
            profile: response_profile,
          },
        });
      } else {
        dispatch({
          type: SearchActionType.FETCH_SEARCHRESULT_FAILED,
          payload: [],
          error: response.message,
        });
      }
    } catch (error) {
      const errResp = error.data.error;
      if (error.status == 401) {
        dispatch(expiredAction());
      }
      if (errResp) {
        switch (errResp.name) {
          case 'empty-review':
            dispatch({
              type: SearchActionType.FETCH_SEARCHRESULT_FAILED,
              payload: [],
              error: error,
            });
            break;

          default:
            dispatch({
              type: SearchActionType.FETCH_SEARCHRESULT_FAILED,
              error: error,
            });
            break;
        }
      } else {
        dispatch({
          type: SearchActionType.FETCH_SEARCHRESULT_FAILED,
          error: error,
        });
      }
    }
  };
};
