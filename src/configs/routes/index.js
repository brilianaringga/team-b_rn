import {
  createStackNavigator,
  TransitionSpecs,
  TransitionPresets,
} from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import {
  ExamplePage,
  LoginPage,
  ProfilePage,
  RegisterPage,
  SplashScreen,
  HomePage,
  EditPage,
  AvatarPage,
  AllCoursePage,
  PreferredCategoryPage,
  CourseCreatePage,
  NewProfilePage,
  VerificationPage,
  OnboardingPage,
  CourseEditPage,
  MaterialAddPage,
  CourseScorePage,
  ExamCreatePage,
  MaterialPage,
  ExamPage,
  CourseMaterialPage,
  NotificationPage,
  MaterialView,
  ReviewPage,
  TakeExamPage,
} from 'pages';

import {
  React,
  createBottomTabNavigator,
  FontAwesome5,
  View,
  BottomTabBar,
  TouchableNativeFeedback,
  Pressable,
  Platform,
} from 'libraries';
import { Color, FONTS, scale } from 'utils';
import FabButton from 'components/svg/fab';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Icon } from 'react-native-eva-icons';
import {
  navigationRef,
  isReadyRef,
  stackNavigationRef,
} from './RootNavigation';
import {
  CourseDetailPage,
  CoursePage,
  SearchPage,
  SearchResultPage,
  TrainerProfilePage,
  WishlistPage,
} from '../../pages';
import messaging from '@react-native-firebase/messaging';
import { Alert } from 'react-native';

const Stack = createStackNavigator();
const EmptyScreen = () => {
  return null;
};
/*--------------------------
     Transition Options
---------------------------
*/

const CustomTransition = {
  headerShown: false,
  transitionSpec: {
    open: TransitionSpecs.TransitionIOSSpec,
    close: TransitionSpecs.TransitionIOSSpec,
  },
  ...TransitionPresets.SlideFromRightIOS,
};

const BottomTab = createBottomTabNavigator();

/*--------------------------
     Bottom Navigation
---------------------------
*/

function BottomNavigation() {
  return (
    <BottomTab.Navigator
      initialRouteName="HomePage"
      tabBar={(props) => (
        <View
          style={{
            // position: 'absolute',
            // bottom: 0,
            // left: 0,
            // right: 0,
            // backgroundColor: 'transparent',
            backgroundColor: Platform.OS === 'ios' ? 'white' : 'transparent',
            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 1,
            },
            shadowOpacity: 0.22,
            shadowRadius: 2.22,
          }}>
          <BottomTabBar {...props} />
        </View>
      )}
      tabBarOptions={{
        activeTintColor: '#2D8989',
        activeBackgroundColor: Color.primaryColor,
        labelStyle: {
          fontFamily: FONTS.quickSand,
        },
        style: {
          borderTopWidth: 0,
          backgroundColor: 'transparent',
          elevation: 30,
          height: scale(55),
        },
        tabStyle: {
          backgroundColor: 'white',
        },
        showLabel: false,
      }}>
      <BottomTab.Screen
        name="HomePage"
        component={HomePage}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: (tabInfo) => (
            <Icon
              name={tabInfo.focused ? 'home' : 'home-outline'}
              width={scale(28)}
              height={scale(28)}
              fill={tabInfo.color}
            />
          ),
        }}
      />
      <BottomTab.Screen
        name="SearchPage"
        component={SearchPage}
        options={{
          tabBarLabel: 'Search',
          tabBarIcon: (tabInfo) => (
            <Icon
              name="search"
              width={scale(28)}
              height={scale(28)}
              fill={tabInfo.color}
            />
          ),
        }}
      />
      {/* <BottomTab.Screen
        name="MyCoursePage"
        component={MyCourserPage}
        options={{
          tabBarLabel: 'My Course',
          tabBarIcon: (tabInfo) => (
            <Icon
              name="book-open"
              width={24}
              height={24}
              fill={tabInfo.color}
            />
          ),
        }}
      /> */}
      <BottomTab.Screen
        name="CoursePage"
        component={CoursePage}
        options={({ navigation }) => ({
          tabBarButton: () => (
            <View
              style={{
                position: 'relative',
                width: 75,
                alignItems: 'center',
              }}>
              <FabButton
                color={'white'}
                style={{
                  position: 'absolute',
                  top: 0,
                }}
              />
              <Pressable
                style={{
                  top: -22.5,
                  justifyContent: 'center',
                  alignItems: 'center',
                  width: 55,
                  height: 55,
                  borderRadius: 27,
                  backgroundColor: '#2D8989',
                }}
                onPress={() => {
                  // console.log('tengah');
                  navigation.navigate('CoursePage');
                }}>
                <Icon name="book-open" width={32} height={32} fill={'white'} />
              </Pressable>
            </View>
          ),
        })}
      />
      <BottomTab.Screen
        name="WishlistPage"
        component={WishlistPage}
        options={{
          tabBarLabel: 'Wishlist',
          tabBarIcon: (tabInfo) => (
            <Icon
              name={tabInfo.focused ? 'bookmark' : 'bookmark-outline'}
              width={scale(28)}
              height={scale(28)}
              fill={tabInfo.color}
            />
          ),
        }}
      />
      <BottomTab.Screen
        name="ProfilePage"
        component={NewProfilePage}
        options={{
          tabBarLabel: 'Profile',
          tabBarIcon: (tabInfo) => (
            <Icon
              name={tabInfo.focused ? 'person' : 'person-outline'}
              width={scale(28)}
              height={scale(28)}
              fill={tabInfo.color}
            />
          ),
        }}
      />
    </BottomTab.Navigator>
  );
}

/*--------------------------
     Routes
---------------------------
*/

class Routes extends React.Component {
  render() {
    return (
      <NavigationContainer
        ref={navigationRef}
        onReady={() => {
          isReadyRef.current = true;
        }}>
        <Stack.Navigator
          initialRouteName="SplashScreen"
          screenOptions={CustomTransition}>
          <Stack.Screen name="SplashScreen" component={SplashScreen} />
          <Stack.Screen name="RegisterPage" component={RegisterPage} />
          <Stack.Screen name="LoginPage" component={LoginPage} />
          <Stack.Screen name="ProfilePage" component={ProfilePage} />
          <Stack.Screen name="NewProfilePage" component={NewProfilePage} />
          <Stack.Screen name="AvatarPage" component={AvatarPage} />
          <Stack.Screen name="EditPage" component={EditPage} />
          <Stack.Screen name="VerificationPage" component={VerificationPage} />
          <Stack.Screen name="ExamplePage" component={ExamplePage} />
          <Stack.Screen name="CourseCreatePage" component={CourseCreatePage} />
          <Stack.Screen name="CourseDetailPage" component={CourseDetailPage} />
          <Stack.Screen name="CourseEditPage" component={CourseEditPage} />
          <Stack.Screen name="CourseScorePage" component={CourseScorePage} />
          <Stack.Screen name="ExamPage" component={ExamPage} />
          <Stack.Screen name="ExamCreatePage" component={ExamCreatePage} />
          <Stack.Screen name="MaterialPage" component={MaterialPage} />
          <Stack.Screen name="MaterialAddPage" component={MaterialAddPage} />
          <Stack.Screen
            name="TrainerProfilePage"
            component={TrainerProfilePage}
          />
          <Stack.Screen name="SearchResultPage" component={SearchResultPage} />
          <Stack.Screen name="BottomTab" component={BottomNavigation} />
          <Stack.Screen name="AllCoursePage" component={AllCoursePage} />

          <Stack.Screen
            name="PreferredCategoryPage"
            component={PreferredCategoryPage}
          />
          <Stack.Screen name="OnboardingPage" component={OnboardingPage} />
          <Stack.Screen
            name="CourseMaterialPage"
            component={CourseMaterialPage}
          />
          <Stack.Screen name="NotificationPage" component={NotificationPage} />
          <Stack.Screen name="MaterialView" component={MaterialView} />
          <Stack.Screen name="ReviewPage" component={ReviewPage} />
          <Stack.Screen name="TakeExamPage" component={TakeExamPage} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

export default Routes;
