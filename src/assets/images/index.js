const deliveryMan = require('./deliveryMan.png');
const fruits1 = require('./fruits1.png');
const fruits2 = require('./fruits2.png');
const fruitsSmall = require('./fruitsSmall.png');
const logo = require('./alita.png');
const map = require('./map.png');
const orderPrepare = require('./orderPrepare.png');
const orderTaken = require('./orderTaken.png');
const person = require('./person.png');
const mail = require('./mail.png');
const phone = require('./phone.png');
const lock = require('./lock.png');
const eye = require('./eye.png');
const eyeOff = require('./eyeoff.png');
const emptyCourse = require('./empty_course.png');
const onlineTraining = require('./onlineTraining.png');
const Intro2 = require('./intro2.png');
const Intro3 = require('./intro3.png');
const noData = require('./no_data.svg');
const audio = require('./audio.png');
const pdf = require('./pdf.png');
const video = require('./video.png');
const congratulation = require('./congratulation.png');
const rate1 = require('./22-cry.png');
const rate2 = require('./23-sad.png');
const rate3 = require('./12-neutral.png');
const rate4 = require('./01-smile.png');
const rate5 = require('./13-love.png');
const successSubmit = require('./submitSuccess.png');
const Intro1 = require('./intro1.png');
const EmptyNotification = require('./notification.svg');
const EmptyCourse = require('./emptyCourse.svg');

const IMG = {
  deliveryMan,
  fruits1,
  fruits2,
  fruitsSmall,
  logo,
  map,
  orderPrepare,
  orderTaken,
  person,
  mail,
  phone,
  lock,
  eye,
  eyeOff,
  emptyCourse,
  onlineTraining,
  Intro2,
  Intro3,
  noData,
  audio,
  video,
  pdf,
  congratulation,
  rate1,
  rate2,
  rate3,
  rate4,
  rate5,
  successSubmit,
  Intro1,
  EmptyNotification,
  EmptyCourse,
};
export default IMG;
