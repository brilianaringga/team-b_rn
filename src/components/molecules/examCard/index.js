import { Text } from 'components/atoms';
import { React, View } from 'libraries';
import styles from './style';

const ExamCard = ({ item, ...props }) => {
  const { type, question, choices, answer, questionId } = item;
  return (
    <View style={styles.container}>
      <View style={styles.typeContainer}>
        <View style={styles.circleNumber}>
          <Text style={styles.numberLabel}>{questionId}</Text>
        </View>
        <Text style={styles.typeLabel}>{type} answer</Text>
      </View>
      <View style={styles.questionContainer}>
        <Text style={styles.question}>{question}</Text>
      </View>
      <View style={styles.optionsContainer}>
        {choices &&
          Object.entries(choices).map(([key, value], index) => {
            return (
              <View key={index} style={styles.optionContainer}>
                <Text style={styles.optionKey}>{key}. </Text>
                <Text style={styles.optionValue}>{value}</Text>
              </View>
            );
          })}
      </View>
      <View style={styles.answerContainer}>
        <Text style={styles.answerLabel}>Answer: </Text>
        <Text style={styles.answer}>{answer.join(',')}</Text>
      </View>
    </View>
  );
};

export default ExamCard;
