import { StyleSheet } from 'libraries';
import { scale } from 'utils';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  touchableArea: {
    backgroundColor: 'white',
    borderRadius: scale(10),
    padding: scale(5),
    flexDirection: 'row',
    // shadowColor: '#000',
    // shadowOffset: {
    //   width: 0,
    //   height: 2,
    // },
    // shadowOpacity: 0.25,
    // shadowRadius: 3.84,
    // elevation: 5,
    marginHorizontal: scale(10),
    marginTop: scale(0),
    marginBottom: scale(10),
  },
  imageContainer: {
    height: scale(100),
  },
  cardImage: {
    width: scale(150),
    height: scale(100),
    borderRadius: scale(10),
  },
  detailContainer: {
    flex: 1,
    padding: scale(5),
    // backgroundColor: 'green',
    justifyContent: 'space-between',
  },
  statusContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  publishLabel: {
    fontSize: scale(10),
    color: 'gray',
  },
  title: {
    fontSize: scale(16),
    fontWeight: 'bold',
    justifyContent: 'center',
    marginBottom: scale(5),
  },
  information: {
    alignSelf: 'flex-end',
    fontStyle: 'italic',
  },
  subscriberContainer: {
    flexDirection: 'row',
  },
  detailRow: {
    marginBottom: scale(5),
  },
  detailIcon: {
    marginRight: scale(5),
  },
});

export default styles;
