import { StyleSheet } from 'libraries';
import { scale, verticalScale } from 'utils';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  title: {
    fontSize: scale(18),
    fontWeight: 'bold',
    paddingBottom: scale(5),
  },
  searchContainer: {
    // padding: scale(10),
    paddingHorizontal: scale(10),
    flexDirection: 'row',
    backgroundColor: '#ddd',
    alignItems: 'center',
    borderRadius: scale(5),
  },
  searchField: {
    flex: 1,
    paddingHorizontal: scale(5),
    paddingVertical: scale(10),
  },
  emptyCategories: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  tcOpacity: {
    flex: 1,
    width: '100%',
    paddingVertical: scale(15),
    // backgroundColor: 'green',
  },
  categories: {
    borderBottomWidth: 1,
    borderBottomColor: 'gray',
    fontWeight: '800',
  },
});

export default styles;
