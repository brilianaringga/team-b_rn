import { Text } from 'components/atoms';
import { React, View } from 'libraries';
import { TouchableOpacity } from 'react-native-gesture-handler';
import styles from './style';

const AuthorMaterialCard = ({ item, index, onPress = {}, ...props }) => {
  const { title, type } = item;
  const colors = {
    pdf: 'orange',
    video: 'red',
    audio: 'blue',
  };

  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <View style={styles.firstLine}>
        <Text style={styles.materialTitle}>
          {index}. {title}
        </Text>
        <View style={[styles.materialType, { backgroundColor: colors[type] }]}>
          <Text style={styles.materialTypeLabel}>{type}</Text>
        </View>
      </View>
      <View>
        <Text>{item.desc}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default AuthorMaterialCard;
