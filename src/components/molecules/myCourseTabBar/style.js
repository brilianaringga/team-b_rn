import { StyleSheet } from 'react-native';
import { Color, scale } from 'utils';

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'transparent',
    paddingHorizontal: scale(15),
  },
  indicator: {
    backgroundColor: Color.pink,
    height: 6,
    borderRadius: 10,
    marginHorizontal: scale(15),
  },
  tab: {
    width: scale(120),
    // flexDirection: 'column-reverse',
    // backgroundColor: 'red',
    // borderRightColor: 'black',
    // borderRightWidth: 1,
    // justifyContent: 'flex-end',
    // alignItems: 'flex-end',
  },
  label: {
    // backgroundColor: 'green',
    fontWeight: 'bold',
    width: scale(110),
    // height: scale(30),
    // textAlign: ''
    // position: 'absolute',
    // bottom: 10,
    // top: -10,
    // left: scale(-55),
    // right: 0,
    // alignContent: 'flex-send',
    // alignSelf: 'flex-end',
  },
});

export default styles;
