import { StyleSheet } from 'react-native';
import { Color, scale, METRICS } from 'utils';
const styles = StyleSheet.create({
  container: {
    paddingVertical: scale(8),
    paddingHorizontal: scale(10),
    backgroundColor: 'white',
    borderRadius: scale(20),
    // shadowColor: '#000',
    // shadowOffset: {
    //   width: 0,
    //   height: 2,
    // },
    // shadowOpacity: 0.25,
    // shadowRadius: 3.84,

    // elevation: 5,
    margin: scale(5),
    height: scale(70),
    minWidth: scale(120),
    alignItems: 'center',
    justifyContent: 'center',
  },
  wrapper: { alignItems: 'center' },
  icon: {
    width: scale(30),
    height: scale(30),
  },
  text: { fontSize: scale(15), marginLeft: scale(5), fontWeight: 'bold' },
});

export default styles;
