import { BackButton, Text } from 'components/atoms';
import { React, View } from 'libraries';
import styles from './style';

const PageHeader = ({ title = '', backAction = {}, ...props }) => {
  const { containerStyles = {}, headerStyles = {}, labelStyles = {} } = props;
  return (
    <View style={[styles.container, containerStyles]}>
      <View style={[styles.header, headerStyles]}>
        <BackButton onPress={backAction} />
        <Text bold style={[styles.headerLabel, labelStyles]}>
          {title}
        </Text>
      </View>
    </View>
  );
};

export default PageHeader;
