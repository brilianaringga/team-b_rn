import { StyleSheet } from 'react-native';
import { Color, scale } from 'utils';

const styles = StyleSheet.create({
  container: {},
  header: {
    flexDirection: 'row',
    paddingHorizontal: scale(10),
    alignItems: 'center',
  },
  headerLabel: {
    fontSize: scale(25),
    color: Color.primaryColor,
    marginLeft: scale(10),
    textTransform: 'capitalize',
  },
});

export default styles;
