import { StyleSheet } from 'libraries';
import { scale } from 'utils';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  touchableArea: {
    backgroundColor: 'white',
    borderRadius: scale(10),
    padding: scale(5),
    flexDirection: 'row',
    marginHorizontal: scale(10),
    marginTop: scale(0),
    marginBottom: scale(10),
  },
  imageContainer: {
    height: scale(100),
  },
  cardImage: {
    width: scale(150),
    height: scale(100),
    borderRadius: scale(10),
  },
  detailContainer: {
    flex: 1,
    padding: scale(5),
    justifyContent: 'space-between',
  },
  statusContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  publishLabel: {
    fontSize: scale(10),
    color: 'gray',
    marginLeft: 2
  },
  title: {
    fontSize: scale(16),
    fontWeight: 'bold',
    justifyContent: 'center',
  },
  desc: {
    fontSize: scale(10),
    fontWeight: '400',
    justifyContent: 'center',
    marginTop: 2
  },
  information: {
    alignSelf: 'flex-end',
    fontStyle: 'italic',
  },
});

export default styles;
