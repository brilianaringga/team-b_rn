import IMG from 'assets/images';
import { Text } from 'components/atoms';
import { EvaIcon, Image, React, View } from 'libraries';
import PropTypes from 'prop-types';
import { Pressable } from 'react-native';
import { scale } from 'utils';
import styles from './style';

const MaterialCard = (props) => {
  const { type, title, desc, isComplete, onPress } = props;
  return (
    <Pressable onPress={onPress}>
      <View
        style={{
          marginTop: scale(15),
          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <Image
          source={
            type == 'pdf' ? IMG.pdf : type == 'video' ? IMG.video : IMG.audio
          }
          style={{ height: 50, width: 50 }}
        />
        <View style={{ marginLeft: scale(15), flex: 1 }}>
          <Text numberOfLines={1} bold style={{ fontSize: scale(15) }}>
            {title}
          </Text>
          <Text
            numberOfLines={2}
            style={{ fontSize: scale(13), marginRight: scale(15) }}>
            {desc}
          </Text>
        </View>
        {isComplete ? (
          <EvaIcon
            name="checkmark-circle-outline"
            width={30}
            height={30}
            fill={'green'}
          />
        ) : (
          <View />
        )}
      </View>
    </Pressable>
  );
};

MaterialCard.propTypes = {
  type: PropTypes.string,
  title: PropTypes.string,
  desc: PropTypes.string,
  isComplete: PropTypes.bool,
  onPress: PropTypes.func,
};

MaterialCard.defaultProps = {
  type: '',
  title: '',
  desc: '',
  isComplete: false,
  onPress: () => {},
};

export default React.memo(MaterialCard);
