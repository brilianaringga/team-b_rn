import { EvaIcon, React, Text, TouchableOpacity, View } from 'libraries';
import { Color } from 'utils';
import styles from './style';

const SearchCategoryCard = (props) => {
  const { item, onPress = {} } = props;
  const { id, name, icon, type } = item;

  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <EvaIcon name={icon} width={42} height={42} fill={Color.primaryColor} />
      <Text style={styles.categoryLabel}>{name}</Text>
    </TouchableOpacity>
  );
};

export default SearchCategoryCard;
