import { Text } from 'components/atoms';
import { base_url } from 'configs/api/url';
import { Image, Pressable, React, View } from 'libraries';
import moment from 'moment';
import PropTypes from 'prop-types';
import { METRICS, scale } from 'utils';
import styles from './style';

const NotificationCard = (props) => {
  const { avatar, learner, message, time, thumb, onPress } = props;
  return (
    <Pressable onPress={onPress}>
      <View style={styles.container}>
        <View style={styles.wrapper}>
          <View style={styles.avatarWrapper}>
            <Image
              source={{
                uri:
                  avatar !== null
                    ? `${base_url}/files/user/thumb/${avatar}`
                    : 'https://ppid.sulselprov.go.id/assets/front/img/kadis/avatar.png',
              }}
              style={styles.avatar}
            />
          </View>

          <View style={styles.textWrapper}>
            <Text bold style={styles.title}>
              {learner}
            </Text>

            <Text>
              <Text medium style={styles.message}>
                {message}
              </Text>
            </Text>
            <Text medium style={styles.time}>
              {moment(new Date(time)).fromNow()}
            </Text>
          </View>
        </View>
        <View style={styles.imageWrapper}>
          <Image
            source={{
              uri:
                thumb !== null
                  ? `${base_url}/files/course/image/${thumb}`
                  : 'https://akm-img-a-in.tosshub.com/indiatoday/images/story/201811/online-3412473_1920_1.jpeg',
            }}
            style={styles.image}
          />
        </View>
      </View>
    </Pressable>
  );
};

NotificationCard.propTypes = {
  avatar: PropTypes.string,
  learner: PropTypes.string,
  message: PropTypes.string,
  time: PropTypes.string,
  thumb: PropTypes.string,
  onPress: PropTypes.func,
};

NotificationCard.defaultProps = {
  avatar: null,
  learner: '',
  message: '',
  time: '1999-01-09',
  thumb: null,
  onPress: () => {},
};

export default React.memo(NotificationCard);
