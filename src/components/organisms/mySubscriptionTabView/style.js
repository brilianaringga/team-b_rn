import { StyleSheet } from 'react-native';
import { scale } from 'utils';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: '#673ab7',
    padding: scale(5),
  },
  flatListContainer: {
    flexGrow: 1,
  },
  empty: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  emptyText1: {
    fontSize: scale(24),
    fontWeight: 'bold',
  },
  emptyText2: {
    fontSize: scale(18),
    color: 'gray',
  },
});

export default styles;
