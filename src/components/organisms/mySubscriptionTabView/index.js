import { Text } from 'components/atoms';
import {
  connect,
  // EvaIcon,
  FlatList,
  React,
  // useState,
  useEffect,
  useIsFocused,
  View,
} from 'libraries';
import { scale } from 'utils';
import styles from './style';
import EmptySubscription from 'assets/images/emptySubscription.svg';
import { fetchMySubscribedCourseList } from 'configs/redux/reducers/myCourse/myCourseActions';
import { MySubcriptionCard } from 'components/molecules';

const MySubscriptionTabView = (props) => {
  const { navigation, myCoursesList, dispatchGetMyCoursesList } = props;
  const isFocused = useIsFocused();

  useEffect(() => {
    dispatchGetMyCoursesList();
  }, []);

  useEffect(() => {
    if (isFocused) {
      dispatchGetMyCoursesList();
    }
  }, [isFocused]);

  return (
    <View style={styles.container}>
      <FlatList
        showsVerticalScrollIndicator={false}
        data={myCoursesList}
        keyExtractor={(item) => item.id.toString()}
        contentContainerStyle={styles.flatListContainer}
        ListEmptyComponent={() => {
          return (
            <View style={styles.empty}>
              <EmptySubscription width={scale(250)} height={scale(250)} />
              <Text style={styles.emptyText1}>Whoops...</Text>
              <Text style={styles.emptyText2}>
                You haven't subscribed to any course yet
              </Text>
            </View>
          );
        }}
        renderItem={({ item }) => {
          return (
            <MySubcriptionCard
              item={item}
              onPress={() => {
                console.log(item);
                // navigation.navigate('CourseEditPage', { data: item });
                  navigation.navigate('CourseDetailPage', {
                    data: item.courseDetail,
                  });
                }
              }
            />
          );
        }}
      />
    </View>
  );
};

function mapStateToProps(state) {
  // console.log(`State tab view : ${state}`)
  return {
    //isi state dari reducer
    myCoursesList: state.myCourseStore.myCoursesList,
    isLoading: state.myCourseStore.isLoading,
    error: state.myCourseStore.error,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    //isi dispatch
    dispatchGetMyCoursesList: () => dispatch(fetchMySubscribedCourseList()),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MySubscriptionTabView);
