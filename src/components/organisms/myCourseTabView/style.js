import { StyleSheet } from 'react-native';
import { Color, scale } from 'utils';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: scale(5),
  },
  flatListContainer: {
    flexGrow: 1,
    paddingTop: scale(10),
    paddingBottom: scale(60),
  },
  empty: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  emptyText1: {
    fontSize: scale(24),
    fontWeight: 'bold',
  },
  emptyText2: {
    fontSize: scale(18),
    color: 'gray',
  },
  emptyBtnAddCotainer: {
    width: '100%',
  },
  emptyBtnAddCourse: {
    backgroundColor: Color.primaryColor,
    // width: '100%',
    padding: 0,
    marginTop: scale(20),
  },
  emptyBtnTextAddCourse: {
    color: 'white',
    fontSize: scale(16),
    fontWeight: '400',
    margin: 0,
  },
  btnAddMyCourse: {
    position: 'absolute',
    bottom: scale(35),
    right: scale(15),
    height: scale(50),
    width: scale(50),
    borderRadius: scale(30),
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
});

export default styles;
