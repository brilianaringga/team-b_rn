import { StyleSheet } from 'react-native';
import { Color, scale, METRICS } from 'utils';
const styles = StyleSheet.create({
  container: {
    paddingHorizontal: scale(5),
    backgroundColor: 'white',
    borderRadius: scale(10),
    // width: METRICS.screen.width,
    marginBottom: scale(8),
    marginHorizontal: scale(8),
    flexDirection: 'row',
    alignItems: 'center',
    // shadowColor: '#000',
    // shadowOffset: {
    //   width: 0,
    //   height: 2,
    // },
    // shadowOpacity: 0.25,
    // shadowRadius: 3.84,

    // elevation: 5,
  },
  thumb: {
    width: scale(165),
    aspectRatio: 16 / 9,
    borderRadius: scale(10),
    marginVertical: scale(5),
  },
  info: { flex: 1, marginLeft: scale(5) },
  title: { fontSize: scale(15), marginTop: scale(5), fontWeight: 'bold' },
  desc: {
    fontSize: scale(11),
    color: '#909AAB',
    marginBottom: scale(5),
  },
  footerWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: scale(5),
  },
  footerViewLoved: { flexDirection: 'row', alignItems: 'center' },
  icon: {
    width: scale(20),
    height: scale(20),
    color: 'black',
  },
  heart: { color: 'red' },
  footerText: {
    fontSize: scale(12),
    marginLeft: scale(2),
    marginRight: scale(5),
  },
  footerBookmark: { flexDirection: 'row', alignItems: 'center' },
});

export default styles;
