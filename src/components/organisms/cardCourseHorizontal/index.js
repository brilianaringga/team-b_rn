import { Text } from 'components/atoms';
import { EvaIcon, Image, Pressable, React, View } from 'libraries';
import PropTypes from 'prop-types';
import styles from './style';

const CardCourseHorizontal = (props) => {
  const {
    thumb,
    title,
    desc,
    viewCount,
    lovedCount,
    isWishList,
    isLoved,
    onPress,
  } = props;
  return (
    <Pressable onPress={onPress}>
      <View style={styles.container}>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <Image
            source={{
              uri: thumb,
            }}
            style={styles.thumb}
          />
          <View style={styles.info}>
            <Text numberOfLines={2} medium style={styles.title}>
              {title}
            </Text>
            <Text numberOfLines={2} style={styles.desc}>
              {desc}
            </Text>
            <View
              style={{
                flex: 1,
                justifyContent: 'flex-end',
              }}>
              <View style={styles.footerWrapper}>
                <View style={styles.footerViewLoved}>
                  <EvaIcon
                    name="eye-outline"
                    width={styles.icon.width}
                    height={styles.icon.height}
                    fill={styles.icon.color}
                  />
                  <Text medium style={styles.footerText}>
                    {viewCount}
                  </Text>
                  <EvaIcon
                    name={isLoved ? 'heart' : 'heart-outline'}
                    width={styles.icon.width}
                    height={styles.icon.height}
                    fill={styles.heart.color}
                  />
                  <Text medium style={styles.footerText}>
                    {lovedCount}
                  </Text>
                </View>

                <View style={styles.footerBookmark}>
                  <EvaIcon
                    name={isWishList ? 'bookmark' : 'bookmark-outline'}
                    width={styles.icon.width}
                    height={styles.icon.height}
                    fill={isWishList ? 'orange' : styles.icon.color}
                  />
                </View>
              </View>
            </View>
          </View>
        </View>
      </View>
    </Pressable>
  );
};

CardCourseHorizontal.propTypes = {
  thumb: PropTypes.string,
  title: PropTypes.string,
  desc: PropTypes.string,
  viewCount: PropTypes.string,
  lovedCount: PropTypes.string,
  isWishList: PropTypes.bool,
  isLoved: PropTypes.bool,
  onPress: PropTypes.func,
};

CardCourseHorizontal.defaultProps = {
  thumb: '',
  title: '',
  desc: '',
  viewCount: '',
  lovedCount: '',
  isWishList: false,
  isLoved: false,
  onPress: () => {},
};

export default React.memo(CardCourseHorizontal);
