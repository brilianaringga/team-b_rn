import { StyleSheet } from 'react-native';
import { Color, scale, METRICS } from 'utils';
const styles = StyleSheet.create({
  container: {
    padding: scale(5),
    backgroundColor: 'white',
    borderRadius: scale(10),
    width: scale(160),
    height: scale(210),
    margin: scale(8),
    // shadowColor: '#000',
    // shadowOffset: {
    //   width: 0,
    //   height: 2,
    // },
    // shadowOpacity: 0.25,
    // shadowRadius: 3.84,

    // elevation: 5,
  },
  image: {
    width: scale(150),
    aspectRatio: 4 / 3,
    borderRadius: scale(10),
  },
  title: { fontSize: scale(15), marginTop: scale(5), fontWeight: 'bold' },
  desc: { fontSize: scale(11), color: '#909AAB' },
  footer: { flex: 1, justifyContent: 'flex-end' },
  footerWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  footerViewLoved: { flexDirection: 'row', alignItems: 'center' },
  icon: {
    width: scale(20),
    height: scale(20),
    color: 'black',
  },
  heart: { color: 'red' },
  footerText: {
    fontSize: scale(12),
    marginLeft: scale(2),
    marginRight: scale(5),
  },
  footerBookmark: { flexDirection: 'row', alignItems: 'center' },
});

export default styles;
