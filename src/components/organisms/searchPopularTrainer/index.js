import { Text } from 'components/atoms';
import { base_url } from 'configs/api/url';
import { getTrainerProfile } from 'configs/redux/reducers/profile/profileActions';
import {
  React,
  ScrollView,
  View,
  TouchableOpacity,
  Image,
  EvaIcon,
  connect,
} from 'libraries';
import { Color } from 'utils';
import styles from './style';

const SearchPopularTrainer = ({ popularTrainer = [], ...props }) => {
  const { navigation, dispatchGetTrainer } = props;

  const onTapHandler = async (item) => {
    // fetch trainer first
    const payload = {
      paramsId: item.username,
    };
    await dispatchGetTrainer(payload);

    // then route
    navigation.navigate('TrainerProfilePage', {
      item: item,
      authorUsername: item.username,
    });
  };

  return (
    <ScrollView
      horizontal={true}
      showsHorizontalScrollIndicator={false}
      style={styles.container}>
      {popularTrainer &&
        popularTrainer.map((item) => {
          const {
            username,
            profilePicture,
            fullname,
            averageRating,
            subscriber,
            totalview,
            totalcourse,
          } = item;

          return (
            <TouchableOpacity
              key={username}
              style={styles.trainerCardContainer}
              onPress={() => onTapHandler(item)}>
              <View style={styles.trainerCard}>
                <Image
                  source={{
                    uri:
                      profilePicture != null
                        ? `${base_url}/files/user/image/${profilePicture}`
                        : 'http://placeimg.com/200/200/people',
                  }}
                  style={styles.avatar}
                />
                <View style={styles.trainerRating}>
                  <EvaIcon
                    name="star"
                    width={24}
                    height={24}
                    fill={Color.yellowStar}
                  />
                  <Text>
                    {averageRating === null
                      ? 'No Rating Yet'
                      : `${averageRating} / 5`}
                  </Text>
                </View>
                <Text numberOfLines={1} style={styles.fullname}>
                  {fullname}
                </Text>
                <View style={styles.trainerDetail}>
                  <View style={styles.trainerSubscriber}>
                    <EvaIcon
                      name="people-outline"
                      width={24}
                      height={24}
                      fill={'black'}
                    />
                    <Text>{subscriber}</Text>
                  </View>
                  <View style={styles.trainerLove}>
                    <EvaIcon name="heart" width={24} height={24} fill={'red'} />
                    <Text>{subscriber}</Text>
                  </View>
                </View>
                <View style={styles.trainerDetail}>
                  <View style={styles.trainerView}>
                    <EvaIcon
                      name="eye-outline"
                      width={24}
                      height={24}
                      fill={'black'}
                    />
                    <Text>{totalview}</Text>
                  </View>
                  <View style={styles.trainerCourses}>
                    <EvaIcon
                      name="book"
                      width={24}
                      height={24}
                      fill={Color.primaryColor}
                    />
                    <Text>{totalcourse}</Text>
                  </View>
                </View>
              </View>
            </TouchableOpacity>
          );
        })}
      {/* <TouchableOpacity
      style={{
        flex: 1,
        width: scale(130),
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: scale(14),
        paddingVertical: scale(8),
        paddingHorizontal: scale(10),
        borderRadius: scale(5),
        backgroundColor: Color.primaryColor,
      }}>
      <Text style={{ color: 'white' }}>See More...</Text>
    </TouchableOpacity> */}
    </ScrollView>
  );
};

function mapStateToProps(state) {
  return {
    //isi state dari reducer
  };
}

function mapDispatchToProps(dispatch) {
  return {
    //isi dispatch
    dispatchGetTrainer: (payload) => dispatch(getTrainerProfile(payload)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SearchPopularTrainer);
