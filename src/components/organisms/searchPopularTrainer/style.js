import { StyleSheet } from 'libraries';
import { Color, scale } from 'utils';

const styles = StyleSheet.create({
  container: { flexDirection: 'row' },
  trainerCardContainer: { paddingBottom: 10 },
  trainerCard: {
    width: scale(130),
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginRight: scale(14),
    paddingVertical: scale(8),
    paddingHorizontal: scale(10),
    borderRadius: scale(5),
    backgroundColor: 'white',
    // shadowColor: '#000',
    // shadowOffset: {
    //   width: 0,
    //   height: 2,
    // },
    // shadowOpacity: 0.25,
    // shadowRadius: 3.84,
    // elevation: 5,
  },
  // trainer
  avatar: {
    width: scale(100),
    height: scale(100),
    borderRadius: scale(50),
    marginBottom: scale(10),
  },
  fullname: {
    color: Color.primaryColor,
    fontWeight: '600',
  },
  trainerRating: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  trainerDetail: {
    flex: 1,
    flexDirection: 'row',
  },
  trainerSubscriber: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  trainerLove: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  trainerView: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  trainerCourses: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
});

export default styles;
