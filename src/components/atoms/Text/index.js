import { React, View, Text as RNText } from 'libraries';
import { FONTS } from 'utils/styles';
import PropTypes from 'prop-types';

const Text = ({ children, bold, medium, style, ...props }) => {
  return (
    <RNText
      {...props}
      allowFontScaling={false}
      style={[
        style,
        {
          fontFamily: bold
            ? FONTS.brandonGrotesqueBold
            : medium
            ? FONTS.brandonGrotesqueMediums
            : FONTS.brandonGrotesque,
        },
      ]}>
      {children}
    </RNText>
  );
};

Text.propTypes = {
  bold: PropTypes.bool,
  medium: PropTypes.bool,
};

Text.defaultProps = {
  bold: false,
  medium: false,
};

export default Text;
