import {
  React,
  View,
  Text,
  TouchableOpacity,
  ActivityIndicator,
} from 'libraries';
import PropTypes from 'prop-types';
import { scale } from 'utils';
import styles from './style';

const Button = (props) => {
  const {
    title,
    onPress,
    isLoading,
    loadingColor,
    textStyle,
    buttonStyle,
    disabled,
  } = props;
  return !disabled ? (
    <TouchableOpacity onPress={onPress}>
      <View style={[styles.button, buttonStyle]}>
        {isLoading ? (
          <ActivityIndicator size="small" color={loadingColor} />
        ) : (
          <Text style={[styles.text, textStyle]}>{title}</Text>
        )}
      </View>
    </TouchableOpacity>
  ) : (
    <View
      style={[
        styles.button,
        buttonStyle,
        { backgroundColor: '#CCCCCC', borderRadius: scale(40) },
      ]}>
      <Text style={[styles.text, { color: 'white' }]}>{title}</Text>
    </View>
  );
};

Button.propTypes = {
  title: PropTypes.string,
  onPress: PropTypes.func,
  isLoading: PropTypes.bool,
  loadingColor: PropTypes.string,
  textStyle: PropTypes.object,
  buttonStyle: PropTypes.object,
  disabled: PropTypes.bool,
};

Button.defaultProps = {
  title: '',
  onPress: () => {},
  isLoading: false,
  loadingColor: 'black',
  textStyle: {},
  buttonStyle: {},
  disabled: false,
};

export default React.memo(Button);
