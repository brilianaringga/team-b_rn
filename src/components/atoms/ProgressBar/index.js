import { React, View } from 'libraries';
import PropTypes from 'prop-types';
import { useRef } from 'react';
import Animated from 'react-native-reanimated';
import { Color, scale } from 'utils';
import Text from '../Text';
import styles from './style';

const ProgressBar = (props) => {
  const { step, height } = props;
  const animatedValue = useRef(new Animated.Value(-1000)).current;
  return (
    <View>
      <Text bold style={{ textAlign: 'right' }}>
        {step}/100
      </Text>
      <View
        style={{
          backgroundColor: '#EBECF0',
          borderRadius: scale(10),
          position: 'relative',
          height: height,
          width: '100%',
        }}>
        <View
          style={{
            backgroundColor: Color.primaryColor,
            borderRadius: scale(10),
            width: `${step}%`,
            position: 'absolute',
            height: height,
            left: 0,
            top: 0,
          }}
        />
      </View>
    </View>
  );
};

ProgressBar.propTypes = {
  step: PropTypes.number,
  height: PropTypes.number,
};

ProgressBar.defaultProps = {
  step: 0,
  height: scale(10),
};

export default React.memo(ProgressBar);
