import { React, EvaIcon, Text, TouchableOpacity, View } from 'libraries';
import styles from './style';

const FloatingButton = (props) => {
  const {
    buttonStyle = {},
    onPress = () => {},
    iconWidth = 28,
    iconHeight = 28,
    iconColor = 'white',
  } = props;

  return (
    <TouchableOpacity style={[styles.btnBody, buttonStyle]} onPress={onPress}>
      <EvaIcon
        name="plus"
        width={iconWidth}
        height={iconHeight}
        fill={iconColor}
      />
    </TouchableOpacity>
  );
};

export default FloatingButton;
